package fido.contacts.merge;

import java.lang.Thread.UncaughtExceptionHandler;

import fido.contacts.merge.data.Contact;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class App extends Application {
	
	/*
	 * STATIC
	 */
	
	private static final boolean DEBUG = true;
	
	private static final String LOG_FILENAME = "?APPNAME?-log.txt";
	
	private static final String LOG_TAG = "Aplication";
	
	private static final String PREFERENCES_FILENAME = "settings";
	
	private static App app;
	
	/**
	 * Static getter for shorter access. Handling exception is not necessary, app instance existence is guaranteed. 
	 * @return app instance
	 */
	public static App get(){
		return app;
	}
	
	
	/*
	 * INSTANCE
	 */
	
	private UncaughtExceptionHandler exceptionHandler = new UncaughtExceptionHandler() {
		
	    @Override
	    public void uncaughtException(Thread thread, Throwable exception) {
		    logManager.logUncaughtError(exception);
		    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, exception);
		}
	    
	};
	
	
	private String appName;
	
	private LogManager logManager;
	
	private Activity mainActivity;
	
	private RootManager rootManager;
	
	private SharedPreferences settings;
	
	
	public LogManager getLogManager() {
		return logManager;
	}

	public Activity getMainActivity() {
		return mainActivity;
	}

	public void setMainActivity(Activity mainActivity) {
		this.mainActivity = mainActivity;
	}
	
	public RootManager getRootManager() {
		return rootManager;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		app = this;
		Thread.currentThread().setUncaughtExceptionHandler(exceptionHandler);
		appName = getString(R.string.app_name);
		logManager = new LogManager(LOG_FILENAME.replace("?APPNAME?", appName), DEBUG);
		rootManager = new RootManager(this);
		settings = getSharedPreferences(PREFERENCES_FILENAME, MODE_PRIVATE);
		
		Contact.setMissingDisplayName(getString(R.string.error_missing_display_name));
	}
	
	public boolean settingExist(String settingName){
		boolean result = settings.contains(settingName);
		logManager.logMessage(LOG_TAG, "checked existence of setting '" + settingName + "' with result: " + result);
		return result;
	}
	
	public boolean settingExist(BooleanSetting setting){
		return settingExist(setting.getName());
	}
	
	public boolean loadBooleanSetting(BooleanSetting setting){
		boolean result = settings.getBoolean(setting.getName(), setting.getDefaultValue());
		logManager.logMessage(LOG_TAG, "loaded boolean setting '" + setting.getName() + "', value is: " + result);
		return result;
	}
	
	public void saveBooleanSetting(BooleanSetting setting, boolean value){
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(setting.getName(), value);
		editor.commit();
		logManager.logMessage(LOG_TAG, "saved boolead setting '" + setting.getName() + "' with value : " + value);
	}
	
}
