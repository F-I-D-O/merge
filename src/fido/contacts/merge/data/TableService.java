package fido.contacts.merge.data;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.util.SparseArray;

public abstract class TableService {
	
	/**
	 * Instance of content resolver. It is used for database querying.
	 */
	protected ContentResolver contentResolver;
	
	public TableService(ContentResolver contentResolver) {
		this.contentResolver = contentResolver;
	}
	
	/**
	 * Build SQL IN statement string including brackets.
	 * @param options Options for IN.
	 * @return IN statement string.
	 */
	public static String buildInString(String... options) {
		String inStatemnetString = "(";
		int i = 0;
		while ( i < options.length - 1) {
			inStatemnetString += "'" + options[i++] + "',";
		}
		inStatemnetString += "'" + options[i] + "')";
		return inStatemnetString;
	}
	
	public static <T> String buildInString(SparseArray<T> options) {
		String[] inArgumentsArray = new String[options.size()];
		int key;
		for(int i = 0; i < options.size(); i++) {
		   key = options.keyAt(i);
		   inArgumentsArray[i] = Integer.toString(key);
		}	
		return buildInString(inArgumentsArray);
	}

	/**
	 * Basic querying method. 
	 * @param uri Uri of the table.
	 * @param projection Defines which columns we want.
	 * @param selection Defines which rows we want.
	 * @return Cursor with chosen data.
	 */
	protected Cursor query(Uri uri, String[] projection, String selection){
		return contentResolver.query(uri, projection, selection, null, null);
	}

}
