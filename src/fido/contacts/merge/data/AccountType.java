package fido.contacts.merge.data;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashSet;

import fido.contacts.merge.R;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public enum AccountType {
	PHONE("P", R.drawable.ic_launcher),
	GOOGLE("G", R.drawable.google),
	FACEBOOK("F", R.drawable.facebook);
	
	private static final String ACCOUNT_TYPE_STRING_GOOGLE = "com.google";
	private static final HashSet<String> ACCOUNT_TYPE_STRINGS_FACEBOOK = new HashSet<String>(Arrays.asList(new String[]{
		"com.facebook.auth.login", 
		"com.sonyericsson.facebook.account"
	}));
	
	private String testString;
	
	private int imageId;
	
	private AccountType(String testString, int imageId){
		this.testString = testString;
		this.imageId = imageId;
	}
	
	public static AccountType getAccountType(String accountTypeString){
		if(accountTypeString.equals(ACCOUNT_TYPE_STRING_GOOGLE)){
			return GOOGLE;
		}
		else if(ACCOUNT_TYPE_STRINGS_FACEBOOK.contains(accountTypeString)){
			return FACEBOOK;
		}
		else{
			return PHONE;
		}
	}

	public String getTestString() {
		return testString;
	}
	
	public int getImageId() {
		return imageId;
	}

	public int getIcon(){
		return imageId;
	}

}
