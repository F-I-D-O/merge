package fido.contacts.merge.data;

/**
 * @author Fido
 * Lookup pair key from aggregation candidates map.
 */
public class LookupPair {
	
	/**
	 * First lookUp key.
	 */
	private String lookup1;
	
	/**
	 * Second lookUp key
	 */
	private String lookup2;
	
	private int hashCode;
	
	
	
	
	public String getLookup1() {
		return lookup1;
	}
	
	public String getLookup2() {
		return lookup2;
	}
	
	
	
	/**
	 * Constructor. 
	 * @param lookup1 First lookUp key.
	 * @param lookup2 Second lookUp key.
	 */
	public LookupPair(String lookup1, String lookup2) {
		this.lookup1 = lookup1;
		this.lookup2 = lookup2;
		hashCode = lookup1.hashCode() + lookup2.hashCode();
		
	}
	
	
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof LookupPair){			
			LookupPair secondPair = (LookupPair) o;
			if(hashCode == secondPair.hashCode){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

}
