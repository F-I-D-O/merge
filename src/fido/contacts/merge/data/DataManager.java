package fido.contacts.merge.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.AggregationExceptions;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.util.SparseArray;

/**
 * @author Fido
 * Singleton that manages database operations and aggregation candidates collection.
 */
public final class DataManager {
	
	
	/**
	 * STATIC
	 */
	
	/**
	 * @author david_000
	 * Contract class for this app's own mimetype in data table.
	 */
	protected static final class AppMimetypeContract{
		
		/**
		 * mimetype string identifier
		 */
		protected static final String MIMETYPE = "vnd.android.cursor.item/fido";
		
		/**
		 * Alias for data1 column. It's used for storing the id a contact is aggregated with.
		 */
		protected static final String AGGREGATED_WITH = "data1";
	}
	
	public static boolean debug = true;
	
	
	/**
	 * Data manager instance.
	 */
	private static DataManager dataManager;
		
	
	
	
	/**
	 * Method for instance creation.
	 * @param contentResolver ContentResolver from an Activity.
	 * @throws DataManagerException If instance was already created.
	 */
	public static void create(ContentResolver contentResolver) throws DataManagerException{
		if (dataManager != null) {
            throw new DataManagerException(DataManagerException.MANAGER_ALREADY_CREATED);
        }
		dataManager = new DataManager(contentResolver);
	}
		
	/**
	 * Destroys the instance
	 */
	public static void destroy(){
		dataManager = null;
	}
	
	/**
	 * Getter of the instance.
	 * @return instance of DataManager.
	 * @throws DataManagerException If instance hasn't been created yet.
	 */
	public static DataManager get() throws DataManagerException {
         if (dataManager == null) {
             throw new DataManagerException(DataManagerException.MANAGER_NULL);
         }
         return dataManager;
    }
	
	/**
	 * Determines if there is already an instance of DataManager created.
	 * @return true if data manager has been already created.
	 */
	public static boolean isInit(){
		return dataManager != null;
	}

	
	
	
	/**
	 * INSTANCE
	 */

	/**
	 * Collections
	 */
	
	/**
	 * Map containing Aggregation Candidates as values. Keys are pairs of raw contact ids. It's used only while 
	 * searching. The key value character is used to determine if some combination of contact was already found.
	 */
	private HashMap<NumberPair, AggregationCandidate> aggregationCandidates;
	
	/**
	 * List of aggregation candidates. It's used in UI where the list collection is better for the adapter.
	 */
	private ArrayList<AggregationCandidate> aggregationCandidatesList;
	
	/**
	 * List of aggregation exceptions.
	 */
	private HashSet<NumberPair>	aggregationExceptions;
	
	/**
	 * List of contacts found as matches with another contact. It ensures that raw contact data are loaded only once, 
	 * regardless of the number of aggregation candidates this contact is linked to.
	 */
	private SparseArray<Contact> aggregationContacts;
	
	private ArrayList<Contact> aggregationContactsList;
	
	/**
	 * List of database operations to be queried in one batch.
	 */
	private ArrayList<ContentProviderOperation> providerOperations;
	
	
	
	/**
	 * Other Variables
	 */
	
	private AggregatedContacts aggregatedContacts;
	
	/**
	 * Determines if the content of aggregation candidates list changed. Useful for UI.
	 */
	private boolean aggregationCandidatesListChanged;
		
	/**
	 * Instance of content resolver. It is used for database querying.
	 */
	private ContentResolver contentResolver;
	
	/**
	 * Service class for contacts table
	 */
	protected ContactService contactService;
	
	/**
	 * Service class for data table
	 */
	protected DataService dataService;
	
	/**
	 * Contact chosen to aggregate individually
	 */
	private AggregationCandidate individualAggregation;
	
	/**
	 * Number of all contacts in the database.
	 */
	private int numberOfContacts;
	
	/**
	 * Service class for raw contacts table
	 */
	protected RawContactService rawContactService;
	
	
	
	
	/**
	 * Getter and setters
	 */
		
	public ArrayList<AggregatedContact> getAggregatedContactsList() {
		return aggregatedContacts.getAggregatedContactsList();
	}
	
	/**
	 * Returns an array of all aggregation candidates.
	 * @return Array of all aggregation candidates.
	 */
	public ArrayList<AggregationCandidate> getAggregationCandidatesList() {	
		return aggregationCandidatesList;
	}
	
	public boolean isAggregatedContactsListChanged() {
		return aggregatedContacts.isAggregatedContactsListChanged();
	}
	
	public boolean isAggregationCandidatesListChanged() {
		return aggregationCandidatesListChanged;
	}

	public DataService getDataService() {
		return dataService;
	}
	
	public AggregationCandidate getIndividualAggregation() {
		return individualAggregation;
	}
	
	public void setIndividualAggregation(AggregationCandidate individualAggregation) {
		this.individualAggregation = individualAggregation;
	}
	
	public int getNumberOfContacts() {
		return numberOfContacts;
	}

	
	/**
	 * Methods
	 */

	/**
	 * Constructor
	 * @param contentResolver ContentResolver for querying
	 */
	private DataManager(ContentResolver contentResolver){
		this.contentResolver = contentResolver;
		contactService = new ContactService(contentResolver);
		rawContactService = new RawContactService(contentResolver);
		dataService = new DataService(contentResolver);
	}
	
	
	
	/**
	 * Public
	 */
	
	/**
	 * Fill contacts with data for UI.
	 */
	public void fillContacts() {
		if(aggregationCandidates.size() < 1){ // if no candidates for aggregation found
			aggregationCandidatesList = new ArrayList<AggregationCandidate>();
			return;
		}
		aggregationCandidatesList = new ArrayList<AggregationCandidate>(aggregationCandidates.values());
		String contactIdInStatementString = TableService.buildInString(aggregationContacts);
		aggregationContactsList = CollectionService.sparseArrayToArrayList(aggregationContacts);
		
		fillContactsFromRawContactTable(contactIdInStatementString);
		
		/* Initialization is here because raw contact could be added in fillContactsFromRawContactTable. It's those 
		raw contacts that wasn't found as match during search */
		for (Contact contact : aggregationContactsList) {
			contact.intitRawContactList();
		}
		
		fillContactsFromDataTable(contactIdInStatementString);
		fillContactsFromContactTable(contactIdInStatementString);
	}

	/**
	 * Wrapper method for the process of finding aggregated contacts
	 */
	public void findAggregatedContacts(){
		aggregatedContacts = new AggregatedContacts(this);
		aggregatedContacts.findAggregatedContacts();
	}
	
	/**
	 * Data structure preparation before searching for aggregation candidates.
	 */
	public void initSearch(){
		aggregationContacts = new SparseArray<Contact>();
		aggregationCandidates = new HashMap<NumberPair, AggregationCandidate>();
		initContactCount();
	}
	
	public void removeMergedContact(AggregatedContact aggregatedContact) {
		aggregatedContacts.removeMergedContact(aggregatedContact);
	}
	
	
	
	/**
	 * Private
	 */
	

	/**
	 * Fills virtual contacts with data from contacts table.
	 * @param contactIdInStatementString string with ids of contacts contained in aggregation candidates
	 */
	private void fillContactsFromContactTable(String contactIdInStatementString) {
		Cursor candidatesData = contactService.queryAggregationCandidatesContacts(contactIdInStatementString);
		candidatesData.moveToFirst();	
		DatabaseUtils.dumpCursor(candidatesData);
		do{
			Contact contact = aggregationContacts.get(
					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Contacts._ID)));
			contact.setDisplayName(
					candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
		}while(candidatesData.moveToNext());
		candidatesData.close();			
	}
	
	/**
	 * Fills virtual contacts with data from data table.
	 * @param contactIdInStatementString string with ids of raw contacts contained in virtual contacts
	 */
	private void fillContactsFromDataTable(String contactIdInStatementString) {
		Cursor candidatesData = dataService.queryAggregationCandidatesData(contactIdInStatementString);
		candidatesData.moveToFirst();		
		do{
			Contact contact = aggregationContacts.get(
					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data.CONTACT_ID)));
			RawContact rawContact = contact.getRawContact(
					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)));
			boolean primary = 
					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data.IS_PRIMARY)) == 1;
			if(candidatesData.getString(candidatesData.getColumnIndexOrThrow(ContactsContract.Data.MIMETYPE)).equals(
					StructuredName.CONTENT_ITEM_TYPE)){
				if(!rawContact.isDisplayNamePrimary()){
					String displayName = 
							candidatesData.getString(candidatesData.getColumnIndex(StructuredName.DISPLAY_NAME));
					rawContact.setDisplayName(displayName);
					if(primary){
						rawContact.setDisplayNamePrimary(true);
					}
				}
			}
			else if(candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Data.MIMETYPE)).equals( 
					Photo.CONTENT_ITEM_TYPE)){
				processPhotoData(contact, rawContact, candidatesData, primary);
				
			}
			else if(candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Data.MIMETYPE)).equals( 
					Phone.CONTENT_ITEM_TYPE)){
				rawContact.addPhoneNumber(candidatesData.getString(candidatesData.getColumnIndex(
									ContactsContract.CommonDataKinds.Phone.NUMBER)));
			}
			else if(candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Data.MIMETYPE)).equals( 
					CommonDataKinds.Email.CONTENT_ITEM_TYPE)){
				rawContact.addEmail(candidatesData.getString(candidatesData.getColumnIndex(
									ContactsContract.CommonDataKinds.Email.ADDRESS)));
			}
		}while(candidatesData.moveToNext());
		candidatesData.close();			
	}
	
	private void processPhotoData(Contact contact, RawContact rawContact, Cursor candidatesData, boolean primary){
		if(!rawContact.isPhotoPrimary()){
			boolean superPrimary = 
					candidatesData.getInt(
							candidatesData.getColumnIndex(ContactsContract.Data.IS_SUPER_PRIMARY)) == 1;
			if(rawContact.getPhoto() == null || superPrimary || (primary && !rawContact.isPhotoSuperPrimary())){
				rawContact.setPhoto(
						candidatesData.getBlob(candidatesData.getColumnIndex(
								ContactsContract.CommonDataKinds.Photo.PHOTO)));
				rawContact.setPhotoId(
						candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data._ID)));
				
				if(primary){
					rawContact.setPhotoPrimary(true);
				}
				if(superPrimary){
					rawContact.setPhotoSuperPrimary(true);
				}
			}
			if(contact.getPhoto() == null || superPrimary || (primary && !contact.isPhotoSuperPrimary())){
				contact.setPhotoRawContact(rawContact);
			}
		}
	}
	
	/**
	 * Fills virtual contacts with data from raw contacts table.
	 * @param contactIdInStatementString string with ids of contacts contained in aggregation candidates
	 */
	private void fillContactsFromRawContactTable(String contactIdInStatementString) {
		Cursor candidatesData = rawContactService.queryAggregationCandidatesRawContacts(contactIdInStatementString);
		candidatesData.moveToFirst();		
		do{
			Contact contact = aggregationContacts.get(
					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID)));
			RawContact rawContact = contact.getOrSetRawContact(
					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.RawContacts._ID)));
			rawContact.setAccountType(
					candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_TYPE)));
			rawContact.setAccountName(
					candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_NAME)));
		}while(candidatesData.moveToNext());
		candidatesData.close();	
		
	}
		
	/**
	 * Initialize number of all contacts
	 */
	private void initContactCount(){
		String[] projection = new String[]{ContactsContract.Contacts._COUNT};
		Cursor contactCursor = contactService.queryContact(projection, null);
		contactCursor.moveToFirst();
		numberOfContacts = contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.Contacts._COUNT));
	}
		
	/**
	 * Basic querying method. 
	 * @param uri Uri of the table.
	 * @param projection Defines which columns we want.
	 * @param selection Defines which rows we want.
	 * @return Cursor with chosen data.
	 */
	private Cursor query(Uri uri, String[] projection, String selection){
		return contentResolver.query(uri, projection, selection, null, null);
	}
		
	/**
	 * Query all aggregation exceptions of the type which aggregates raw contacts together.
	 * @return {@link Cursor} with aggregation exceptions.
	 */
	protected Cursor queryAllAggregationExceptions(){
		Uri uri = ContactsContract.AggregationExceptions.CONTENT_URI;
		String[] projection = new String[]{	ContactsContract.AggregationExceptions.RAW_CONTACT_ID1, 
				ContactsContract.AggregationExceptions.RAW_CONTACT_ID2};
		String selection = ContactsContract.AggregationExceptions.TYPE + " = " + 
				ContactsContract.AggregationExceptions.TYPE_KEEP_TOGETHER;
		return query(uri, projection, selection);
	}


	
	
	
	
	
	
	


	


	
	




	
	
	

	/**
	 * Adds new aggregation candidate to the map.
	 * @param firstContactId raw contact id of the first raw contact.
	 * @param secondContactId raw contact of the second raw contact.
	 * @param methodID Integer identifying the string that contains the name of the method with that was aggregation 
	 * candidates found.
	 */
	public void addAgregaitonCandidate(int firstContactId, int secondContactId, int firstRawContactId,
			int secondRawContactId, int methodID){
		Contact firstContact = getOrSetContatct(firstContactId);
		Contact secondContact = getOrSetContatct(secondContactId);
		
		RawContact firstRawContact = firstContact.getOrSetRawContact(firstRawContactId);
		RawContact secondRawContact = secondContact.getOrSetRawContact(secondRawContactId);
		
		NumberPair aggregationKey = new NumberPair(firstContact.getId(), secondContact.getId());
		if(aggregationCandidates.containsKey(aggregationKey)){
			aggregationCandidates.get(aggregationKey).addMethod(methodID);
		}
		else{
			aggregationCandidates.put(aggregationKey, new AggregationCandidate(firstRawContact, secondRawContact, 
					methodID, firstContact,	secondContact));
		}
	}
	
	/**
	 * Get contact by id. if contact isn't in any aggregation yet, it will be added to list
	 * @param contactId id of the contact
	 * @return contact
	 */
	private Contact getOrSetContatct(int contactId) {
		Contact contact = aggregationContacts.get(contactId);
		if(contact == null){
			contact = new Contact(contactId);
			aggregationContacts.put(contactId, contact);
		}
		return contact;
	}
	
	

	/**
	 * Aggregates two contacts in database based on given {@link AggregationCandidate}.
	 * @param aggCandidate {@link AggregationCandidate}.
	 */
	public void aggregateContacts(AggregationCandidate aggCandidate) {
		 ContentValues cv = new ContentValues();
		 cv.put(AggregationExceptions.TYPE, AggregationExceptions.TYPE_KEEP_TOGETHER);
		 cv.put(AggregationExceptions.RAW_CONTACT_ID1, aggCandidate.getFirstRawContact().getRawContactId());
		 cv.put(AggregationExceptions.RAW_CONTACT_ID2, aggCandidate.getSecondRawContact().getRawContactId());
		 contentResolver.update(AggregationExceptions.CONTENT_URI, cv, null, null);
		 System.out.println(aggCandidate.getFirstRawContact().getRawContactId() + "-" + aggCandidate.getSecondRawContact().getRawContactId());
	}
	
	/**
	 * Clear the sign of changes in {@link AggregationCandidate} list.
	 */
	public void clearAggregaionCandidatesListChanged() {
		aggregationCandidatesListChanged = false;
	}
	
	public void clearAggregatedContactsListChanged() {
		aggregatedContacts.clearAggregatedContactsListChanged();
	}
	
	/**
	 * Executes provider operations batch and destroys the list.
	 * @throws OperationApplicationException 
	 * @throws RemoteException 
	 */
	public void executeProviderOperations() throws RemoteException, OperationApplicationException {
			contentResolver.applyBatch(ContactsContract.AUTHORITY, providerOperations);
			providerOperations = null;	
	}
	
	public boolean aggregationExceptionExist(int firstRawContactId, int secondRawContactId){
		NumberPair rawContactIdPair = new NumberPair(firstRawContactId, secondRawContactId);
//		System.out.println(rawContactIdPair.getFirstNumber() + "+" + rawContactIdPair.getSecondNumber());
		return aggregationExceptions.contains(rawContactIdPair);
	}
	
	/**
	 * Returns an {@link ArrayList} of {@link AggregationCandidate} already approved by user to merge.
	 * @return {@link ArrayList} of {@link AggregationCandidate} approved by user to merge.
	 */
	public ArrayList<AggregationCandidate> getAggregationCandidatesForMerge(){
		ArrayList<AggregationCandidate> aggregationCandidatesListTmp = 
				new ArrayList<AggregationCandidate>();
		if(individualAggregation != null){	
			aggregationCandidatesListTmp.add(individualAggregation);
		}
		else{
			aggregationCandidatesListTmp = getApprovedAggregationCandidatesList();
		}
		return aggregationCandidatesListTmp;
	}
	
	/**
	 * Initialize the providerOperations {@link ArrayList}. This has to be called before adding any operation.
	 */
	public void initProvierOperations() {
		providerOperations = new ArrayList<ContentProviderOperation>();
	}
	
	
	
	/**
	 * Remove {@link AggregationCandidate} from the list for the UI.
	 * @param aggregationCandidate {@link AggregationCandidate}
	 */
	public void removeAggregationCandidate(AggregationCandidate aggregationCandidate){
		aggregationCandidatesList.remove(aggregationCandidate);
		aggregationCandidatesListChanged = true;
	}
	
	/**
	 * Sets up batch operations for updating one aggregated"s contact Display name and id.
	 * @param aggregationCandidate {@link AggregationCandidate}
	 */
	public void setupContactUpdateBatch(AggregationCandidate aggregationCandidate) {
		int firstRawContactId = aggregationCandidate.getFirstRawContact().getRawContactId();
		int secondRawContactId = aggregationCandidate.getSecondRawContact().getRawContactId();
		
		// mark for disconnecting
		addAppMark(firstRawContactId, secondRawContactId);
		addAppMark(secondRawContactId, firstRawContactId);
		
		int contactId = getContactId(aggregationCandidate.getFirstRawContact().getRawContactId());
		Cursor contactCursor = contactService.queryContactDisplayNameAndPhotoId(contactId);
		String contactDisplayName = 
				contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		if(!contactDisplayName.equals(aggregationCandidate.getChosenNameContact().getDisplayName())){
			RawContact notChosen = aggregationCandidate.getChosenNameContact() == aggregationCandidate.getFirstRawContact() ? 
					aggregationCandidate.getSecondRawContact() : aggregationCandidate.getFirstRawContact();
			AddOpDeletePreviousOverideRows(aggregationCandidate, contactId);
			AddOpOveridePrimaryContactDisplayName(aggregationCandidate, notChosen);
		}
		int contactPhotoID = contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
		if(contactPhotoID != aggregationCandidate.getChosenPhotoContact().getPhotoId()){
			AddOpUpdatePhotoId(aggregationCandidate);
		}
	}
	
	
	private void addAppMark(int rawContactId, int secondRawContactId) {
		ContentProviderOperation.Builder opInsert = 
				ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
		opInsert.withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
		opInsert.withValue(ContactsContract.Data.MIMETYPE, AppMimetypeContract.MIMETYPE);
		opInsert.withValue(AppMimetypeContract.AGGREGATED_WITH, secondRawContactId);
		providerOperations.add(opInsert.build());
	}
	
	private void removeAppMark(int rawContactId, int secondRawContactId) {	
		String where = ContactsContract.Data.RAW_CONTACT_ID + " = " + rawContactId + " AND " + 
				ContactsContract.Data.MIMETYPE + " = '" + AppMimetypeContract.MIMETYPE + "' AND " + 
				AppMimetypeContract.AGGREGATED_WITH + " = " + secondRawContactId;
		contentResolver.delete(ContactsContract.Data.CONTENT_URI, where, null);
	}

	/**
	 * Constructs bath operation for delete previous contact display name override rows, and add the operation to 
	 * providerOperations {@link ArrayList}. 
	 * @param aggregationCandidate {@link AggregationCandidate}.
	 */
	private void AddOpDeletePreviousOverideRows(AggregationCandidate aggregationCandidate, int rawContactId) {
		ContentProviderOperation.Builder opDelete = 
				ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI);
		opDelete.withSelection(ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + 
				ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.DATA14 + " = ?",
			new String[]{String.valueOf(rawContactId), 
				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
				"fido"});
		providerOperations.add(opDelete.build());
	}
	
	/**
	 * Constructs bath operation for insert override rows with contact display name, and add the operation to 
	 * providerOperations {@link ArrayList}. 
	 * @param aggregationCandidate {@link AggregationCandidate}.
	 */
	private void AddOpOveridePrimaryContactDisplayName(AggregationCandidate aggregationCandidate, RawContact notChosen) {		
		ContentProviderOperation.Builder opInsert = 
				ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
		opInsert.withValue(ContactsContract.Data.MIMETYPE, 
				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
		opInsert.withValue(ContactsContract.Data.DATA1, aggregationCandidate.getChosenNameContact().getDisplayName());
		opInsert.withValue(ContactsContract.Data.DATA14, "fido");
		opInsert.withValue(ContactsContract.Data.RAW_CONTACT_ID, notChosen.getRawContactId());
		opInsert.withValue(ContactsContract.Data.IS_PRIMARY, 1);
		providerOperations.add(opInsert.build());
	}
	
	/**
	 * Constructs bath operation for updating contact photo id, and add the operation to providerOperations 
	 * {@link ArrayList}.
	 * @param aggregationCandidate {@link AggregationCandidate}.
	 */
	private void AddOpUpdatePhotoId(AggregationCandidate aggregationCandidate) {
		ContentProviderOperation.Builder opUpdate = 
				ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
		opUpdate.withSelection(ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + 
				ContactsContract.Data.MIMETYPE + " = ?", 
			new String[]{String.valueOf(aggregationCandidate.getChosenPhotoContact().getRawContactId()), 
				ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE});
		opUpdate.withValue(ContactsContract.Data.IS_PRIMARY, 1);
		opUpdate.withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1);
		providerOperations.add(opUpdate.build());
	}
		

	
	
	/**
	 * Gets contact ID by the rawContacId given.
	 * @param rawContactId id oft the rawContact.
	 * @return contact ID by the rawContacId given.
	 */
	private int getContactId(int rawContactId) {
		String[] projection = new String[]{ContactsContract.RawContacts.CONTACT_ID};
		String selection = ContactsContract.RawContacts._ID + " = " + rawContactId;
		Cursor contactCursor = rawContactService.queryRawContact(projection, selection);
		contactCursor.moveToFirst();
		return contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));
	}
	
	/**
	 * Returns displayName of a contact based on given LookupKey.
	 * @param lookupKey LookupKey of the contact.
	 * @return String displayName of the contact.
	 */
	private String getDisplayName(String lookupKey){
		Cursor cursor = contactService.queryDisplayName(lookupKey);
		cursor.moveToFirst();
		return cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	}
	
	/**
	 * Gets raw contact ID of the last aggregated contact.
	 * @return raw contact ID of the last aggregated contact.
	 */
	private int getLastAggregatedRawContactId() {
		Uri uri = ContactsContract.AggregationExceptions.CONTENT_URI;
		String[] projection = new String[]{ContactsContract.AggregationExceptions.RAW_CONTACT_ID1};
		String sortOrder = ContactsContract.AggregationExceptions._ID + " ASC LIMIT 1";
		Cursor cursor = contentResolver.query(uri, projection, null, null, sortOrder);
		cursor.moveToFirst();
		return cursor.getInt(cursor.getColumnIndex(ContactsContract.AggregationExceptions.RAW_CONTACT_ID1));
	}
		
	
	
	
	

	public ArrayList<AggregationCandidate> getApprovedAggregationCandidatesList() {
		ArrayList<AggregationCandidate> approvedAggregationCandidates= new ArrayList<AggregationCandidate>();
		for (AggregationCandidate candidate : aggregationCandidatesList) {
			if(candidate.isMergeApproved()){
				approvedAggregationCandidates.add(candidate);
			}
		}
		return approvedAggregationCandidates;
	}


	public void disconnectContacts(AggregatedContact aggregatedContact) {
		ContentValues cv = new ContentValues();
		cv.put(AggregationExceptions.TYPE, AggregationExceptions.TYPE_KEEP_SEPARATE);
		cv.put(AggregationExceptions.RAW_CONTACT_ID1, aggregatedContact.getFirstRawContact().getRawContactId());
		cv.put(AggregationExceptions.RAW_CONTACT_ID2, aggregatedContact.getSecondRawContact().getRawContactId());
		contentResolver.update(AggregationExceptions.CONTENT_URI, cv, null, null);
		int firstRawContactId = aggregatedContact.getFirstRawContact().getRawContactId();
		int secondRawContactId = aggregatedContact.getSecondRawContact().getRawContactId();
		removeAppMark(firstRawContactId, secondRawContactId);
		removeAppMark(secondRawContactId, firstRawContactId);
		removeNameHack(firstRawContactId);
		removeNameHack(secondRawContactId);
	}


	private void removeNameHack(int rawContactId) {
		String where = ContactsContract.Data.RAW_CONTACT_ID + " = " + rawContactId + " AND " +
						ContactsContract.Data.MIMETYPE + " = '" + 
						ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "' AND " + 
						ContactsContract.Data.DATA14 + " = 'fido'";
		contentResolver.delete(ContactsContract.Data.CONTENT_URI, where, null);
	}


	


//	public void addTrickyName(Contact chosenNameContact) {
//		System.out.println(chosenNameContact.getRawContactId());
//		ContentValues cv = new ContentValues();
//		cv.put(ContactsContract.Data.RAW_CONTACT_ID, chosenNameContact.getRawContactId());
//		cv.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
//		cv.put(ContactsContract.Data.IS_PRIMARY, 1);
////		cv.put(ContactsContract.Data.IS_SUPER_PRIMARY, 1);
//		cv.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "žluťoučký kůň úpěl");
//		cv.put(ContactsContract.Data.DATA14, "fido");
//		contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
//	}


	public void removeTrickyNames() {
		String where = ContactsContract.Data.MIMETYPE + " = '" + 
				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "' AND " + 
				ContactsContract.Data.DATA14 + " = 'fido'";
		contentResolver.delete(ContactsContract.Data.CONTENT_URI, where, null);
	}


	public void groupAggregationCandidates() {
		int groupIndex = 1;
		
		for (int i = 0; i < aggregationCandidatesList.size(); i++) {
			AggregationCandidate firstAggregationCandidate = aggregationCandidatesList.get(i);
			for (int j = i + 1; j < aggregationCandidatesList.size(); j++) {
				AggregationCandidate secondAggregationCandidate = aggregationCandidatesList.get(j);
				int swapIndex = i + 1;
				if(firstAggregationCandidate.shareRawContactWith(secondAggregationCandidate)){
					if(firstAggregationCandidate.getGroup() > 0){
						secondAggregationCandidate.setGroup(firstAggregationCandidate.getGroup());
					}
					else{
						firstAggregationCandidate.setGroup(groupIndex);
						secondAggregationCandidate.setGroup(groupIndex);
						groupIndex++;
					}
					Collections.swap(aggregationCandidatesList, j, swapIndex);
					swapIndex++;
				}
			}
		}
	}

}
