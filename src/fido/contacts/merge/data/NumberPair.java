package fido.contacts.merge.data;

public class NumberPair {
	private int firstNumber;
	private int secondNumber;
	
	public int getFirstNumber() {
		return firstNumber;
	}
	public int getSecondNumber() {
		return secondNumber;
	}
	
	public NumberPair(int firstNumber, int secondNumber) {
		super();
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
	}
	
	@Override
	public int hashCode() {
		return firstNumber + secondNumber;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null || getClass() != obj.getClass()){
			return false;
		}
		NumberPair other = (NumberPair) obj;
//		System.out.println(firstNumber + "," + secondNumber + "|" + other.firstNumber + "," +  other.secondNumber);
		if (((firstNumber == other.firstNumber) && (secondNumber == other.secondNumber)) ||
				((firstNumber == other.secondNumber) && (secondNumber == other.firstNumber))){
//			System.out.println("succes: " + firstNumber + "," + secondNumber);
			return true;
		}
		return false;	
	}
	
	
}
