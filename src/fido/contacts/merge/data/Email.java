package fido.contacts.merge.data;

import java.util.ArrayList;
import java.util.Arrays;

import fido.contacts.merge.StringService;

/**
 * @author Fido
 * Contact data class for email mimetype.
 */
public class Email extends ModuleData {
	
	/**
	 * At sign.
	 */
	private static final char AT_SIGN = '@';
	
	/**
	 * Regular expression for all characters which are commonly used as name separator in email address.
	 */
	private static final String EMAIL_NAME_SEPARATOR_REGEX = "[.-]";
	
	/**
	 * Whole address.
	 */
	private String addres;
	
	/**
	 * Parts of the address before at sign divided by email name separator regex.
	 */
	private ArrayList<String> addresNameParts;

	/**
	 * Email address without domain.
	 */
	private String addressWithotDomain;
	
	
	
	
	public String getAddres() {
		return addres;
	}

	
	
	
	/**
	 * Constructor
	 * @param lookupKey Lookup key of the contact.
	 * @param contactId ContactId of the contact.
	 * @param addres Email address.
	 */
	public Email(String lookupKey, int contactId, int rawContactId, String addres) {
		super(lookupKey, contactId, rawContactId);
		this.addres = addres;
	}
	
	
	
	
	/**
	 * Returns parts of the address before at sign divided by email name separator regex. Initializes the field if it
	 * hasn't been created yet.
	 * @return String[] emailNameParts.
	 */
	public ArrayList<String> getAddresNameParts(){
		if(addresNameParts == null){
			String addresWithoutDomain = getAddresWithotDomain();
			addresNameParts = new ArrayList<String>(Arrays.asList(addresWithoutDomain.split(EMAIL_NAME_SEPARATOR_REGEX)));
		}		
		return addresNameParts;
	}
	
	/**
	 * Returns email address without domain. Initializes the field if it hasn't been created yet.
	 * @return
	 */
	public String getAddresWithotDomain(){
		if(addressWithotDomain == null){
			addressWithotDomain = addres.substring(StringService.FIRST_CHARACTER_POSITION, addres.indexOf(AT_SIGN));
		}
		return addressWithotDomain;
	}

}
