package fido.contacts.merge.data;

import java.util.ArrayList;

import android.util.SparseArray;

/**
 * @author Fido
 *	Service class for collections
 */
public class CollectionService {
	
	/**
	 * Converts {@link SparseArray} to {@link ArrayList}
	 * @param sparseArray {@link SparseArray} to convert.
	 * @return {@link ArrayList} with data from sparse array.
	 */
	public static <C> ArrayList<C> sparseArrayToArrayList(SparseArray<C> sparseArray) {
	    if (sparseArray == null){
	    	return null;
	    }
	    
	    ArrayList<C> arrayList = new ArrayList<C>(sparseArray.size());
	    for (int i = 0; i < sparseArray.size(); i++)
	        arrayList.add(sparseArray.valueAt(i));
	    return arrayList;
	}
}
