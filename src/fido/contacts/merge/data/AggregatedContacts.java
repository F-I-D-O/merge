package fido.contacts.merge.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fido.contacts.merge.data.DataManager.AppMimetypeContract;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.provider.ContactsContract.AggregationExceptions;
import android.util.SparseArray;

public class AggregatedContacts {
	
	/**
	 * Map off aggregated contacts. It's used in searching process
	 */
	private HashMap<NumberPair,AggregatedContact> aggregatedContacts;
	
	/**
	 * List of aggregated contacts. It's used in UI.
	 */
	private ArrayList<AggregatedContact> aggregatedContactsList;
	
	/**
	 * Determines if the content of aggregated contacts list changed. Useful for UI.
	 */
	private boolean aggregatedContactsListChanged;
	
	/**
	 * Collection of aggregated raw contacts. It guarantee that raw contacts in aggregated contacts are not created 
	 * multiple times.
	 */
	private SparseArray<RawContact> aggregatedRawContacts;
	
	private DataManager dataManager;
	
	
	public boolean isAggregatedContactsListChanged() {
		return aggregatedContactsListChanged;
	}
	

	public AggregatedContacts(DataManager dataManager) {
		this.dataManager = dataManager;
		aggregatedContacts = new HashMap<NumberPair,AggregatedContact>();
		aggregatedRawContacts =  new SparseArray<RawContact>();
	}
	
	
	public void clearAggregatedContactsListChanged() {
		aggregatedContactsListChanged = false;
	}
	
	public void findAggregatedContacts() {
		Cursor aggregatedContactsCursor = dataManager.queryAllAggregationExceptions();
		aggregatedContactsCursor.moveToFirst();
		do {
			int firstRawContactId = aggregatedContactsCursor.getInt(
					aggregatedContactsCursor.getColumnIndex(AggregationExceptions.RAW_CONTACT_ID1));
			int secondRawContactId = aggregatedContactsCursor.getInt(
					aggregatedContactsCursor.getColumnIndex(AggregationExceptions.RAW_CONTACT_ID2));
			addAggregatedContact(firstRawContactId, secondRawContactId);
		} while(aggregatedContactsCursor.moveToNext());
		fillAggregatedRawContacts();
		aggregatedContactsList = new ArrayList<AggregatedContact>(aggregatedContacts.values());
		Iterator<AggregatedContact> it = aggregatedContactsList.iterator();
		while (it.hasNext()) {
			AggregatedContact aggregatedContact = it.next(); 
			if(aggregatedContact.getFirstRawContact().getDisplayName() == null || 
					aggregatedContact.getSecondRawContact().getDisplayName() == null){
				it.remove();
			}
		}	
	}	
	
	public ArrayList<AggregatedContact> getAggregatedContactsList() {
		return aggregatedContactsList;
	}
	
	public void removeMergedContact(AggregatedContact aggregatedContact) {
		aggregatedContactsList.remove(aggregatedContact);
		aggregatedContactsListChanged = true;
	}


	
	
	/**
	 * Adds aggregated contact to aggregatedContacts collection.
	 * @param firstRawContactId id of the first raw contact
	 * @param secondRawContactId id of the second raw contact
	 */
	private void addAggregatedContact(int firstRawContactId, int secondRawContactId) {
		NumberPair rawContactIdPair = new NumberPair(firstRawContactId, secondRawContactId);
		if(!aggregatedContacts.containsKey(rawContactIdPair)){
			RawContact firstRawContact = getOrSetRawContact(firstRawContactId);
			RawContact secondRawContact= getOrSetRawContact(secondRawContactId);
			AggregatedContact aggregatedContact = new AggregatedContact(firstRawContact, secondRawContact); 
			aggregatedContacts.put(rawContactIdPair, aggregatedContact);
		}		
	}
	
	/**
	 * Gets raw contact by id. if contact isn't in any aggregation yet, it will be added to list.
	 * @param rawContactId id of the raw contact
	 * @return  raw contact
	 */
	private RawContact getOrSetRawContact(int rawContactId) {
		RawContact contact = aggregatedRawContacts.get(rawContactId);
		if(contact == null){
			contact = new RawContact(rawContactId);
			aggregatedRawContacts.put(rawContactId, contact);
		}
		return contact;
	}
	
	/**
	 * Fill all aggregated contacts with data for UI. 
	 */
	private void fillAggregatedRawContacts(){
		
		//	If there are no contacts to aggregate	
		if(aggregatedContacts.size() < 1){
			return;
		}
		else{
			String rawContactsInSelectionString = TableService.buildInString(aggregatedRawContacts);
			fillAggregatedRawContactsFromRawContactTable(rawContactsInSelectionString);
			fillAggregatedRawContactsFromDataTable(rawContactsInSelectionString);
		}
	}
	
	private void fillAggregatedRawContactsFromDataTable(String rawContactsInSelectionString) {	
		Cursor aggregatedContactsData = 
				dataManager.dataService.queryAggregatedContactsData(rawContactsInSelectionString);
		aggregatedContactsData.moveToFirst();		
		do{
			RawContact contact = aggregatedRawContacts.get(aggregatedContactsData.getInt(
					aggregatedContactsData.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)));
			String mimetype = aggregatedContactsData.getString(aggregatedContactsData.getColumnIndex(
					ContactsContract.Data.MIMETYPE));
			if(mimetype.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)){
				contact.setPhoto(aggregatedContactsData.getBlob(aggregatedContactsData.getColumnIndex(
						ContactsContract.CommonDataKinds.Photo.PHOTO)));
			}
			else if(mimetype.equals(AppMimetypeContract.MIMETYPE)){
				int firstRawContactId = aggregatedContactsData.getInt(aggregatedContactsData.getColumnIndex(
						ContactsContract.Data.RAW_CONTACT_ID));
				int secondRawContactId = aggregatedContactsData.getInt(aggregatedContactsData.getColumnIndex(
						AppMimetypeContract.AGGREGATED_WITH));
				AggregatedContact aggregatedContact = 
						aggregatedContacts.get(new NumberPair(firstRawContactId, secondRawContactId));
				if(aggregatedContact != null){
					aggregatedContact.setAggregatedWithApp(true);
				}			
			}
			else{
				System.out.println("chyba");
			}
		}while(aggregatedContactsData.moveToNext());
		aggregatedContactsData.close();	
	}

	private void fillAggregatedRawContactsFromRawContactTable(String rawContactsInSelectionString) {
		Cursor aggregatedContactsData = 
				dataManager.rawContactService.queryAggregatedContactsRawContact(rawContactsInSelectionString);
		aggregatedContactsData.moveToFirst();		
		do{
			RawContact contact = aggregatedRawContacts.get(aggregatedContactsData.getInt(
					aggregatedContactsData.getColumnIndex(ContactsContract.RawContacts._ID)));
			contact.setDisplayName(aggregatedContactsData.getString(aggregatedContactsData.getColumnIndex(
					ContactsContract.Contacts.DISPLAY_NAME)));	
		}while(aggregatedContactsData.moveToNext());
		aggregatedContactsData.close();
	}

}
