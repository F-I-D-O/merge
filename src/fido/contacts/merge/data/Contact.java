package fido.contacts.merge.data;

import java.util.ArrayList;

import fido.contacts.merge.App;
import fido.contacts.merge.R;

import android.graphics.Bitmap;
import android.util.SparseArray;

/**
 * @author Fido
 * This class represents single contact.
 */
public class Contact {
	
	/*
	 * STATIC
	 */
	
	private static final String LOG_TAG = "Contact object";
	
	private static final String ERROR_MISSING_DISPLAY_NAME = "Contact missing display name. Contact id = ?";
	
	private static String missingDisplayName;
	
	public static void setMissingDisplayName(String missingDisplayName) {
		Contact.missingDisplayName = missingDisplayName;
	}
	
	/*
	 * INSTANCE
	 */

	private int id;	
	
	private SparseArray<RawContact> rawContacts;
	
	private ArrayList<RawContact> rawContactsList;
	
	private RawContact displayNameRawContact;
	
	private RawContact photoRawContact;
	
	

	public int getId() {
		return id;
	}

	public Contact(int contactId) {
		this.id = contactId;
		rawContacts = new SparseArray<RawContact>();
	}
	
	private void addRawContact(RawContact rawContact){
		rawContacts.put(rawContact.getRawContactId(), rawContact);
	}
	
	public ArrayList<RawContact> getRawContacts() {
		return rawContactsList;
	}
	
	public RawContact getRawContact(int rawContactId){
		return rawContacts.get(rawContactId);
	}
	
	public String getDisplayName(){
		try {
			return displayNameRawContact.getDisplayName();
		} catch (NullPointerException e) {	
			App.get().getLogManager().logError(LOG_TAG, ERROR_MISSING_DISPLAY_NAME.replace("?", Integer.toString(id)));
			return missingDisplayName;
		}	
	}

	public void setDisplayName(String displayName) {
		for (RawContact rawContact : rawContactsList) {
		   if(rawContact.getDisplayName().equals(displayName)){
			   displayNameRawContact = rawContact;
			   break;
		   }
		}
		System.out.println(displayName);
		if(displayNameRawContact == null){
			System.out.println("|" + displayName + "|");
		}
	}
	
	public Bitmap getPhoto(){
		if(photoRawContact != null){
			return photoRawContact.getPhoto();
		}
		else {
			return null;
		}
	}
	
	public void setPhotoRawContact(RawContact photoRawContact){
		this.photoRawContact = photoRawContact;  
	}

	public void intitRawContactList() {
		rawContactsList = CollectionService.sparseArrayToArrayList(rawContacts);	
	}

	public RawContact getOrSetRawContact(int rawContactId) {
		RawContact rawContact = getRawContact(rawContactId);
		if(rawContact == null){
			rawContact = new RawContact(rawContactId);
			addRawContact(rawContact);
		}
		return rawContact;
	}
	
	public boolean isPhotoPrimary(){
		return photoRawContact.isPhotoPrimary();
	}
	
	public boolean isPhotoSuperPrimary(){
		return photoRawContact.isPhotoSuperPrimary();
	}

}
