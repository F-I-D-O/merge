package fido.contacts.merge.data;

import java.util.ArrayList;
import java.util.Arrays;

import fido.contacts.merge.StringService;

/**
 * @author Fido
 * Contact data class for structuredName mimetype.
 */
public class Name extends ModuleData {
	
	/**
	 * parts of the name
	 */
	private ArrayList<String> parts;

	
	
	
	public ArrayList<String> getParts() {
		return parts;
	}

	
	
	
	/**
	 * Constructor. Save the name divided to the parts }unlike email which is divided on demand).
	 * @param lookupKey Lookup key of the contact.
	 * @param contactId ContactId of the contact.
	 * @param addres Email address.
	 */
	public Name(String lookupKey, int contactId, int rawContactId, String nameString) {
		super(lookupKey, contactId, rawContactId);
		parseNameString(nameString);
	}
	
	
	

	/**
	 * Divide name into name parts.
	 * @param nameString
	 */
	private void parseNameString(String nameString) {
		nameString = nameString.trim();
		parts = new ArrayList<String>(Arrays.asList(nameString.split(StringService.WHITESPACES)));
	}
}
