package fido.contacts.merge.data;

import fido.contacts.merge.data.DataManager.AppMimetypeContract;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;

public class DataService extends TableService {

	/**
	 * Constructor
	 * @param contentResolver content resolver;
	 */
	public DataService(ContentResolver contentResolver) {
		super(contentResolver);
	}
	
	/**
	 * Query data for aggregated contacts.
	 * @param rawContactsInSelectionString 
	 * @return Cursor with raw contact data for aggregated contacts.
	 */
	protected Cursor queryAggregatedContactsData(String rawContactsInSelectionString){
		String[] projection = new String[]{	ContactsContract.Data.RAW_CONTACT_ID,
											ContactsContract.Data.MIMETYPE,
											ContactsContract.CommonDataKinds.Photo.PHOTO,
											AppMimetypeContract.AGGREGATED_WITH};
		String selection = ContactsContract.Data.RAW_CONTACT_ID + " IN " + rawContactsInSelectionString + 
				" AND " + ContactsContract.Data.MIMETYPE + " IN " + 
				buildInString(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE,
					DataManager.AppMimetypeContract.MIMETYPE);
		return queryData(projection, selection);
	}
	
	/**
	 * Returns cursor with raw contact data for virtual contacts contained in aggregation candidates. These data are
	 * used in UI.
	 * @param rawContactIdInStatementString2 
	 * @return cursor with data from data table for filling aggregation candidates
	 */
	Cursor queryAggregationCandidatesData(String contactIdInStatementString) {
		String[] projection = new String[]{	ContactsContract.Data._ID,   
											ContactsContract.Data.RAW_CONTACT_ID,
											ContactsContract.Data.CONTACT_ID,
											ContactsContract.Data.MIMETYPE,
											ContactsContract.Data.DATA1,
											ContactsContract.Data.DATA14,  // for photo 
											ContactsContract.Data.DATA15,  // for photo
											ContactsContract.Data.IS_PRIMARY,
											ContactsContract.Data.IS_SUPER_PRIMARY};
		String selection = ContactsContract.Data.CONTACT_ID + " IN " + contactIdInStatementString
				 + " AND " + ContactsContract.Data.MIMETYPE + " IN " + buildInString(StructuredName.CONTENT_ITEM_TYPE,
						Photo.CONTENT_ITEM_TYPE, Phone.CONTENT_ITEM_TYPE, Email.CONTENT_ITEM_TYPE); 
		return queryData(projection, selection);
	}
	
	/**
	 * Query data from ContactsContract.Data table corresponding given projection and selection.
	 * @param projection Defines which columns we want.
	 * @param selection Defines which rows we want.
	 * @return Cursor with chosen data.
	 */
	public Cursor queryData(String[] projection, String selection)
    {
    	Uri uri = ContactsContract.Data.CONTENT_URI;
    	selection += " AND " + ContactsContract.Contacts.IN_VISIBLE_GROUP + " = 1 AND " +
    				 	ContactsContract.Contacts.DISPLAY_NAME + " IS NOT NULL";
    	return query(uri, projection, selection);
    }
}
