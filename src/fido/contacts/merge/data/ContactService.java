package fido.contacts.merge.data;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

public class ContactService extends TableService {

	public ContactService(ContentResolver contentResolver) {
		super(contentResolver);
	}
	
	/**
	 * Method for querying data from ContactsContract.Contacts table.
	 * @param projection Defines which columns we want.
	 * @param selection Defines which rows we want.
	 * @return Cursor with chosen data.
	 */
	public Cursor queryContact(String[] projection, String selection){
    	Uri uri = ContactsContract.Contacts.CONTENT_URI;
    	selection += " AND " + ContactsContract.Contacts.IN_VISIBLE_GROUP + " = 1 AND " +
			 	ContactsContract.Contacts.DISPLAY_NAME + " IS NOT NULL";
    	return query(uri, projection, selection);
    }
	
	/**
	 * Gets Cursor with contact display name and photoId based on contactId.
	 * @param contactId - contact ID
	 * @return {@link Cursor} with contact display name and photoId.
	 */
	public Cursor queryContactDisplayNameAndPhotoId(int contactId) {
		String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.PHOTO_ID};
		String selection = ContactsContract.Contacts._ID + " = " + contactId;
		Cursor contactCursor = queryContact(projection, selection);
		contactCursor.moveToFirst();
		return contactCursor;
	}
	
	/**
	 * Method for querying displayName based on contact's lookupKey.
	 * @param lookupKey Contact's lookupKey.
	 * @return Cursor with contact's displayName
	 */
	public Cursor queryDisplayName (String lookupKey){
		String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
		String selection = ContactsContract.Data.LOOKUP_KEY + " = '" + lookupKey + "'";
		return queryContact(projection, selection);
	}

	public Cursor queryAggregationCandidatesContacts(String contactIdInStatementString) {
		String[] projection = new String[]{
				ContactsContract.Contacts._ID,
				ContactsContract.Contacts.DISPLAY_NAME							};
		String selection = ContactsContract.Contacts._ID + " IN " + contactIdInStatementString;
		System.out.println(contactIdInStatementString);
		return queryContact(projection, selection);
	}

}
