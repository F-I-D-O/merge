package fido.contacts.merge.data;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class RawContact {
	
	/**
	 * Id of the raw contact
	 */
	private int rawContactId;
	
	/**
	 * Primary display name for raw contact
	 */
	private String displayName;
	
	/**
	 * Primary photo for raw contact
	 */
	private Bitmap photo;
	
	/**
	 * URI string representing path to the primary photo file.
	 */
	private String photoUri;
	
	/**
	 * Id of the data row of the contacts's photo.
	 */
	private int photoId;
	
	private AccountType accountType;
	
	private String accountName;
	
	private boolean displayNamePrimary;

	private boolean photoPrimary;
	
	private boolean photoSuperPrimary;
	
	private ArrayList<String> phoneNumbers;
	
	private ArrayList<String> emails;
	
	public int getRawContactId() {
		return rawContactId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Bitmap getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photoData) {
		if(photoData != null){
			this.photo = BitmapFactory.decodeStream(new ByteArrayInputStream(photoData));
		}	
	}

	public String getPhotoUri() {
		return photoUri;
	}

	public int getPhotoId() {
		return photoId;
	}

	public void setPhotoId(int photoId) {
		this.photoId = photoId;
	}

	public String getAccountType() {
		return accountType.getTestString();
	}
	
	public int getAccountTypeIconId() {
		return accountType.getIcon();
	}

	public void setAccountType(String accountTypeString) {
		this.accountType = AccountType.getAccountType(accountTypeString);
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public boolean isDisplayNamePrimary() {
		return displayNamePrimary;
	}

	public void setDisplayNamePrimary(boolean displayNamePrimary) {
		this.displayNamePrimary = displayNamePrimary;
	}

	public boolean isPhotoPrimary() {
		return photoPrimary;
	}

	public void setPhotoPrimary(boolean photoPrimary) {
		this.photoPrimary = photoPrimary;
	}

	public boolean isPhotoSuperPrimary() {
		return photoSuperPrimary;
	}

	public void setPhotoSuperPrimary(boolean photoSuperPrimary) {
		this.photoSuperPrimary = photoSuperPrimary;
	}

	public ArrayList<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	public ArrayList<String> getEmails() {
		return emails;
	}
	
	

	public RawContact(int rawContactId) {
		this.rawContactId = rawContactId;
	}
	
	
	public void addPhoneNumber(String phoneNumber){
		if (phoneNumbers == null) {
			phoneNumbers = new ArrayList<String>();
		}
		phoneNumbers.add(phoneNumber);
	}
	
	public void addEmail(String email){
		if (emails == null) {
			emails = new ArrayList<String>();
		}
		emails.add(email);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + rawContactId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null || getClass() != obj.getClass()){
			return false;
		}
		RawContact other = (RawContact) obj;
		if (rawContactId != other.rawContactId){
			return false;
		}		
		return true;
	}

	public void fillRawContact(String displayName, String photoUri, int photoId) {
		this.displayName = displayName;
		this.photoUri = photoUri;
		this.photoId = photoId;
	}

	
}
