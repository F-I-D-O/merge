package fido.contacts.merge.data;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * @author Fido
 * Service class for raw contact table.
 */
public class RawContactService extends TableService {

	/**
	 * Constructor
	 * @param contentResolver content resolver;
	 */
	public RawContactService(ContentResolver contentResolver) {
		super(contentResolver);
	}
	
	/**
	 * Query raw contact data for aggregated contacts.
	 * @param rawContactsInSelectionString 
	 * @return Cursor with raw contact data for aggregated contacts.
	 */
	public Cursor queryAggregatedContactsRawContact(String rawContactsInSelectionString) {
		String[] projection = new String[]{
				ContactsContract.RawContacts._ID,
				ContactsContract.Contacts.DISPLAY_NAME   
		};
		
		String selection = ContactsContract.RawContacts._ID + " IN " + rawContactsInSelectionString; 
		return queryRawContact(projection, selection);
	}
	
	/**
	 * Query data for raw contacts contained in virtual contacts
	 * @param contactIdInStatementString
	 * @return cursor with raw contact data for aggregation candidates.
	 */
	Cursor queryAggregationCandidatesRawContacts(String contactIdInStatementString) {
		String[] projection = new String[]{
				ContactsContract.RawContacts._ID,
				ContactsContract.RawContacts.CONTACT_ID,
				ContactsContract.RawContacts.ACCOUNT_TYPE,
				ContactsContract.RawContacts.ACCOUNT_NAME,
				ContactsContract.Contacts.DISPLAY_NAME    
		};
		
		String selection = ContactsContract.RawContacts.CONTACT_ID + " IN " + contactIdInStatementString; 
		return queryRawContact(projection, selection);
	}
	
	/**
	 * Returns a cursor with all raw contacts filled with contact id and raw contact id.
	 * @return Cursor with raw contacts.
	 */
	Cursor queryAllRawContactsForAggregatioCandidates(){
		String[] projection = new String[]{ContactsContract.RawContacts.CONTACT_ID, ContactsContract.RawContacts._ID};
		return queryRawContact(projection, null);
	}
	
	
	
	
	/**
	 * Method for querying data from ContactsContract.RawContacts table.
	 * @param projection Defines which columns we want.
	 * @param selection Defines which rows we want.
	 * @return Cursor with chosen data.
	 */
	Cursor queryRawContact(String[] projection, String selection){
    	Uri uri = ContactsContract.RawContacts.CONTENT_URI;
    	return query(uri, projection, selection);
    }

	

}
