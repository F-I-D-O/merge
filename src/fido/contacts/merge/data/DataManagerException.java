package fido.contacts.merge.data;

/**
 * @author Fido
 * Exception for {@link DataManager}.
 */
public class DataManagerException extends Exception {
	private static final long serialVersionUID = -471943954327167777L;

	/**
	 * Text for all exceptions.
	 */
	private static final String EXCEPTION_TEXT = "Data manager exception: ";
	
	/**
	 * Text if {@link DataManager} has been already created.
	 */
	public static final String MANAGER_ALREADY_CREATED = "Data manager has been already created!";	
	
	/**
	 * Text if {@link DataManager} has not been initialized yet.
	 */
	public static final String MANAGER_NULL = "Data manager has not been created!";
	
	/**
	 * Constructor.
	 * @param message chosen message text.
	 */
	public DataManagerException(String message) {
		super(EXCEPTION_TEXT + message);
	}
}
