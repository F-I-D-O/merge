package fido.contacts.merge.data;

/**
 * @author Fido
 * Ancestor class for all data classes representing rows from Data table.
 */
public abstract class ModuleData {
	
	/**
	 * Long id of Contact which this data row belongs to. This id dosn't change during contact lifetime (unlike _ID).
	 */
	private String lookupKey;
	
	/**
	 * Id of Contact which this data row belongs to.
	 */
	private int contactId;
		
	/**
	 * Id of the raw contact which this data row belongs to.
	 */
	private int rawContactId;
	
	public String getLookupKey() {
		return lookupKey;
	}
	
	public int getContactId() {
		return contactId;
	}

	public int getRawContactId() {
		return rawContactId;
	}
	
	
	/**
	 * Constructor
	 * @param lookupKey Long id of Contact which this data row belongs to. This id dosn't change during contact lifetime (unlike _ID).
	 * @param contactId Id of the contact which this data row belongs to.
	 * @param rawContactId Id of the raw contact which this data row belongs to.
	 */
	protected ModuleData(String lookupKey, int contactId, int rawContactId){
		this.lookupKey = lookupKey;
		this.contactId = contactId;
		this.rawContactId = rawContactId;
	}



}
