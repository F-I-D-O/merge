package fido.contacts.merge.data;

import java.util.ArrayList;

/**
 * @author Fido
 * Holds data that is required for aggregating two raw contacts.
 */
public class AggregationCandidate {
	
	private ArrayList<Contact> contacts;
	
	/**
	 * First raw contact for aggregation.
	 */
	private RawContact firstRawContact;
	
	/**
	 * Second raw contact for aggregation
	 */
	private RawContact secondRawContact;
	
	/**
	 * Virtual contact for presenting first {@link RawContact}
	 */
	private Contact firstContact;
	
	/**
	 * Virtual contact for presenting second {@link RawContact}
	 */
	private Contact secondContact;
	
	/**
	 * List of methods that have found contacts as aggregation candidates.
	 */
	private ArrayList<Integer> methods;
	
	/**
	 * Determine if aggregation is approved by the user.
	 */
	private boolean mergeApproved;
	
	/**
	 * Raw contact that's name was chosen by user for the aggregated contact
	 */
	private RawContact chosenNameRawContact;
	
	/**
	 * Raw contact that's photo was chosen by user for the aggregated contact
	 */
	private RawContact chosenPhotoRawContact;
	
	private int hashCode;
	
	private int group;
	
	
	
	
	public RawContact getFirstRawContact() {
		return firstRawContact;
	}

	public RawContact getSecondRawContact() {
		return secondRawContact;
	}

	public Contact getFirstContact() {
		return firstContact;
	}

	public Contact getSecondContact() {
		return secondContact;
	}

	public ArrayList<Integer> getMethods() {
		return methods;
	}

	public boolean isMergeApproved() {
		return mergeApproved;
	}

	public void setMergeApproved(boolean merge) {
		this.mergeApproved = merge;
	}
	
	public RawContact getChosenNameContact() {
		return chosenNameRawContact;
	}

	public void setChosenNameContact(RawContact chosenNameRawContact) {
		this.chosenNameRawContact = chosenNameRawContact;
	}

	public RawContact getChosenPhotoContact() {
		return chosenPhotoRawContact;
	}

	public void setChosenPhotoContact(RawContact chosenPhotoRawContact) {
		this.chosenPhotoRawContact = chosenPhotoRawContact;
	}

	
	
	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	/**
	 * Constructor. It sets up only raw contacts and first method. Virtual contacts are filled later, after 
	 * searching ends.
	 * @param firstRawContact First {@link RawContact} to aggregate.
	 * @param secondRawContact Second {@link RawContact} to aggregate.
	 * @param methodStringID ID of a string that identifies search method that has founded the match.
	 * @param secondContact first virtual contact
	 * @param firstContact second virtual contact
	 */
	public AggregationCandidate(RawContact firstRawContact, RawContact secondRawContact, int methodStringID, 
			Contact firstContact, Contact secondContact) {
		this.firstRawContact = firstRawContact;
		this.secondRawContact = secondRawContact;
		methods = new ArrayList<Integer>();
		methods.add(methodStringID);
		this.firstContact = firstContact;
		this.secondContact = secondContact;
		hashCode = firstRawContact.hashCode() + secondRawContact.hashCode();
		
		//	first contact is default for name and image	
		chosenNameRawContact = firstRawContact;
		chosenPhotoRawContact = firstRawContact;
	}
	
	
	
	
	/**
	 * Adds an ID of the search method to the list.
	 * @param methodID ID of a string that identifies search method that has founded the match.
	 */
	public void addMethod(int methodID){
		methods.add(methodID);
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof AggregationCandidate){			
			AggregationCandidate secondCandidate = (AggregationCandidate) o;
			if(hashCode == secondCandidate.hashCode){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

	public boolean shareRawContactWith(AggregationCandidate secondAggregationCandidate) {
		if(firstRawContact.equals(secondAggregationCandidate.firstRawContact) || 
				firstRawContact.equals(secondAggregationCandidate.secondRawContact) ||
				secondRawContact.equals(secondAggregationCandidate.firstRawContact) ||
				secondRawContact.equals(secondAggregationCandidate.secondRawContact)){
			return true;
		}
		else{
			return false;
		}
	}

	public ArrayList<Contact> getContacts() {
		if(contacts == null){
			contacts = new ArrayList<Contact>();
			contacts.add(firstContact);
			contacts.add(secondContact);
		}
		return contacts;
	}
	
}
