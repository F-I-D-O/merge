package fido.contacts.merge.data;

import java.util.Arrays;
import java.util.HashSet;

import fido.contacts.merge.StringService;

public class PhoneNumber extends ModuleData {
		
	/**
	 * Position of the first number if country calling code is present.
	 */
	private static final int FIRST_NUMBER_POSITION = 1;
	
	/**
	 * Position of the first number in country calling code.
	 */
	private static final int COUNTRY_CALLING_CODE_FIRST_NUMBER_POSITION = 0;
	
	/**
	 * Position of the second number in country calling code.
	 */
	private static final int COUNTRY_CALLING_CODE_SECOND_NUMBER_POSITION = 1;
	
	/**
	 * Position of the third number in country calling code.
	 */
	private static final int COUNTRY_CALLING_CODE_THIRD_NUMBER_POSITION = 2;
	
	/**
	 * Position of the fourth number in country calling code.
	 */
	private static final int COUNTRY_CALLING_CODE_FOURTH_NUMBER_POSITION = 2;
	
	/**
	 * Country calling code prefix.
	 */
	private static final char COUNTRY_CALLING_CODE_SIGN = '+';
	
	/**
	 * Country calling code of north American territory
	 */
	private static final char COUNTRY_CALLING_CODE_NORTH_AMERICA = '1';
	
	/**
	 * Country calling codes of the countries that have two-digit country calling code.
	 */
	private static final HashSet<String> COUNTRY_CALLING_CODES_TWO_DIGITS = new HashSet<String>(Arrays.asList(
		new String[]{"20", "27", "28", "30", "31", "32", "33", "34", "36", "39", "40", "41", "43", "44", "45", "46", 
			"47", "48", "49", "51", "52", "53", "54", "55", "56", "57", "58", "60", "61", "62", "63", "64", "65", "66",
			"81", "82", "83", "84", "86", "89", "90", "91", "92", "93", "94", "95", "98"}));
	
	
	
	
	/**
	 * Phone number
	 */
	private String phoneNumber;
	
	/**
	 * Country calling code.
	 */
	private String countryCallingCode;

	
	
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getCountryCallingCode() {
		return countryCallingCode;
	}
	
	
	
	
	/**
	 * Constructor
	 * @param lookupKey Lookup key of the contact.
	 * @param contactId ContactId of the contact.
	 * @param phoneNumberString Phone number with prefix.
	 * @param splitPrefix If country calling code should be separated from the number.
	 */
	public PhoneNumber(String lookupKey, int contactId, int rawContactId, String phoneNumberString, boolean splitPrefix) {
		super(lookupKey, contactId, rawContactId);
		parsePhoneNumber(phoneNumberString, splitPrefix);
	}

	/**
	 * Parse phone number. If splitPrefix is false or the phoneNumberString comes without country calling code, 
	 * it only save the number to the phoneNumber field.
	 * @param phoneNumberString Phone number with prefix.
	 * @param splitPrefix If country calling code should be separated from the number.
	 */
	private void parsePhoneNumber(String phoneNumberString, boolean splitPrefix) {
		phoneNumberString = phoneNumberString.replaceAll(StringService.WHITESPACES, "");
		if (splitPrefix && phoneNumberString.charAt(StringService.FIRST_CHARACTER_POSITION) == 
				COUNTRY_CALLING_CODE_SIGN){
			phoneNumberString = phoneNumberString.substring(FIRST_NUMBER_POSITION);
			if(phoneNumberString.charAt(StringService.FIRST_CHARACTER_POSITION) == 
					COUNTRY_CALLING_CODE_NORTH_AMERICA){
				parsePhoneNumberWithPrefix(phoneNumberString, COUNTRY_CALLING_CODE_FOURTH_NUMBER_POSITION);
			}
			else if(COUNTRY_CALLING_CODES_TWO_DIGITS.contains(phoneNumberString.substring(
					COUNTRY_CALLING_CODE_FIRST_NUMBER_POSITION, COUNTRY_CALLING_CODE_SECOND_NUMBER_POSITION))){
				parsePhoneNumberWithPrefix(phoneNumberString, COUNTRY_CALLING_CODE_SECOND_NUMBER_POSITION);
			}
			else {
				parsePhoneNumberWithPrefix(phoneNumberString, COUNTRY_CALLING_CODE_THIRD_NUMBER_POSITION);
			}
		}
		else {
			countryCallingCode = "";
			phoneNumber = phoneNumberString;
		}
		
	}
	
	/**
	 * Separate country calling code from the number, and save it to instance fields.
	 * @param phoneNumberString String with phone number already without spaces.
	 * @param countryCallingCodeLastNumberPosition position of the last character of the country calling code in the 
	 * phoneNumberString. 
	 */
	private void parsePhoneNumberWithPrefix(String phoneNumberString, int countryCallingCodeLastNumberPosition) {
		countryCallingCode = phoneNumberString.substring(COUNTRY_CALLING_CODE_FIRST_NUMBER_POSITION, 
				countryCallingCodeLastNumberPosition + 1);
		phoneNumber = phoneNumberString.substring(countryCallingCodeLastNumberPosition + 1);
	}
	
}
