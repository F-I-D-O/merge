package fido.contacts.merge.ui.help;

import fido.contacts.merge.R;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public abstract class HelpLayout extends LinearLayout {
	
	private static final int FIRST_SCREEN = 0;
	
	private int activeScreen;
	
	private Context context;
	
	private Integer[] helpScreans;
	
	private FrameLayout parent;
	
	private LinearLayout helpContent;
	
	private Button nextButton;
	
	private Button prevButton;
	
	private Button closeButton;

	public HelpLayout(Context context, FrameLayout parent) {
		super(context);
		this.context = context;
		this.parent = parent;
		helpScreans = getScreens();
		
		// does not work without orientation set!!
		setOrientation(LinearLayout.VERTICAL);
		
		setBackgroundColor(getResources().getColor(R.color.helpWrapBackground));
		
		inflate(context, R.layout.help_wrap, this);
		initButtons();
		helpContent = (LinearLayout) findViewById(R.id.helpContent);
		activeScreen = FIRST_SCREEN;
		loadHelpContent();
	}
	
	private void initButtons() {
		nextButton = (Button) findViewById(R.id.buttonNext);
		prevButton = (Button) findViewById(R.id.buttonPrevious);
		closeButton = (Button) findViewById(R.id.buttonClose);
		
		nextButton.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				nextScreen();
			}
		});
		
		prevButton.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				previousScreen();
			}
		});
		
		closeButton.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				end();
			}
		});
		
	}

	private void loadHelpContent(){
		helpContent.removeAllViews();
		inflate(context, helpScreans[activeScreen], helpContent);
		helpContent.invalidate();
		if(activeScreen == FIRST_SCREEN){
			prevButton.setEnabled(false);
		}
		else{
			prevButton.setEnabled(true);
		}
		if(activeScreen == helpScreans.length - 1){
			nextButton.setEnabled(false);
		}
		else{
			nextButton.setEnabled(true);
		}
	}
	
	private void end(){
		parent.removeView(this);
	}
	
	private void nextScreen(){
		activeScreen++;
		loadHelpContent();
	}
	
	private void previousScreen(){
		activeScreen--;
		loadHelpContent();
	}
	
	
	
	protected abstract Integer[] getScreens();

	
}
