package fido.contacts.merge.ui.help;

import fido.contacts.merge.R;
import android.content.Context;
import android.widget.FrameLayout;

public class HelpLayoutOptions extends HelpLayout {
	
	private static final Integer[] HELP_SCREENS = {R.layout.help_options_1, R.layout.help_options_2};

	public HelpLayoutOptions(Context context, FrameLayout parent) {
		super(context, parent);
	}

	@Override
	protected Integer[] getScreens() {
		return HELP_SCREENS;
	}

}
