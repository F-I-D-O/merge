package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.data.RawContact;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;

public class RawContactUI {
	
	public static final int ITEM_HEIGHT = 45;
	
	public static RelativeLayout getItem(RawContact rawContact, RelativeLayout rawContactItem){
//		rawContactItem.getLayoutParams().height = ITEM_HEIGHT;
		
//		LayoutParams layoutParams = rawContactItem.getLayoutParams() != null ? rawContactItem.getLayoutParams() :
//			new LayoutParams(LayoutParams.MATCH_PARENT, 0);		
//		layoutParams.height = ITEM_HEIGHT;
//		rawContactItem.setLayoutParams(layoutParams);
		
		LayoutParams layoutParams = rawContactItem.getLayoutParams();
		if(layoutParams == null){
			rawContactItem.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, ITEM_HEIGHT));
		}
		
		final TextView rawContactNameView = (TextView) rawContactItem.findViewById(R.id.name);
		final ImageView rawContactAccountTypeView = (ImageView) rawContactItem.findViewById(R.id.accountType);
		
		rawContactNameView.setText(rawContact.getDisplayName());
		rawContactAccountTypeView.setImageResource(rawContact.getAccountTypeIconId());
		
		return rawContactItem;
	}

}
