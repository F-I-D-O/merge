package fido.contacts.merge.ui;

import fido.contacts.merge.ImageCommon;
import fido.contacts.merge.MergeThread;
import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregationCandidate;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MergeActivity extends Activity implements IAsyncWait {
	
	public static final int RESULT_MERGE_COMPLETE = 1;
	
	private DataManager dataManager;
	
	private AggregationCandidate aggregationCandidate;
	
	private Drawable defaultContactImage;

	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        defaultContactImage = ImageCommon.resize(getResources().getDrawable(R.drawable.contactus_icon), 
				this);
        
        setContentView(R.layout.activity_merge);
        
        try {
			dataManager = DataManager.get();
			aggregationCandidate = dataManager.getIndividualAggregation();
			
			RadioButton displayName1 = (RadioButton) this.findViewById(R.id.radioDisplayName1);
			RadioButton displayName2 = (RadioButton) this.findViewById(R.id.radioDisplayName2);
			
			displayName1.setText(aggregationCandidate.getFirstContact().getDisplayName());
			displayName2.setText(aggregationCandidate.getSecondContact().getDisplayName());
			
			RadioGroup displayNameGroup = (RadioGroup) this.findViewById(R.id.radioGroup1);
			displayNameGroup.setOnCheckedChangeListener(nameChangeListener);
			
			RadioButton photo1 = (RadioButton) this.findViewById(R.id.radioPhoto1);
			if(aggregationCandidate.getFirstContact().getPhoto() != null){		
				BitmapDrawable image = new BitmapDrawable(getResources(), aggregationCandidate.getFirstContact().getPhoto());
				image = ImageCommon.resize(image, this);
				photo1.setCompoundDrawablesWithIntrinsicBounds( null, null, image, null);
			}
			else{
				photo1.setCompoundDrawablesWithIntrinsicBounds( null, null, defaultContactImage, null);
			}
			
			RadioButton photo2 = (RadioButton) this.findViewById(R.id.radioPhoto2);
			if(aggregationCandidate.getSecondContact().getPhoto() != null){		
				BitmapDrawable image = new BitmapDrawable(getResources(), aggregationCandidate.getSecondContact().getPhoto());
				image = ImageCommon.resize(image, this);
				photo2.setCompoundDrawablesWithIntrinsicBounds( null, null, image, null);
			}
			else{
				photo2.setCompoundDrawablesWithIntrinsicBounds( null, null, defaultContactImage, null);
			}
			
			RadioGroup photoGroup = (RadioGroup) this.findViewById(R.id.radioGroup2);
			photoGroup.setOnCheckedChangeListener(photoChangeListener);
			
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
    }
	
	public void merge(View view){
		aggregationCandidate.setMergeApproved(true);
		MergeThread mThread = new MergeThread(this);
		mThread.execute();
		dataManager.removeAggregationCandidate(aggregationCandidate);
//		this.finish();
	}

	public void asyncCompleted() {
		setResult(RESULT_MERGE_COMPLETE);
		this.finish();
	}
	
	OnCheckedChangeListener nameChangeListener = new OnCheckedChangeListener(){
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			if(checkedId == R.id.radioDisplayName1){
				aggregationCandidate.setChosenNameContact(aggregationCandidate.getFirstRawContact());
			}
			else{
				aggregationCandidate.setChosenNameContact(aggregationCandidate.getSecondRawContact());
			}
			
		}
	};
	
	OnCheckedChangeListener photoChangeListener = new OnCheckedChangeListener(){
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			if(checkedId == R.id.radioPhoto1){
				aggregationCandidate.setChosenPhotoContact(aggregationCandidate.getFirstRawContact());
			}
			else{
				aggregationCandidate.setChosenPhotoContact(aggregationCandidate.getSecondRawContact());
			}
			
		}
	};
}
