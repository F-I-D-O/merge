package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.searchModules.ModuleManagerException;
import fido.contacts.merge.searchModules.ModulesManager;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

public class OptionSettingsActivity extends FragmentActivity {

	private int optionIndex;
	
	public int getOptionIndex() {
		return optionIndex;
	}

	@Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_option_settings);
	        
	        TextView header = (TextView) findViewById(R.id.settingsHeader);
	        try {
				header.setText(getString(R.string.header_settings) + " - " + getString(
						ModulesManager.get().getSearchModules().get(optionIndex).getDisplayNameId())+ " " + 
								getString(R.string.options_activity_module_suffix));
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ModuleManagerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        this.optionIndex = getIntent().getExtras().getInt("OptionIndex");
	    }
}
