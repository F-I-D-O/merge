package fido.contacts.merge.ui;

import java.util.ArrayList;

import fido.contacts.merge.MergeThread;
import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregationCandidate;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

public class MergeMultipleActivity extends Activity implements IAsyncWait {
	
	private MergeMultipleListAdapter adapter;
	
	private ExpandableListView listView;
	
	private ArrayList<AggregationCandidate> approvedAggregationCandidates;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge_multiple);
 
        listView = (ExpandableListView) findViewById(R.id.mergeMultipleList);
        try {
        	approvedAggregationCandidates = DataManager.get().getApprovedAggregationCandidatesList();
			adapter = new MergeMultipleListAdapter(this, approvedAggregationCandidates);
			listView.setAdapter(adapter);
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
	
	public void startMerging(View view){
		MergeThread mThread = new MergeThread(this);
		mThread.execute();
	}

	@Override
	public void asyncCompleted() {
		this.finish();
	}

}
