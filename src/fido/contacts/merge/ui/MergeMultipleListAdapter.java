package fido.contacts.merge.ui;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import fido.contacts.merge.ImageCommon;
import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregationCandidate;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MergeMultipleListAdapter extends BaseExpandableListAdapter {
	
	private ArrayList<AggregationCandidate> aggregationCandidates;
	
	/**
	 * Layout inflater (for not creating it all the time).
	 */
	private LayoutInflater inflater;
	
	private Context context;
	
	private int groupPosition; 
	
	private Drawable defaultContactImage;

	public MergeMultipleListAdapter(Context context, ArrayList<AggregationCandidate> approvedAggregationCandidates) {
		aggregationCandidates = approvedAggregationCandidates;
		
		this.context = context;
		
		defaultContactImage = ImageCommon.resize(context.getResources().getDrawable(R.drawable.contactus_icon), 
				context);
		
		// Cache the LayoutInflate to avoid asking for a new one each time.
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getGroupCount() {
		return aggregationCandidates.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return aggregationCandidates.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return aggregationCandidates.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.group_merge_multiple, null);
        }
 
        TextView firstContactName = (TextView) convertView.findViewById(R.id.firstContactName);
        TextView secondContactName = (TextView) convertView.findViewById(R.id.secondContactName);
//        lblListHeader.setTypeface(null, Typeface.BOLD);
        firstContactName.setText(aggregationCandidates.get(groupPosition).getFirstRawContact().getDisplayName());
        secondContactName.setText(aggregationCandidates.get(groupPosition).getSecondRawContact().getDisplayName());
 
        return convertView;
	}

	@SuppressLint("NewApi")
	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {	 
		if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_merge_multiple, null);
        }
 
		AggregationCandidate aggregationCandidate = aggregationCandidates.get(groupPosition);
		
		RadioButton displayName1 = (RadioButton) convertView.findViewById(R.id.radioDisplayName1);
		RadioButton displayName2 = (RadioButton) convertView.findViewById(R.id.radioDisplayName2);
		
		displayName1.setText(aggregationCandidate.getFirstRawContact().getDisplayName());
		displayName2.setText(aggregationCandidate.getSecondRawContact().getDisplayName());
		
		RadioGroup displayNameGroup = (RadioGroup) convertView.findViewById(R.id.radioGroup1);
		displayNameGroup.setOnCheckedChangeListener(nameChangeListener);
		
		if(aggregationCandidate.getChosenNameContact() == aggregationCandidate.getSecondRawContact()){
			displayName2.setSelected(true);
		}
		
//		ImageView photo1 = (ImageView) convertView.findViewById(R.id.photoView1);
		RadioButton radioPhoto1 = (RadioButton) convertView.findViewById(R.id.radioPhoto1);
		if(aggregationCandidate.getFirstRawContact().getPhotoUri() != null){		
			try {
				InputStream inputStream = context.getContentResolver().openInputStream(
						Uri.parse(aggregationCandidate.getFirstRawContact().getPhotoUri()));
				Drawable image = Drawable.createFromStream(inputStream, aggregationCandidate.getFirstRawContact().getPhotoUri());
//				photo1.setImageDrawable(image);
				image = ImageCommon.resize(image, context);
				radioPhoto1.setCompoundDrawablesWithIntrinsicBounds( null, null, image, null);
			} 
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
//			Resources r = context.getResources();
//			int picId = r.getIdentifier("contactus_icon", "drawable", "fido.contacts.merge");
//			photo1.setImageResource(picId);
			radioPhoto1.setCompoundDrawablesWithIntrinsicBounds( null, null, defaultContactImage, null);
		}
//		ImageView photo2 = (ImageView) convertView.findViewById(R.id.photoView2);
		RadioButton radioPhoto2 = (RadioButton) convertView.findViewById(R.id.radioPhoto2);
		if(aggregationCandidate.getSecondRawContact().getPhotoUri() != null){	
			try {
				InputStream inputStream = context.getContentResolver().openInputStream(
						Uri.parse(aggregationCandidate.getSecondRawContact().getPhotoUri()));
				Drawable image = Drawable.createFromStream(inputStream, aggregationCandidate.getSecondRawContact().getPhotoUri());
				image = ImageCommon.resize(image, context);
//				photo2.setImageDrawable(image);
				radioPhoto2.setCompoundDrawablesWithIntrinsicBounds( null, null, image, null);
			} 
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
//			Resources r = context.getResources();
//			int picId = r.getIdentifier("contactus_icon", "drawable", "fido.contacts.merge");
//			photo2.setImageResource(picId);
			radioPhoto2.setCompoundDrawablesWithIntrinsicBounds( null, null, defaultContactImage, null);
		}
		
		RadioGroup photoGroup = (RadioGroup) convertView.findViewById(R.id.radioGroup2);
		photoGroup.setOnCheckedChangeListener(photoChangeListener);
		
		if(aggregationCandidate.getChosenPhotoContact() == aggregationCandidate.getSecondRawContact()){
			radioPhoto2.setSelected(true);
		}
        
        return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
	
	OnCheckedChangeListener nameChangeListener = new OnCheckedChangeListener(){
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			AggregationCandidate aggregationCandidate = aggregationCandidates.get(groupPosition);
			if(checkedId == R.id.radioDisplayName1){
				aggregationCandidate.setChosenNameContact(aggregationCandidate.getFirstRawContact());
			}
			else{
				aggregationCandidate.setChosenNameContact(aggregationCandidate.getSecondRawContact());
			}
			
		}
	};
	
	OnCheckedChangeListener photoChangeListener = new OnCheckedChangeListener(){
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			AggregationCandidate aggregationCandidate = aggregationCandidates.get(groupPosition);
			if(checkedId == R.id.radioPhoto1){
				aggregationCandidate.setChosenPhotoContact(aggregationCandidate.getFirstRawContact());
			}
			else{
				aggregationCandidate.setChosenPhotoContact(aggregationCandidate.getSecondRawContact());
			}
			
		}
	};
	
	

}
