package fido.contacts.merge.ui.resultDetail;

import java.util.ArrayList;

import fido.contacts.merge.App;
import fido.contacts.merge.R;
import fido.contacts.merge.data.RawContact;
import fido.contacts.merge.ui.RawContactUI;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RawContactsAdapter extends BaseExpandableListAdapter {
	
	private static final String LOG_TAG = "RawContactAdapter";
	
	private static final int RAW_CONTATC_GROUP_HEIGHT = 45;
	
	
	private ArrayList<RawContact> virtualContactRawContacts;
	
	private LayoutInflater inflater;
	
	private Context context;
	
	private Drawable defaultContactImage;
	
    private int[] childHeights;
    
    private int listTotalHeight;

    
    private ExpandableListView listView;

	
	public RawContactsAdapter(Context context, ArrayList<RawContact> contactsRawContacts,
            ExpandableListView listView) {
		this.context = context;
		this.virtualContactRawContacts = contactsRawContacts;	
        this.listView = listView;

        childHeights = new int[contactsRawContacts.size()];
        listTotalHeight = contactsRawContacts.size() * RAW_CONTATC_GROUP_HEIGHT;
        
		defaultContactImage = context.getResources().getDrawable(R.drawable.contactus_icon);
		
		// Cache the LayoutInflate to avoid asking for a new one each time.
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getGroupCount() {
		return virtualContactRawContacts.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return virtualContactRawContacts.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return virtualContactRawContacts.get(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View rawContactItem, ViewGroup parent) {
		if (rawContactItem == null) {
			// Parent couldn't be there TODO research appropriate parent
            rawContactItem = inflater.inflate(R.layout.item_raw_contact, null);
        }
		
		return RawContactUI.getItem(virtualContactRawContacts.get(groupPosition), (RelativeLayout) rawContactItem);
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View rawContactItemDetail,
			ViewGroup parent) {
		if (rawContactItemDetail == null) {	
			// Parent couldn't be there TODO research appropriate parent
            rawContactItemDetail = inflater.inflate(R.layout.result_detail_fragment_item_raw_contact, null);
            
            RawContact rawContact = virtualContactRawContacts.get(groupPosition);
    		
    		TextView displayNameView =  (TextView) rawContactItemDetail.findViewById(R.id.displayName);
    		TextView accountNameView =  (TextView) rawContactItemDetail.findViewById(R.id.accountName);
    		ImageView photoView =  (ImageView) rawContactItemDetail.findViewById(R.id.photo);
    		final ImageView rawContactAccountTypeView = (ImageView) rawContactItemDetail.findViewById(R.id.accountType);
    		
    		displayNameView.setText(rawContact.getDisplayName());
    		accountNameView.setText(rawContact.getAccountName());
    		rawContactAccountTypeView.setImageResource(rawContact.getAccountTypeIconId());
    		
    		ArrayList<String> phoneNumbers = rawContact.getPhoneNumbers();
    		if(phoneNumbers != null){
    			for (String phoneNumber : phoneNumbers) {
    				TextView phoneNumberView = new TextView(context);
    				LinearLayout phoneNumbersList = (LinearLayout) rawContactItemDetail.findViewById(R.id.phoneNumbers);
    				phoneNumbersList.addView(phoneNumberView);
    				phoneNumberView.setText(phoneNumber);
    			}
    		}
    		
    		ArrayList<String> emails = rawContact.getEmails();
    		if(emails != null){
    			for (String email : emails) {
    				TextView emailView = new TextView(context);
    				LinearLayout emailsList = (LinearLayout) rawContactItemDetail.findViewById(R.id.emails);
    				emailsList.addView(emailView);
    				emailView.setText(email);
    			}
    		}
    		
    		if(rawContact.getPhoto() != null){		
    			BitmapDrawable image = new BitmapDrawable(context.getResources(), rawContact.getPhoto());
    			photoView.setImageDrawable(image);
    		}
    		else{
    			photoView.setImageDrawable(defaultContactImage);
    		}
        }

		return rawContactItemDetail;
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
	
    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
        
        App.get().getLogManager().logMessage(LOG_TAG, "Raw contact list collapsed.");
        
        decreaseHeight(childHeights[groupPosition]);
        App.get().getLogManager().logMessage(LOG_TAG, "height dencreased by " + childHeights[groupPosition]);
    }

	@Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
        
        App.get().getLogManager().logMessage(LOG_TAG, "Raw contact list expanded.");
        if(childHeights[groupPosition] == 0){
        	computeChildHeihgt(groupPosition);
        }
        
        increaseListHeight(childHeights[groupPosition]);
        App.get().getLogManager().logMessage(LOG_TAG, "height increased by " + childHeights[groupPosition]);
    }
    
    private void computeChildHeihgt(int groupPosition) {
    	RawContact rawContact = (RawContact) getGroup(groupPosition);
        View rawContactItemDetail = inflater.inflate(R.layout.result_detail_fragment_item_raw_contact, null);
        rawContactItemDetail.setLayoutParams(
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
                        RelativeLayout.LayoutParams.WRAP_CONTENT));
        rawContactItemDetail.measure(MeasureSpec.UNSPECIFIED,MeasureSpec.UNSPECIFIED);
        childHeights[groupPosition] = rawContactItemDetail.getMeasuredHeight();
        ArrayList<String> phoneNumbers = rawContact.getPhoneNumbers();
        if(phoneNumbers != null){
            childHeights[groupPosition] += phoneNumbers.size() * 40;
        }
        ArrayList<String> emails = rawContact.getEmails();
        if(emails != null){
            childHeights[groupPosition] += emails.size() * 40;
        }
        
        childHeights[groupPosition] += 20;
        
        App.get().getLogManager().logMessage(LOG_TAG, "computed child height = " + rawContactItemDetail.getHeight());
        App.get().getLogManager().logMessage(LOG_TAG, "measured child height = " + rawContactItemDetail.getMeasuredHeight());
	}
    

    private void decreaseHeight(int height) {
    	App.get().getLogManager().logMessage(LOG_TAG, "decreasing height by: " + height);
    	LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) listView.getLayoutParams();
    	listTotalHeight -= height;
	    param.height = listTotalHeight;
	    App.get().getLogManager().logMessage(LOG_TAG, "expanded list changed height: " + param.height);
	    listView.setLayoutParams(param);	
	}

	private void increaseListHeight(int height){
		App.get().getLogManager().logMessage(LOG_TAG, "increasing height by: " + height);
		LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) listView.getLayoutParams();
		listTotalHeight += height;
	    param.height = listTotalHeight;
	    App.get().getLogManager().logMessage(LOG_TAG, "expanded list changed height: " + param.height);
	    listView.setLayoutParams(param);
    }


}
