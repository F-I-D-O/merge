package fido.contacts.merge.ui.resultDetail;

import java.util.ArrayList;

import fido.contacts.merge.App;
import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregationCandidate;
import fido.contacts.merge.data.Contact;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.data.RawContact;
import fido.contacts.merge.ui.MergeActivity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class ResultDetailFragment extends Fragment {
	
	private static final String LOG_TAG = "ResultDetailFragment";
	
	
	
	
	private DataManager dataManager;
	private ResultDetailActivity activity;
	private AggregationCandidate aggregationCandidate;
	
	private Drawable defaultContactImage;
	
	private ScrollView scrollArea;

	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.result_detail_fragment, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		defaultContactImage = App.get().getResources().getDrawable(R.drawable.contactus_icon);
		
		try {
			dataManager = DataManager.get();

			activity = (ResultDetailActivity) getActivity();
			aggregationCandidate = dataManager.getAggregationCandidatesList().get(activity.index);
			
			scrollArea = (ScrollView) activity.findViewById(R.id.scrollArea);
			
			LinearLayout contactsView = (LinearLayout) activity.findViewById(R.id.contacts);
			
			for (Contact contact : aggregationCandidate.getContacts()) {
				drawContact(contactsView, contact);
			}
			
			Button discardButton = (Button) getActivity().findViewById(R.id.buttonDiscard);
			discardButton.setOnClickListener(discardListener);
			
			Button aggregateButton = (Button) getActivity().findViewById(R.id.buttonAgregateContacts);
			aggregateButton.setOnClickListener(aggregateListener);
			
//			System.out.println(aggregationCandidate.getFirstRawContact().getRawContactId() + "-" + aggregationCandidate.getSecondRawContact().getRawContactId());

			
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	private void drawContact(LinearLayout contactsView, Contact contact) {
		
		LinearLayout contactItem = (LinearLayout) 
				activity.getLayoutInflater().inflate(R.layout.result_detail_fragment_item_contact, null);

		ImageView photoView =  (ImageView) contactItem.findViewById(R.id.photoView);
		TextView displayNameView =  (TextView) contactItem.findViewById(R.id.displayName);
		Button openContactDetailInPeopleAppButton = 
				(Button) contactItem.findViewById(R.id.buttonViewDetailInPeopleApp);
				
		if(contact.getPhoto() != null){		
			BitmapDrawable image = new BitmapDrawable(App.get().getResources(), contact.getPhoto());
			photoView.setImageDrawable(image);
		}
		else{
			photoView.setImageDrawable(defaultContactImage);
		}
		displayNameView.setText(contact.getDisplayName());
		openContactDetailInPeopleAppButton.setOnClickListener(openContactDetailInPeopleAppButtonClickListener);
		
		drawRawContactList(contactsView, contactItem, contact);
		
		contactsView.addView(contactItem);
	}

	private void drawRawContactList(final LinearLayout contactsView, final LinearLayout contactItem, Contact contact) {	
		final ExpandableListView expandableListView = (ExpandableListView) contactItem.findViewById(R.id.rawContactList);
		final ArrayList<RawContact> rawContacts = contact.getRawContacts();
		RawContactsAdapter adapter = new RawContactsAdapter(activity, rawContacts, expandableListView);
		expandableListView.setAdapter(adapter);
//		expandableListView.setOnGroupExpandListener(new OnGroupExpandListener() {
//			
//			@Override
//			public void onGroupExpand(int groupPosition) {
//				App.get().getLogManager().logMessage(LOG_TAG, "Raw contact list expanded.");
//				LinearLayout.LayoutParams param = (LinearLayout.LayoutParams) expandableListView.getLayoutParams();
//				App.get().getLogManager().logMessage(LOG_TAG, "expanded list measured height: " + param.height);
//				childHeight = adapter.getChildView(groupPosition, groupPosition, isLastChild, , parent)
//			    param.height = rawContacts.size() * RAW_CONTATC_GROUP_HEIGHT;
//			    App.get().getLogManager().logMessage(LOG_TAG, "expanded list changed height: " + param.height);
//			    expandableListView.setLayoutParams(param);
//			    expandableListView.refreshDrawableState();
//			    contactItem.refreshDrawableState();
//			    contactsView.refreshDrawableState();
//			    scrollAreaChild.refreshDrawableState();
//			    scrollArea.refreshDrawableState();
//			}
//		});
		App.get().getLogManager().logMessage(LOG_TAG, "Raw contact list drawed.");
	}

	OnClickListener discardListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			dataManager.removeAggregationCandidate(aggregationCandidate);
			activity.finish();
		}
	};
	
	OnClickListener aggregateListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			dataManager.setIndividualAggregation(aggregationCandidate);
			startActivityForResult(new Intent(activity, MergeActivity.class), ResultDetailActivity.EXIT_STATUS_REQUEST);
		}
	};
	
	private OnClickListener openContactDetailInPeopleAppButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
//		    final int position = listView.getPositionForView(v);	    
//		    if (position != ExpandableListView.INVALID_POSITION) {
//		    	Contact contact = (Contact) getGroup(position);
//		    	Intent intent= new Intent(Intent.ACTION_VIEW);
//		    	Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contact.getId()));
//		        intent.setData(uri);
//		        context.startActivity(intent);
//		    }
		}
	};

}
