package fido.contacts.merge.ui.optionsActivity;

import java.util.Observable;
import java.util.Observer;

import fido.contacts.merge.App;
import fido.contacts.merge.BooleanSetting;
import fido.contacts.merge.FindMergedContactsThread;
import fido.contacts.merge.R;
import fido.contacts.merge.UnlockThread;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.searchModules.ModuleManagerException;
import fido.contacts.merge.searchModules.ModulesManager;
import fido.contacts.merge.ui.DialogResponseListener;
import fido.contacts.merge.ui.SearchActivity;
import fido.contacts.merge.ui.UICommon;
import fido.contacts.merge.ui.help.HelpLayoutOptions;

import android.os.Bundle;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author Fido
 * Initial activity, user can change options here, and than starts search activity.
 */
public class OptionsActivity extends FragmentActivity implements Observer, DialogResponseListener {
	
	private static final String LOG_TAG = "OptionsActivity";
	
	/**
	 * represent unlock user action
	 */
	public static final boolean  UNLOCK = true;
	
	/**
	 * represent lock user action
	 */
	public static final boolean  LOCK = false;
	
	
	
	
	/**
	 * Aplication
	 */
	private App app;
		
	/**
	 * Search button
	 */
	private Button searchButton;

	/**
	 * lock/unlock button
	 */
	private Button facebookLockButton;
	
	/**
	 * Button to star activity which manages merged contacts
	 */
	private Button manageMergedButton;
	
	/**
	 * Text indicating if user has root access
	 */
	private TextView rootIndicator;
	
	
	
	
	@Override
	public void update(Observable observable, Object data) {
		app.getLogManager().logMessage(LOG_TAG, "Options activity Observer received signal");
		if(App.get().getRootManager().isDeviceRootTestCompleted()){
			drawRootArea();
		}
	}
	
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {  
    	app = (App) getApplicationContext();
    	app.setMainActivity(this);
    	
    	// for root area to listen when root access check is completed
    	app.getRootManager().addObserver(this);
    	
    	getActionBar().setTitle(R.string.header_options);
        try {
        	if(!ModulesManager.isInit()){
        		ModulesManager.create();
        	}
        	super.onCreate(savedInstanceState);
        	boolean showHelp = false;
        	final FrameLayout frame = new FrameLayout(app);
        	setContentView(frame);
        	View mainView = getLayoutInflater().inflate(R.layout.activity_options, frame, false);
        	frame.addView(mainView);
        	if(showHelp){
        		frame.addView(new HelpLayoutOptions(app, frame));
        	}

	        initRootArea();
	        searchButton = (Button) findViewById(R.id.startSearchButton);
	        manageMergedButton = (Button) findViewById(R.id.buttonManageMergedContacts);
		} catch (ModuleManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @Override
	protected void onPause() {
		super.onPause();
		app.getLogManager().save();
	}
    
    @Override
	protected void onResume() {		
		super.onResume();
		if(DataManager.isInit()){
			DataManager.destroy();
			System.out.println("Data manager destroyed by options activity on resume");
		}
		unlockUI();
	}
    
    

	/**
	 * Initializes root area. It draws the content only if root check was already completed.
	 */
	private void initRootArea() {
		facebookLockButton = (Button) findViewById(R.id.buttonFacebookLock);
		rootIndicator = (TextView) findViewById(R.id.rootAccessIndicator);
		if(App.get().getRootManager().isDeviceRootTestCompleted()){
			drawRootArea();
		}
	}
	
	/**
	 * Draws root area content
	 */
	private void drawRootArea(){
		RelativeLayout loadingOverlay = (RelativeLayout) findViewById(R.id.loadingOverlay);
		FrameLayout rootArea = (FrameLayout) findViewById(R.id.rootArea);
		rootArea.removeView(loadingOverlay);
		if(app.getRootManager().isDeviceRooted()){
			rootIndicator.setText(R.string.message_device_rooted);
			UICommon.toastMessage(R.string.message_device_rooted, this);
			facebookLockButton.setEnabled(true);
			facebookLockButton.setOnClickListener(new UnlockButtonListener(this));
			if(app.loadBooleanSetting(BooleanSetting.FACEBOOK_CONTACTS_LOCKED)){
				facebookLockButton.setText(R.string.button_unlock_facebook_contacts);
			}
			else{
				facebookLockButton.setText(R.string.button_lock_facebook_contacts);
			}
		}
		else{
			rootIndicator.setText(R.string.message_device_not_rooted);
			UICommon.toastMessage(R.string.message_device_not_rooted, this);
			facebookLockButton.setEnabled(false);
		}
	}


	


	

	/**
	 * Starts the SearchActivity. Parameter {@link View} is not used, but it's obligatory.
	 */
	public void startSearching(View view){
		startActivity(new Intent(this, SearchActivity.class));
	}
	
	public void manageMergedContacts(View view){
		lockUI();
		FindMergedContactsThread findMergedThread = new FindMergedContactsThread(this);
		findMergedThread.execute();
	}
	
	protected void unlockFacebookContactsStart(){
		try {
			lockUI();
			UnlockThread unlockThread = new UnlockThread(UNLOCK, this);
			unlockThread.execute();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void unlockFacebookContactsEnd(){
		app.saveBooleanSetting(BooleanSetting.FACEBOOK_CONTACTS_LOCKED, false);
		facebookLockButton.setText(R.string.button_lock_facebook_contacts);
		unlockUI();
		System.out.println("unlock procedure end");
	}
	
	protected void lockFacebookContactsStart(){
		try {
			lockUI();
			UnlockThread unlockThread = new UnlockThread(LOCK, this);
			unlockThread.execute();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void lockFacebookContactsEnd(){
		app.saveBooleanSetting(BooleanSetting.FACEBOOK_CONTACTS_LOCKED, true);
		facebookLockButton.setText(R.string.button_unlock_facebook_contacts);
		unlockUI();
	}
	
	private void lockUI(){
		searchButton.setEnabled(false);
		facebookLockButton.setEnabled(false);
		manageMergedButton.setEnabled(false);
	}
	
	private void unlockUI(){
		searchButton.setEnabled(true);
		facebookLockButton.setEnabled(true);
		manageMergedButton.setEnabled(true);
	}

	

	@Override
	public void yesButtonClicked() {
		if(App.get().loadBooleanSetting(BooleanSetting.FACEBOOK_CONTACTS_LOCKED)){
			unlockFacebookContactsStart();	
		}
		else{
			lockFacebookContactsStart();
		}
	}

	@Override
	public void noButtonClicked() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void okButtonClicked() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelButtonClicked() {
		// TODO Auto-generated method stub
		
	}

	public void promptUnlockFacebookContacts() {
		UICommon.messageBoxWithNotShowAgainCheckbox(this, BooleanSetting.APROVE_UNLOCK_DIALOG, 
				getString(R.string.dialog_title_unlock_contacts), getString(R.string.dialog_text_unlock_lock_contacts));		
	}

	public void promptLockFacebookContacts() {
		UICommon.messageBoxWithNotShowAgainCheckbox(this, BooleanSetting.APROVE_UNLOCK_DIALOG, 
				getString(R.string.dialog_title_unlock_contacts), getString(R.string.dialog_text_unlock_lock_contacts));
	}
	    
}
