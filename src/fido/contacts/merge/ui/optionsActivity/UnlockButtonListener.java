package fido.contacts.merge.ui.optionsActivity;

import fido.contacts.merge.App;
import fido.contacts.merge.BooleanSetting;
import android.view.View;
import android.view.View.OnClickListener;

public final class UnlockButtonListener implements OnClickListener {
	
	private OptionsActivity optionsActivity;
	
	public UnlockButtonListener(OptionsActivity opttionsActivity) {
		super();
		this.optionsActivity = opttionsActivity;
	}

	@Override
	public void onClick(View v) {
		if(App.get().settingExist(BooleanSetting.APROVE_UNLOCK_DIALOG) && 
			App.get().loadBooleanSetting(BooleanSetting.APROVE_UNLOCK_DIALOG)){

			if(App.get().loadBooleanSetting(BooleanSetting.FACEBOOK_CONTACTS_LOCKED)){
				optionsActivity.unlockFacebookContactsStart();	
			}
			else{
				optionsActivity.lockFacebookContactsStart();
			}
		}
		else {
			if(App.get().loadBooleanSetting(BooleanSetting.FACEBOOK_CONTACTS_LOCKED)){
				optionsActivity.promptUnlockFacebookContacts();	
			}
			else{
				optionsActivity.promptLockFacebookContacts();
			}
		}
	}

}
