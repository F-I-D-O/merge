package fido.contacts.merge.ui.optionsActivity;

import fido.contacts.merge.R;
import fido.contacts.merge.searchModules.ModuleManagerException;
import fido.contacts.merge.searchModules.ModulesManager;
import fido.contacts.merge.searchModules.SearchModule;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * @author Fido
 * Adapter that provides data from search modules to options fragment.
 */
public class OptionsAdapter extends ArrayAdapter<SearchModule> {
	
	/**
	 * Layout inflater (for not creating it all the time).
	 */
	private LayoutInflater inflater;
	
	/**
	 * Context from an activity - for resource finding.
	 */
	private Context context;

	/**
	 * Constructor
	 * @param context Context from the activity.
	 * @throws ModuleManagerException 
	 */
	public OptionsAdapter(Context context) throws ModuleManagerException {
		super(context, R.layout.item_search_options, ModulesManager.get().getSearchModules());
		this.context = context;
		
		// Cache the LayoutInflate to avoid asking for a new one each time.
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final SearchModule searchModule = this.getItem(position);
		
		if(convertView == null){
			convertView = inflater.inflate(R.layout.item_search_options, null);
		}
		final TextView nameView = (TextView) convertView.findViewById(R.id.searchOptionName);
		final CheckBox selectedBox = (CheckBox) convertView.findViewById(R.id.searchOptionSelected);
		
		nameView.setText(context.getString(searchModule.getDisplayNameId()) + " " + 
				context.getString(R.string.options_activity_module_suffix));	
		selectedBox.setChecked(searchModule.isActivated());
				
		selectedBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				searchModule.setActivated(selectedBox.isChecked());
			}
		});
		
		return convertView;
	}
}
