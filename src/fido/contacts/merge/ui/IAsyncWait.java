package fido.contacts.merge.ui;

public interface IAsyncWait {
	
	public abstract void asyncCompleted();

}
