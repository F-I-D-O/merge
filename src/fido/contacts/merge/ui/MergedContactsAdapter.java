package fido.contacts.merge.ui;

import java.util.ArrayList;
import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregatedContact;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MergedContactsAdapter extends BaseAdapter {
	
	private ArrayList<AggregatedContact> aggregatedContacts;
	
	private boolean showOnlyContactsMergedByApp;

	/**
	 * Layout inflater (for not creating it all the time).
	 */
	private LayoutInflater inflater;
	
	private Context context;

	public boolean isShowOnlyContactsMergedByApp() {
		return showOnlyContactsMergedByApp;
	}

	public void setShowOnlyContactsMergedByApp(boolean showOnlyContactsMergedByApp) {
		this.showOnlyContactsMergedByApp = showOnlyContactsMergedByApp;
		notifyDataSetChanged();
	}

	public MergedContactsAdapter(Context context) throws DataManagerException {
		aggregatedContacts = DataManager.get().getAggregatedContactsList();

		// Cache the LayoutInflate to avoid asking for a new one each time.
		inflater = LayoutInflater.from(context);
		
		this.context = context;
		showOnlyContactsMergedByApp = false;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final AggregatedContact aggregatedContact = this.getItem(position);
		if(showOnlyContactsMergedByApp && !aggregatedContact.isAggregatedWithApp()){
			convertView = inflater.inflate(R.layout.null_layout, null);
			convertView.setId(R.layout.null_layout);
		}
		else{		
			if(convertView == null || convertView.getId() == R.layout.null_layout){
				convertView = inflater.inflate(R.layout.item_manage_merged, null);	
			}
			
			final TextView firstContactNameView = (TextView) convertView.findViewById(R.id.firstContactName);
			final TextView secondContactNameView = (TextView) convertView.findViewById(R.id.secondContactName);
			final TextView mergedByView = (TextView) convertView.findViewById(R.id.mergedBy);
			
			firstContactNameView.setText(aggregatedContact.getFirstRawContact().getDisplayName());	
			secondContactNameView.setText(aggregatedContact.getSecondRawContact().getDisplayName());
			String mergedByString = aggregatedContact.isAggregatedWithApp() ? context.getString(R.string.yes) : 
				context.getString(R.string.no);
			mergedByView.setText(context.getString(R.string.merged_contacts_adapter_merged_by) + " " + mergedByString);
			
			if(DataManager.debug){
				final TextView firstRawContactDevView = 
						(TextView) convertView.findViewById(R.id.firstRawContactDevText);
				final TextView secondRawContactDevView = 
						(TextView) convertView.findViewById(R.id.secondRawContactDevText);
				firstRawContactDevView.setText(
						Integer.toString(aggregatedContact.getFirstRawContact().getRawContactId()));	
				secondRawContactDevView.setText(
						Integer.toString(aggregatedContact.getSecondRawContact().getRawContactId()));	
			}
		}
		return convertView;
	}
	
	
	@Override
	public int getCount() {
		return aggregatedContacts.size();
	}

	@Override
	public AggregatedContact getItem(int position) {
		return aggregatedContacts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return aggregatedContacts.get(position).hashCode();
	}
	
}
