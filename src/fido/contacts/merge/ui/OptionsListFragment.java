package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.searchModules.ModuleManagerException;
import fido.contacts.merge.ui.optionsActivity.OptionsAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * @author Fido
 * Fragment that contains the options list
 */
public class OptionsListFragment extends ListFragment {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			setListAdapter(new OptionsAdapter(this.getActivity()));
		} catch (ModuleManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.options_fragment, container);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		showSettings(position);
	}

	private void showSettings(int position) {
		Intent detailIntent = new Intent();
        detailIntent.setClass(getActivity(), OptionSettingsActivity.class);
        detailIntent.putExtra("OptionIndex", position);
        startActivity(detailIntent);
	}
}
