package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.SearchThread;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.searchModules.ModuleManagerException;
import fido.contacts.merge.searchModules.ModulesManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SearchActivity extends Activity {
	
	private ModulesManager moduleManager;
	
	private ProgressBar progressBarTotal;
	private ProgressBar progressBarModule;
	private ProgressBar progressBarPartial;
	private TextView headerTotalProgress;
	private TextView headerModuleProgress;
	private TextView headerPartialProgress;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    
        setContentView(R.layout.activity_search_dialog); 
        if(!DataManager.isInit()){
        	start();
        }
    }
	
	public void start(){
		try {
			moduleManager = ModulesManager.get();
		} catch (ModuleManagerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			DataManager.create(this.getContentResolver());
			SearchThread searchThread = new SearchThread(this);
	        searchThread.execute();
	        initializeDialog();
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initializeDialog() {
		moduleManager.countActivatedModules();
		progressBarTotal = (ProgressBar) findViewById(R.id.searchProgressBarTotal);
		progressBarTotal.setMax(moduleManager.getNumberOfActivatedModules());
		progressBarModule = (ProgressBar) findViewById(R.id.SearchProgressBarModule);
		progressBarPartial = (ProgressBar) findViewById(R.id.SearchProgressBarPartial);
		headerTotalProgress = (TextView) findViewById(R.id.SearchTextViewTotal);
		headerModuleProgress = (TextView) findViewById(R.id.SearchTextViewModule);
		headerPartialProgress = (TextView) findViewById(R.id.SearchTextViewPartial);
	}

	public void updateProgress() {
		progressBarTotal.setProgress(moduleManager.getProgress().getTotalProgress());
		progressBarModule.setProgress(moduleManager.getProgress().getModuleProgress());
		progressBarPartial.setProgress(moduleManager.getProgress().getPartialProgress());
		headerTotalProgress.setText(getResources().getText(moduleManager.getProgress().getTotalProgressHeaderStringId()));
		headerModuleProgress.setText(getResources().getText(moduleManager.getProgress().getModuleProgressHeaderStringId()));
		headerPartialProgress.setText(moduleManager.getProgress().getPartialProgressHeaderString());
		progressBarPartial.setMax(moduleManager.getProgress().getPartialProgresMaxValue());
	}
	
	public void completed(){
		Intent resultsIntent = new Intent(this, ResultsActivity.class);
    	this.startActivity(resultsIntent);
	}

}
