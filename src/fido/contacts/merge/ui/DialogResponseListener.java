package fido.contacts.merge.ui;

public interface DialogResponseListener {
	
	public abstract void yesButtonClicked();
	
	public abstract void noButtonClicked();
	
	public abstract void okButtonClicked();
	
	public abstract void cancelButtonClicked();
	
}
