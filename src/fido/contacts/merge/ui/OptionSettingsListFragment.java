package fido.contacts.merge.ui;

import fido.contacts.merge.searchModules.ModuleManagerException;
import android.os.Bundle;
import android.support.v4.app.ListFragment;

public class OptionSettingsListFragment extends ListFragment {
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		OptionSettingsActivity activity = (OptionSettingsActivity) getActivity();
		try {
			setListAdapter(new OptionSettingsAdapter(activity));
		} catch (ModuleManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
