package fido.contacts.merge.ui;

import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

public class MergedContactsListFragment extends ListFragment {
	
	MergedContactsAdapter adapter;
	
	DataManager dataManager;
	
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			dataManager = DataManager.get();
			adapter = new MergedContactsAdapter(this.getActivity());
			setListAdapter(adapter);
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		if(dataManager.isAggregatedContactsListChanged()){
			adapter.notifyDataSetChanged();
			dataManager.clearAggregatedContactsListChanged();
		}	
		super.onResume();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		showDetails(position);
	}

	private void showDetails(int position) {
		Intent detailIntent = new Intent();
        detailIntent.setClass(getActivity(), MergedContactDeatailActivity.class);
        detailIntent.putExtra("index", position);
        startActivity(detailIntent);
	}	
	
	public void toggleFilter(){
		if(adapter.isShowOnlyContactsMergedByApp()){
			adapter.setShowOnlyContactsMergedByApp(false);
		}
		else{
			adapter.setShowOnlyContactsMergedByApp(true);
		}
	}
}
