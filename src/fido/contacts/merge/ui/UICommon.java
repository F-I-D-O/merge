package fido.contacts.merge.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;
import fido.contacts.merge.App;
import fido.contacts.merge.BooleanSetting;
import fido.contacts.merge.R;

public final class UICommon {
	
	public static final String ERROR = "Error";
	
	public static void showError(Activity activity, String message){
		messageBox(activity, ERROR, message);
	}
	
	public static void messageBox(Activity activity, String title, String message)
	{
	    AlertDialog.Builder messageBox = new AlertDialog.Builder(activity);
	    messageBox.setTitle(title);
	    messageBox.setMessage(message);
	    messageBox.setCancelable(false);
	    messageBox.setNeutralButton("OK", null);
	    messageBox.show();
	}
	
	public static void messageBoxWithNotShowAgainCheckbox(final Activity activity, final BooleanSetting setting,
			final String title, final String message)
	{
	    AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
	    LayoutInflater dialogInflater = LayoutInflater.from(activity);
	    View checkboxLayout = dialogInflater.inflate(R.layout.component_not_show_again_checkbox, null);
	    
	    // check box is used in listener
	    final CheckBox dontShowAgain = (CheckBox) checkboxLayout.findViewById(R.id.skipCheckbox);
	    dialog.setView(checkboxLayout);
	    
	    dialog.setTitle(title);
	    dialog.setMessage(message);
	    dialog.setCancelable(false);
	    
	    DialogInterface.OnClickListener dialogSubmitListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int buttonType) {          	
            	boolean operationApproved = (buttonType == DialogInterface.BUTTON_POSITIVE) ? true : false;
            	if(dontShowAgain.isChecked()){
            		App.get().saveBooleanSetting(setting, operationApproved);
            		App.get().getLogManager().logMessage("Message box - " + setting.getName(), 
            				"don't show again saved with value: " + Boolean.toString(operationApproved));
            	}
            	if(operationApproved){
            		((DialogResponseListener) activity).yesButtonClicked();
            	}
            }
        };
	    
	    dialog.setPositiveButton(R.string.yes, dialogSubmitListener);
	    dialog.setNegativeButton(R.string.no, dialogSubmitListener);
	    dialog.show();
	}
	
	
	
	public static void toastMessage(int stringID, Activity act) {
		toastMessage(act.getResources().getString(stringID), act.getApplicationContext());
	}
	
	public static void toastMessage(String string, Activity act) {
		toastMessage(string, act.getApplicationContext());
	}
	
	public static void toastMessage(String string, Context context) {
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, string, duration);
		toast.show();
	}
}
