package fido.contacts.merge.ui;

import fido.contacts.merge.DiscardListener;
import fido.contacts.merge.R;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.ui.optionsActivity.OptionsActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GestureDetectorCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class ResultsActivity extends FragmentActivity {
	
	private DataManager dataManager;
	
	private GestureDetectorCompat gestureDetector; 
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return gestureDetector.onTouchEvent(event);
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        gestureDetector = new GestureDetectorCompat(this, new DiscardListener());

        try {
			dataManager = DataManager.get();
			initTextViews();
		} 
        catch (DataManagerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        
    }
	
	private void initTextViews() {
		TextView numberOfContactsView = (TextView) findViewById(R.id.contactCount);
        TextView numberOfAggregatedContactsView = (TextView) findViewById(R.id.aggregationCandidatesCount);
       
		numberOfContactsView.setText(getResources().getString(R.string.result_activity_contacts_count) + " " +
					dataManager.getNumberOfContacts());
        numberOfAggregatedContactsView.setText(getResources().getString(
        		R.string.result_activity_aggregation_candidates_count) + " " +
        		dataManager.getAggregationCandidatesList().size());
	}

	/**
	 * Starts the MergeMultipleActivity. Parameter {@link View} is not used, but it's obligatory.
	 */
	public void prepareMergeMultiple(View view){
		startActivity(new Intent(this, MergeMultipleActivity.class));
	}
	
	public void backToOptions(View view){
		DataManager.destroy();
		startActivity(new Intent(this, OptionsActivity.class));
	}
}
