package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregatedContact;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.data.RawContact;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MergedContactDetailFragment extends Fragment {
	
	private DataManager dataManager;
	private MergedContactDeatailActivity activity;
	private AggregatedContact aggregatedContact;
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_merged_contact_details, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			dataManager = DataManager.get();

			activity = (MergedContactDeatailActivity) getActivity();
			aggregatedContact = dataManager.getAggregatedContactsList().get(activity.index);
			
			RawContact firstRawContact = aggregatedContact.getFirstRawContact();
			RawContact secondRawContact = aggregatedContact.getSecondRawContact();
			
			//View layout = inflater.inflate(R.layout.test_result_fragment_view, container, false);
			TextView firstContactName = (TextView) getActivity().findViewById(R.id.firstContactName);
			TextView secondContactName = (TextView) getActivity().findViewById(R.id.secondContactName);
		
			firstContactName.setText(aggregatedContact.getFirstRawContact().getDisplayName());
			secondContactName.setText(aggregatedContact.getSecondRawContact().getDisplayName());
			
			if(firstRawContact.getPhoto() != null){
				ImageView firstContactPhoto = (ImageView) getActivity().findViewById(R.id.firstContactPhoto);
				firstContactPhoto.setImageBitmap(firstRawContact.getPhoto());
			}
			if(secondRawContact.getPhoto() != null){
				ImageView secondContactPhoto = (ImageView) getActivity().findViewById(R.id.secondContactPhoto);
				secondContactPhoto.setImageBitmap(secondRawContact.getPhoto());
			}
						
			Button disaggregateButton = (Button) getActivity().findViewById(R.id.buttonDisagregateContacts);
			disaggregateButton.setOnClickListener(disaggregateListener);
//			System.out.println(aggregatedContact.getFirstRawContact().getRawContactId() + "-" + 
//					aggregatedContact.getSecondRawContact().getRawContactId());

			
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	

	OnClickListener disaggregateListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			dataManager.disconnectContacts(aggregatedContact);
			dataManager.removeMergedContact(aggregatedContact);
			activity.finish();
		}
	};
}
