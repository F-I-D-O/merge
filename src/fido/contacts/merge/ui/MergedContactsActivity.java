package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.ui.optionsActivity.OptionsActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

public class MergedContactsActivity extends FragmentActivity {

	private DataManager dataManager;
	
	MergedContactsListFragment fragment;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_merged);
        fragment = (MergedContactsListFragment) 
        		getSupportFragmentManager().findFragmentById(R.id.mergedContactsListFragment);
        CheckBox filterToggle = (CheckBox) findViewById(R.id.showOnlyContactsMergedByThisAppToggle);
        filterToggle.setOnClickListener(toggleFillterListener);
        try {
			dataManager = DataManager.get();
		} catch (DataManagerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
    }
	
	public void backToOptions(View view){
		DataManager.destroy();
		startActivity(new Intent(this, OptionsActivity.class));
	}
	
	public void toggleFilter(){
		fragment.toggleFilter();
	}
	
	OnClickListener toggleFillterListener = new OnClickListener(){
		@Override
		public void onClick(View v) {
			toggleFilter();
		}
	};
}
