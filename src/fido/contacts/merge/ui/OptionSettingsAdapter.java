package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.searchModules.ModuleManagerException;
import fido.contacts.merge.searchModules.ModulesManager;
import fido.contacts.merge.searchModules.SearchModule.ModuleSetting;
import fido.contacts.merge.searchModules.SearchModule.ModuleSettingCheckbox;
import fido.contacts.merge.searchModules.WrongDataTypeException;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

public class OptionSettingsAdapter extends ArrayAdapter<ModuleSetting> {
	
	/**
	 * Layout inflater (for not creating it all the time).
	 */
	private LayoutInflater inflater;
	
	/**
	 * Context from an activity - for resource finding.
	 */
	private Context context;

	/**
	 * Constructor
	 * @param context Context from the activity.
	 * @throws ModuleManagerException 
	 */
	public OptionSettingsAdapter(OptionSettingsActivity optionSettingsActivity ) throws ModuleManagerException {
		super(optionSettingsActivity, R.layout.item_option_setting, ModulesManager.get().getSearchModules().
				get(optionSettingsActivity.getOptionIndex()).getSearchSettingList());
		this.context = optionSettingsActivity;
		
		// Cache the LayoutInflate to avoid asking for a new one each time.
		inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ModuleSetting moduleSetting = this.getItem(position);
		
		if(convertView == null){
			convertView = inflater.inflate(R.layout.item_option_setting, null);
		}
		final TextView nameView = (TextView) convertView.findViewById(R.id.optionSettingName);
		nameView.setText(context.getString(moduleSetting.getDisplayNameId()));	
		
		FrameLayout buttonArea = (FrameLayout) convertView.findViewById(R.id.optionSettingButtonArea);
		if(moduleSetting.getClass() == ModuleSettingCheckbox.class){
			buttonArea.addView(getCheckBox((ModuleSettingCheckbox) moduleSetting));
		}
		
		
		return convertView;
	}

	private View getCheckBox(final ModuleSettingCheckbox moduleSetting) {
		final CheckBox checkBox = (CheckBox) new CheckBox(context);
		checkBox.setChecked(moduleSetting.getState());
		System.out.println(moduleSetting.getState());
		checkBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				try {
					moduleSetting.setState(checkBox.isChecked());
				} catch (WrongDataTypeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		return checkBox;
	}
}
