package fido.contacts.merge.ui;

import fido.contacts.merge.R;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.ui.resultDetail.ResultDetailActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ResultsListFragment extends ListFragment {
	
	ResultsAdapter adapter;
	
	DataManager dataManager;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			dataManager = DataManager.get();
			adapter = new ResultsAdapter(this.getActivity());
			setListAdapter(adapter);
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		if(dataManager.isAggregationCandidatesListChanged()){
			adapter.notifyDataSetChanged();
			dataManager.clearAggregaionCandidatesListChanged();
		}	
		super.onResume();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		showDetails(position);
	}

	private void showDetails(int position) {
		Intent detailIntent = new Intent();
        detailIntent.setClass(getActivity(), ResultDetailActivity.class);
        detailIntent.putExtra("index", position);
        startActivity(detailIntent);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		 View view = inflater.inflate(R.layout.result_list_fragment_list_results, container);
         return view;
	}	
	
}
