package fido.contacts.merge.ui;

import java.util.ArrayList;
import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregationCandidate;
import fido.contacts.merge.data.Contact;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.data.RawContact;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ResultsAdapter extends BaseAdapter {
	
	private static final int FIRST_METHOD_INDEX = 0;
	
	private static final int FIRST_VIRTUAL_CONTACT_POSITION = 1;
	private static final int SECOND_VIRTUAL_CONTACT_POSITION = 2;
	
	private ArrayList<AggregationCandidate> aggregationCandidates;

	/**
	 * Layout inflater (for not creating it all the time).
	 */
	private LayoutInflater inflater;

	public ResultsAdapter(Context context) throws DataManagerException {
		aggregationCandidates = DataManager.get().getAggregationCandidatesList();

		// Cache the LayoutInflate to avoid asking for a new one each time.
		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final AggregationCandidate aggregationCandidate = this.getItem(position);
		
		if(convertView == null){
			convertView = inflater.inflate(R.layout.result_list_fragment_item_result, null);
		}
		
		drawContact(aggregationCandidate.getFirstRawContact(), aggregationCandidate.getFirstContact(), FIRST_VIRTUAL_CONTACT_POSITION, convertView);
		drawContact(aggregationCandidate.getSecondRawContact(), aggregationCandidate.getSecondContact(), SECOND_VIRTUAL_CONTACT_POSITION, convertView);
		
		final RelativeLayout itemContainer = (RelativeLayout) convertView.findViewById(R.id.resultItem);
		final TextView methodNameView = (TextView) convertView.findViewById(R.id.method);
		final CheckBox selectedBox = (CheckBox) convertView.findViewById(R.id.aggregationCandidatesSelectBox);
		
		if(aggregationCandidate.getGroup() > 0){
			if(aggregationCandidate.getGroup() % 2 > 0){
				itemContainer.setBackgroundColor(convertView.getResources().getColor(R.color.resultItemColor1));
			}
			else{
				itemContainer.setBackgroundColor(convertView.getResources().getColor(R.color.resultItemColor2));
			}
		}
		else{
			itemContainer.setBackgroundColor(convertView.getResources().getColor(R.color.resultItemColorDefault));
		}
		
		methodNameView.setText(methodsToString(aggregationCandidate.getMethods(), convertView));
		selectedBox.setChecked(aggregationCandidate.isMergeApproved());

		selectedBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				aggregationCandidate.setMergeApproved(selectedBox.isChecked());
			}
		});
		
		return convertView;
	}
	
	private void drawContact(RawContact aggregationRawContact, Contact contact, int position, View convertView){
		LinearLayout contactView;
		contactView = (LinearLayout) (position == 1 ? convertView.findViewById(R.id.firstContact) : 
			convertView.findViewById(R.id.secondContact));
		
//		contactView.inflate(context, resource, root)
		
		TextView contactDisplayNameView = (TextView) contactView.findViewById(R.id.contactDisplayName);	
		contactDisplayNameView.setText(contact.getDisplayName());
		
		if(DataManager.debug){
			TextView contactDebugView = (TextView) contactView.findViewById(R.id.contactDebugView);	
			contactDebugView.setText(contact.getId() + "/" + aggregationRawContact.getRawContactId());
		}
		
		LinearLayout rawContactsView = (LinearLayout) contactView.findViewById(R.id.rawContactsList);
		rawContactsView.removeAllViews();
		for (RawContact rawContact : contact.getRawContacts()) {	
			final RelativeLayout rawContactItem = (RelativeLayout) inflater.inflate(R.layout.item_raw_contact, null);
			rawContactsView.addView(RawContactUI.getItem(rawContact, rawContactItem));
		}
	}
	
	private String methodsToString(ArrayList<Integer> methodIds, View convertView){
		String methodString = String.format("%s %s", convertView.getResources().getString(R.string.result_adapter_found_by), 
				convertView.getResources().getString(methodIds.get(FIRST_METHOD_INDEX))); 
		for (int i = 1; i < methodIds.size(); i++) {	
			methodString = String.format("%s, %s", methodString, convertView.getResources().getString(methodIds.get(i))); 
		}
		return methodString;
	}

	@Override
	public int getCount() {
		return aggregationCandidates.size();
	}

	@Override
	public AggregationCandidate getItem(int position) {
		return aggregationCandidates.get(position);
	}

	@Override
	public long getItemId(int position) {
		return aggregationCandidates.get(position).hashCode();
	}
	
}
