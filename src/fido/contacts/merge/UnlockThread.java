package fido.contacts.merge;

import fido.contacts.merge.ui.optionsActivity.OptionsActivity;
import android.os.AsyncTask;

public class UnlockThread extends AsyncTask<Void, Void, Void> {

	private boolean unlock;
	
	private OptionsActivity optionsActivity;
	
	public UnlockThread(boolean unlock, OptionsActivity optionsActivity) {
		super();
		this.unlock = unlock;
		this.optionsActivity = optionsActivity;
	}

	@Override
	protected Void doInBackground(Void... params) {	
		RootManager rootManager = App.get().getRootManager();
			if (unlock == OptionsActivity.UNLOCK) {
				try {
					rootManager.unlockFacebookContacts();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else{
				try {
					rootManager.lockFacebookContacts();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		return null;		
	}

	@Override
	protected void onPostExecute(Void result) {
		System.out.println("xhbhc");
		super.onPostExecute(result);
		if(unlock){
			optionsActivity.unlockFacebookContactsEnd();
		}
		else{
			optionsActivity.lockFacebookContactsEnd();
		}
	}
	
	

}
