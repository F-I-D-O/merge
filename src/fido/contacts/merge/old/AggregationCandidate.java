//package fido.contacts.merge.old;
//
//import java.util.ArrayList;
//
///**
// * @author Fido
// * Holds data that is required for aggregating two contacts.
// */
//public class AggregationCandidate {
//	
//	/**
//	 * First contact for aggregation.
//	 */
//	private Contact firstContact;
//	
//	/**
//	 * Second contact for aggregation
//	 */
//	private Contact secondContact;
//	
//	/**
//	 * List of methods that have found contacts as aggregation candidates.
//	 */
//	private ArrayList<Integer> methods;
//	
//	/**
//	 * Determine if aggregation is approved by the user.
//	 */
//	private boolean mergeApproved;
//	
//	private int hashCode;
//	
//	/**
//	 * Contact that's name was chosen by user for the aggregated contact
//	 */
//	private Contact chosenNameContact;
//	
//	/**
//	 * Contact that's photo was chosen by user for the aggregated contact
//	 */
//	private Contact chosenPhotoContact;
//	
//	
//	
//	
//	public Contact getFirstContact() {
//		return firstContact;
//	}
//
//	public void setFirstContact(String displayName, int rawContactId, String photoUri, int photoId) {
//		firstContact.fillContact(displayName, rawContactId, photoUri, photoId);
//	}
//
//	public Contact getSecondContact() {
//		return secondContact;
//	}
//
//	public void setSecondContact(String displayName, int rawContactId, String photoUri, int photoId) {
//		secondContact.fillContact(displayName, rawContactId, photoUri, photoId);
//	}
//
//	public ArrayList<Integer> getMethods() {
//		return methods;
//	}
//
//	public boolean isMergeApproved() {
//		return mergeApproved;
//	}
//
//	public void setMergeApproved(boolean merge) {
//		this.mergeApproved = merge;
//	}
//	
//	public Contact getChosenNameContact() {
//		return chosenNameContact;
//	}
//
//	public void setChosenNameContact(Contact chosenNameContact) {
//		this.chosenNameContact = chosenNameContact;
//	}
//
//	public Contact getChosenPhotoContact() {
//		return chosenPhotoContact;
//	}
//
//	public void setChosenPhotoContact(Contact chosenPhotoContact) {
//		this.chosenPhotoContact = chosenPhotoContact;
//	}
//
//	
//	
//	/**
//	 * Constructor. It sets up only contacts and first method, the rest fields are filled later, after searching ends.
//	 * @param firstContact First {@link Contact} to aggregate.
//	 * @param secondContact Second {@link Contact} to aggregate.
//	 * @param methodStringID ID of a string that identifies search method that has founded the match.
//	 */
//	public AggregationCandidate(Contact firstContact, Contact secondContact, int methodStringID) {
//		this.firstContact = firstContact;
//		this.secondContact = secondContact;
//		methods = new ArrayList<Integer>();
//		methods.add(methodStringID);
//		hashCode = firstContact.getLookupKey().hashCode() + secondContact.getLookupKey().hashCode();
//		
//		//	first contact is default for name and image	
//		chosenNameContact = firstContact;
//		chosenPhotoContact = firstContact;
//	}
//	
//	
//	
//	
//	/**
//	 * Adds an ID of the search method to the list.
//	 * @param methodID ID of a string that identifies search method that has founded the match.
//	 */
//	public void addMethod(int methodID){
//		methods.add(methodID);
//	}
//	
//	@Override
//	public boolean equals(Object o) {
//		if(o instanceof AggregationCandidate){			
//			AggregationCandidate secondCandidate = (AggregationCandidate) o;
//			if(hashCode == secondCandidate.hashCode){
//				return true;
//			}
//			else{
//				return false;
//			}
//		}
//		else{
//			return false;
//		}
//	}
//	
//	@Override
//	public int hashCode() {
//		return hashCode;
//	}
//		
//}
