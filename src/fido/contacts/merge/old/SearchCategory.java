package fido.contacts.merge.old;


public class SearchCategory {
	private String name;
	private boolean selected;
	private SearchCategorySettings settings;
	
	public String getName() {
		return name;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public SearchCategorySettings getSettings() {
		return settings;
	}

	public SearchCategory(String name, boolean selected, SearchCategorySettings settings) {
		this.name = name;
		this.selected = selected;
		this.settings = settings;
	}

	public SearchCategory(String name, boolean selected) {
		super();
		this.name = name;
		this.selected = selected;
	}
	
}
