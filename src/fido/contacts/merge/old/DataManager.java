//package fido.contacts.merge.old;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import android.content.ContentProviderOperation;
//import android.content.ContentResolver;
//import android.content.ContentValues;
//import android.content.OperationApplicationException;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.RemoteException;
//import android.provider.ContactsContract;
//import android.provider.ContactsContract.AggregationExceptions;
//
///**
// * @author Fido
// * Singleton that manages database operations and aggregation candidates collection.
// */
//public final class DataManager {
//	
//	/**
//	 * STATIC
//	 */
//	
//	/**
//	 * @author david_000
//	 * Contract class for this app's own mimetype in data table.
//	 */
//	private static final class AppMimetypeContract{
//		
//		/**
//		 * mimetype string identifier
//		 */
//		private static final String MIMETYPE = "vnd.android.cursor.item/fido";
//		
//		/**
//		 * Alias for data1 column. It's used for storing the id a contact is aggregated with.
//		 */
//		private static final String AGGREGATED_WITH = "data1";
//	}
//	
//	
//	
//	/**
//	 * Data manager instance.
//	 */
//	private static DataManager dataManager;
//		
//	/**
//	 * Method for instance creation.
//	 * @param contentResolver ContentResolver from an Activity.
//	 * @throws DataManagerException If instance was already created.
//	 */
//	public static void create(ContentResolver contentResolver) throws DataManagerException{
//		if (dataManager != null) {
//            throw new DataManagerException(DataManagerException.MANAGER_ALREADY_CREATED);
//        }
//		dataManager = new DataManager(contentResolver);
//	}
//	
//	
//	/**
//	 * Destroys the instance
//	 */
//	public static void destroy(){
//		dataManager = null;
//	}
//	
//	/**
//	 * Getter of the instance.
//	 * @return instance of DataManager.
//	 * @throws DataManagerException If instance hasn't been created yet.
//	 */
//	public static DataManager get() throws DataManagerException {
//         if (dataManager == null) {
//             throw new DataManagerException(DataManagerException.MANAGER_NULL);
//         }
//         return dataManager;
//    }
//	
//	/**
//	 * Determines if there is already an instance of DataManager created.
//	 * @return true if data manager has been already created.
//	 */
//	public static boolean isInit(){
//		return dataManager != null;
//	}
//
//	
//	
//	
//	/**
//	 * INSTANCE
//	 */
//
//	
//	private HashMap<Integer, VirtualContact> virtualContactsMapedByContactId;
//	
//	private HashMap<Integer, VirtualContact> virtualContactsMapedByRawContactId;
//	
//	/**
//	 * Instance of content resolver. It is used for database querying.
//	 */
//	private ContentResolver contentResolver;
//	
//	/**
//	 * Map containing Aggregation Candidates as values. Keys are pairs of lookupKey. It's used only while searching.
//	 * The key value character is used to determine if some combination of contact was already found.
//	 */
//	private HashMap<LookupPair, AggregationCandidate> aggregationCandidates;
//	
//	/**
//	 * List of aggregation candidates. It\s used in UI where the list collection is better for the adapter.
//	 */
//	private ArrayList<AggregationCandidate> aggregationCandidatesList;
//	
//	/**
//	 * List of contacts found as matches with another contact. It ensures that contact data are loaded only once, 
//	 * regardless of the number of aggregation candidates this contact is linked to.
//	 */
//	private HashMap<String, Contact> aggregationContacts;
//	
//	/**
//	 * Determines if there was a change in aggregationCandidatesList for example if we discard 
//	 * {@link AggregationCandidate} in UI
//	 */
//	private boolean aggregaionCandidatesListChanged;
//	
//	private boolean aggregatedContactsListChanged;
//	
//	/**
//	 * Contact chosen to aggregate individually
//	 */
//	private AggregationCandidate individualAggregation;
//	
//	/**
//	 * List of database operations to be queried in one batch.
//	 */
//	private ArrayList<ContentProviderOperation> providerOperations;
//	
//	
//	private HashSet<NumberPair>	aggregationExceptions;
//	
//	private int numberOfContacts;
//	
//	private HashMap<NumberPair,AggregatedContact> aggregatedContacts;
//	
//	private ArrayList<AggregatedContact> aggregatedContactsList;
//	
//	private HashMap<Integer,RawContact> aggregatedRawContacts;
//	
//	/**
//	 * Returns an array of all aggregation candidates.
//	 * @return Array of all aggregation candidates.
//	 */
//	public ArrayList<AggregationCandidate> getAggregationCandidatesList() {	
//		return aggregationCandidatesList;
//	}
//	
//	public boolean isAggregaionCandidatesListChanged() {
//		return aggregaionCandidatesListChanged;
//	}
//	
//	public boolean isAggregatedContactsListChanged() {
//		return aggregatedContactsListChanged;
//	}
//	
//	public AggregationCandidate getIndividualAggregation() {
//		return individualAggregation;
//	}
//
//	public void setIndividualAggregation(AggregationCandidate individualAggregation) {
//		this.individualAggregation = individualAggregation;
//	}
//	
//	public int getNumberOfContacts() {
//		return numberOfContacts;
//	}	
//	
//	
//	public ArrayList<AggregatedContact> getAggregatedContactsList() {
//		return aggregatedContactsList;
//	}
//
//
//
//
//	/**
//	 * Constructor
//	 * @param contentResolver ContentResolver for querying
//	 */
//	private DataManager(ContentResolver contentResolver){
//		this.contentResolver = contentResolver;
//		
//	}
//	
//	public void initSearch(){
//		initVirtualContacts();
//		aggregationContacts = new HashMap<String, Contact>();
//		aggregationCandidates = new HashMap<LookupPair, AggregationCandidate>();
//		initContactCount();
//	}
//	
//	private void initVirtualContacts() {
//		virtualContactsMapedByContactId = new HashMap<Integer, VirtualContact>();
//		virtualContactsMapedByRawContactId = new HashMap<Integer, VirtualContact>();
//		loadVirtualContacts();
//		joinVirtualContactsOnAggregationExceptions();
////		for (Map.Entry<Integer, VirtualContact> entry : virtualContactsMapedByRawContactId.entrySet()) {			
////			System.out.println(entry.getKey() + "=" + entry.getValue().getContactId());
////		}
//	}
//
//
//	private void joinVirtualContactsOnAggregationExceptions() {
//		intitAggregationExceptions();
//		for (NumberPair aggregationException : aggregationExceptions) {
//			joinVirtualContacts(aggregationException.getFirstNumber(), aggregationException.getSecondNumber());
//		}
//	}
//
//
//	private void joinVirtualContacts(int firstRawContactId, int secondRawContactId) {
////		System.out.println(firstRawContactId + "x" + secondRawContactId);
//		VirtualContact firstVirtualContact = virtualContactsMapedByRawContactId.get(firstRawContactId);
//		VirtualContact secondVirtualContact = virtualContactsMapedByRawContactId.get(secondRawContactId);
//		if(firstVirtualContact != null && secondVirtualContact != null){ // this check is useful if locked contacts are connected
//			if(firstVirtualContact != secondVirtualContact){
//				for (int rawContactId : secondVirtualContact.getRawContacts()) {
//					firstVirtualContact.addRawContact(rawContactId);
//					virtualContactsMapedByRawContactId.put(rawContactId, firstVirtualContact);
//				}
//			}
//		}	
//	}
//	
//	public VirtualContact getVirtualContactByRawContactId(int rawContactId){
//		return virtualContactsMapedByRawContactId.get(rawContactId);
//	}
//
//
//	private void loadVirtualContacts() {
//		Cursor rawContactsCursor = queryAllRawContactsForVirtualContacts();
//		rawContactsCursor.moveToFirst();
//		do{
//			int contactId = rawContactsCursor.getInt(
//					rawContactsCursor.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));
//			int rawContactId = rawContactsCursor.getInt(
//					rawContactsCursor.getColumnIndex(ContactsContract.RawContacts._ID));
//			rawContactToVirtualContact(contactId, rawContactId);
//		}while(rawContactsCursor.moveToNext());
//		rawContactsCursor.close();
//	}
//
//
//	private void rawContactToVirtualContact(int contactId, int rawContactId) {
//		VirtualContact virtualContact;
//		if(virtualContactsMapedByContactId.containsKey(contactId)){
//			virtualContact = virtualContactsMapedByContactId.get(contactId);
//			virtualContact.addRawContact(rawContactId);
//		}
//		else{
//			virtualContact = new VirtualContact(contactId, rawContactId);
//			virtualContactsMapedByContactId.put(contactId, virtualContact);		
//		}
//		virtualContactsMapedByRawContactId.put(rawContactId, virtualContact);
//	}
//
//
//	public void initManageMerged(){
//		aggregatedContacts = new HashMap<NumberPair,AggregatedContact>();
//		aggregatedRawContacts =  new HashMap<Integer, RawContact>();
//	}
//	
//	
//	
//
//	/**
//	 * Adds new aggregation candidate to the map.
//	 * @param firstRawContactId raw contact id of the first raw contact.
//	 * @param secondRawContactId raw contact of the second raw contact.
//	 * @param methodID Integer identifying the string that contains the name of the method with that was aggregation 
//	 * candidates found.
//	 */
//	public void addAgregaitonCandidate(int firstRawContactId, int secondRawContactId, int methodID){
//		Contact firstContact = getContact(firstRawContactId);
//		Contact secondContact = getContact(secondRawContactId);
//		
//		LookupPair aggregationKey = new LookupPair(firstRawContactId, secondRawContactId);
//		if(aggregationCandidates.containsKey(aggregationKey)){
//			aggregationCandidates.get(aggregationKey).addMethod(methodID);
//		}
//		else{
//			aggregationCandidates.put(aggregationKey, new AggregationCandidate(firstContact, secondContact, methodID));
//		}
//	}
//	
//	/**
//	 * Aggregates two contacts in database based on given {@link AggregationCandidate}.
//	 * @param aggCandidate {@link AggregationCandidate}.
//	 */
//	public void aggregateContacts(AggregationCandidate aggCandidate) {
//		 ContentValues cv = new ContentValues();
//		 cv.put(AggregationExceptions.TYPE, AggregationExceptions.TYPE_KEEP_TOGETHER);
//		 cv.put(AggregationExceptions.RAW_CONTACT_ID1, aggCandidate.getFirstRawContact().getRawContactId());
//		 cv.put(AggregationExceptions.RAW_CONTACT_ID2, aggCandidate.getSecondRawContact().getRawContactId());
//		 contentResolver.update(AggregationExceptions.CONTENT_URI, cv, null, null);
//		 System.out.println(aggCandidate.getFirstRawContact().getRawContactId() + "-" + aggCandidate.getSecondRawContact().getRawContactId());
//	}
//	
//	/**
//	 * Clear the sign of changes in {@link AggregationCandidate} list.
//	 */
//	public void clearAggregaionCandidatesListChanged() {
//		aggregaionCandidatesListChanged = false;
//	}
//	
//	public void clearAggregatedContactsListChanged() {
//		aggregatedContactsListChanged = false;
//	}
//	
//	/**
//	 * Executes provider operations batch and destroys the list.
//	 * @throws OperationApplicationException 
//	 * @throws RemoteException 
//	 */
//	public void executeProviderOperations() throws RemoteException, OperationApplicationException {
//			contentResolver.applyBatch(ContactsContract.AUTHORITY, providerOperations);
//			providerOperations = null;	
//	}
//	
//	public boolean aggregationExceptionExist(int firstRawContactId, int secondRawContactId){
//		NumberPair rawContactIdPair = new NumberPair(firstRawContactId, secondRawContactId);
////		System.out.println(rawContactIdPair.getFirstNumber() + "+" + rawContactIdPair.getSecondNumber());
//		return aggregationExceptions.contains(rawContactIdPair);
//	}
//	
//	/**
//	 * Fill all aggregation candidates with data. This method iterates through the cursor with data, and fill 
//	 * appropriate contact from AggregationContactList with every row.
//	 */
//	public void fillContacts(){
//		if(aggregationCandidates.size() < 1){
//			aggregationCandidatesList = new ArrayList<AggregationCandidate>();
//			return;
//		}
//		Cursor candidatesData = queryAggregationContactsData();
//		candidatesData.moveToFirst();		
//		do{
//			Contact contact = aggregationContacts.get(candidatesData.getString(candidatesData.getColumnIndex(
//					ContactsContract.Data.LOOKUP_KEY)));
//			contact.fillContact(
//					candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Data.DISPLAY_NAME)), 
//					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)),
//					candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Data.PHOTO_URI)),
//					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data.PHOTO_ID)));
//		}while(candidatesData.moveToNext());
//		candidatesData.close();
//		aggregationCandidatesList = new ArrayList<AggregationCandidate>(aggregationCandidates.values());
//	}
//	
//	
//	private void fillVirtualContacts() {
//		if(aggregationCandidates.size() < 1){
//			aggregationCandidatesList = new ArrayList<AggregationCandidate>();
//			return;
//		}
//		Cursor candidatesData = queryAggregationVirtualContactsData();
//		candidatesData.moveToFirst();		
//		do{
//			Contact contact = aggregationContacts.get(candidatesData.getString(candidatesData.getColumnIndex(
//					ContactsContract.Data.LOOKUP_KEY)));
//			contact.fillContact(
//					candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Data.DISPLAY_NAME)), 
//					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)),
//					candidatesData.getString(candidatesData.getColumnIndex(ContactsContract.Data.PHOTO_URI)),
//					candidatesData.getInt(candidatesData.getColumnIndex(ContactsContract.Data.PHOTO_ID)));
//		}while(candidatesData.moveToNext());
//		candidatesData.close();
//		aggregationCandidatesList = new ArrayList<AggregationCandidate>(aggregationCandidates.values());
//		
//	}
//
//
//	private Cursor queryAggregationVirtualContactsData() {
//		String[] projection = new String[]{	ContactsContract.Data.DISPLAY_NAME, 
//											ContactsContract.Data.RAW_CONTACT_ID,
//											ContactsContract.Data.PHOTO_URI,
//											ContactsContract.Data.PHOTO_ID};
//		ArrayList<RawContact> rawContacts = getRawContactsFromAggregationVirtualContcts();
//		String selection = ContactsContract.Data.RAW_CONTACT_ID + " IN " + buildRawContactIdSelection(rawContacts);
//		return queryData(projection, selection);
//	}
//
//
//	private ArrayList<RawContact> getRawContactsFromAggregationVirtualContcts() {
//		ArrayList<AggregationCandidate> aggregationCandidates = 
//				(ArrayList<AggregationCandidate>) this.aggregationCandidates.values();
//		ArrayList<RawContact> rawContactsFromVirtualContacts = new ArrayList<RawContact>();
//		for (AggregationCandidate aggregationCandidate : aggregationCandidates) {
//			VirtualContact virtualContact = virtualContactsMapedByRawContactId.get(
//					aggregationCandidate.getFirstRawContact().getRawContactId());
//			
//		}
//		return null;
//	}
//
//
//	/**
//	 * Fill all aggregation candidates with data. This method iterates through the cursor with data, and fill 
//	 * appropriate contact from AggregationContactList with every row.
//	 */
//	public void fillRawContacts(){
//		
//		//	If there are no contacts to aggregate	
//		if(aggregatedContacts.size() < 1){
//			aggregatedContactsList = new ArrayList<AggregatedContact>();
//			return;
//		}
//		
//		Cursor aggregatedContactsData = queryAggregatedContactsData();
//		aggregatedContactsData.moveToFirst();		
//		do{
//			RawContact contact = aggregatedRawContacts.get(aggregatedContactsData.getInt(aggregatedContactsData.getColumnIndex(
//					ContactsContract.Data.RAW_CONTACT_ID)));
//			String mimetype = aggregatedContactsData.getString(aggregatedContactsData.getColumnIndex(
//					ContactsContract.Data.MIMETYPE));
//			if(mimetype.equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)){
//				contact.setDisplayName(aggregatedContactsData.getString(aggregatedContactsData.getColumnIndex(
//						ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME)));
//			}
//			else if(mimetype.equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)){
//				contact.setPhoto(aggregatedContactsData.getBlob(aggregatedContactsData.getColumnIndex(
//						ContactsContract.CommonDataKinds.Photo.PHOTO)));
//			}
//			else if(mimetype.equals(AppMimetypeContract.MIMETYPE)){
//				int firstRawContactId = aggregatedContactsData.getInt(aggregatedContactsData.getColumnIndex(
//						ContactsContract.Data.RAW_CONTACT_ID));
//				int secondRawContactId = aggregatedContactsData.getInt(aggregatedContactsData.getColumnIndex(
//						AppMimetypeContract.AGGREGATED_WITH));
//				AggregatedContact aggregatedContact = 
//						aggregatedContacts.get(new NumberPair(firstRawContactId, secondRawContactId));
//				if(aggregatedContact != null){
////				System.out.println(firstRawContactId + "-" + secondRawContactId);
//					aggregatedContact.setAggregatedWithApp(true);
//				}			
//			}
//			else{
//				System.out.println("chyba");
//			}
//		}while(aggregatedContactsData.moveToNext());
//		aggregatedContactsData.close();
//	}
//	
//	/**
//	 * Returns an {@link ArrayList} of {@link AggregationCandidate} already approved by user to merge.
//	 * @return {@link ArrayList} of {@link AggregationCandidate} approved by user to merge.
//	 */
//	public ArrayList<AggregationCandidate> getAggregationCandidatesForMerge(){
//		ArrayList<AggregationCandidate> aggregationCandidatesListTmp = 
//				new ArrayList<AggregationCandidate>();
//		if(individualAggregation != null){	
//			aggregationCandidatesListTmp.add(individualAggregation);
//		}
//		else{
//			aggregationCandidatesListTmp = getApprovedAggregationCandidatesList();
//		}
//		return aggregationCandidatesListTmp;
//	}
//	
//	/**
//	 * Initialize the providerOperations {@link ArrayList}. This has to be called before adding any operation.
//	 */
//	public void initProvierOperations() {
//		providerOperations = new ArrayList<ContentProviderOperation>();
//	}
//	
//	/**
//	 * Query data from ContactsContract.Data table corresponding given projection and selection.
//	 * @param projection Defines which columns we want.
//	 * @param selection Defines which rows we want.
//	 * @return Cursor with chosen data.
//	 */
//	public Cursor queryData(String[] projection, String selection)
//    {
//    	Uri uri = ContactsContract.Data.CONTENT_URI;
//    	selection += " AND " + ContactsContract.Contacts.IN_VISIBLE_GROUP +" = 1";
//    	return query(uri, projection, selection);
//    }
//	
//	/**
//	 * Remove {@link AggregationCandidate} from the list for the UI.
//	 * @param aggregationCandidate {@link AggregationCandidate}
//	 */
//	public void removeAggregationCandidate(AggregationCandidate aggregationCandidate){
//		aggregationCandidatesList.remove(aggregationCandidate);
//		aggregaionCandidatesListChanged = true;
//	}
//	
//	/**
//	 * Sets up batch operations for updating one aggregated"s contact Display name and id.
//	 * @param aggregationCandidate {@link AggregationCandidate}
//	 */
//	public void setupContactUpdateBatch(AggregationCandidate aggregationCandidate) {
//		int firstRawContactId = aggregationCandidate.getFirstRawContact().getRawContactId();
//		int secondRawContactId = aggregationCandidate.getSecondRawContact().getRawContactId();
//		
//		// mark for disconnecting
//		addAppMark(firstRawContactId, secondRawContactId);
//		addAppMark(secondRawContactId, firstRawContactId);
//		
//		int contactId = getContactId(aggregationCandidate.getFirstRawContact().getRawContactId());
//		Cursor contactCursor = queryContactDisplayNameAndPhotoId(contactId);
//		String contactDisplayName = 
//				contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//		if(!contactDisplayName.equals(aggregationCandidate.getChosenNameContact().getDisplayName())){
//			Contact notChosen = aggregationCandidate.getChosenNameContact() == aggregationCandidate.getFirstRawContact() ? 
//					aggregationCandidate.getSecondRawContact() : aggregationCandidate.getFirstRawContact();
//			AddOpDeletePreviousOverideRows(aggregationCandidate, notChosen, contactId);
//			AddOpOveridePrimaryContactDisplayName(aggregationCandidate, notChosen);
//		}
//		int contactPhotoID = contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
//		if(contactPhotoID != aggregationCandidate.getChosenPhotoContact().getPhotoId()){
//			AddOpUpdatePhotoId(aggregationCandidate);
//		}
//	}
//	
//	
//	private void addAppMark(int rawContactId, int secondRawContactId) {
//		ContentProviderOperation.Builder opInsert = 
//				ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
//		opInsert.withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
//		opInsert.withValue(ContactsContract.Data.MIMETYPE, AppMimetypeContract.MIMETYPE);
//		opInsert.withValue(AppMimetypeContract.AGGREGATED_WITH, secondRawContactId);
//		providerOperations.add(opInsert.build());
//	}
//	
//	private void removeAppMark(int rawContactId, int secondRawContactId) {	
//		String where = ContactsContract.Data.RAW_CONTACT_ID + " = " + rawContactId + " AND " + 
//				ContactsContract.Data.MIMETYPE + " = '" + AppMimetypeContract.MIMETYPE + "' AND " + 
//				AppMimetypeContract.AGGREGATED_WITH + " = " + secondRawContactId;
//		contentResolver.delete(ContactsContract.Data.CONTENT_URI, where, null);
//	}
//
//
//	public void findAggregatedContacts(){
//		Cursor aggregatedContactsCursor = queryAggregationExceptions();
//		aggregatedContactsCursor.moveToFirst();
//		do {
//			int firstRawContactId = aggregatedContactsCursor.getInt(
//					aggregatedContactsCursor.getColumnIndex(AggregationExceptions.RAW_CONTACT_ID1));
//			int secondRawContactId = aggregatedContactsCursor.getInt(
//					aggregatedContactsCursor.getColumnIndex(AggregationExceptions.RAW_CONTACT_ID2));
//			addAggregatedContact(firstRawContactId, secondRawContactId);
//		} while(aggregatedContactsCursor.moveToNext());
//		fillRawContacts();
//		aggregatedContactsList = new ArrayList<AggregatedContact>(aggregatedContacts.values());
//		Iterator<AggregatedContact> it = aggregatedContactsList.iterator();
//		while (it.hasNext()) {
//			AggregatedContact aggregatedContact = it.next(); 
//			if(aggregatedContact.getFirstRawContact().getDisplayName() == null || 
//					aggregatedContact.getSecondRawContact().getDisplayName() == null){
//				it.remove();
//				
//				
//				
//			}
//		}
//	}
//	
//	
//	
//	private void addAggregatedContact(int firstRawContactId, int secondRawContactId) {
//		NumberPair rawContactIdPair = new NumberPair(firstRawContactId, secondRawContactId);
//		if(!aggregatedContacts.containsKey(rawContactIdPair)){
//			RawContact firstRawContact = getRawContact(firstRawContactId);
//			RawContact secondRawContact= getRawContact(secondRawContactId);
//			AggregatedContact aggregatedContact = new AggregatedContact(firstRawContact, secondRawContact); 
//			aggregatedContacts.put(rawContactIdPair, aggregatedContact);
////			System.out.println(firstRawContactId + "-" + secondRawContactId);
//		}		
//	}
//
//
//	private Cursor queryAggregatedContacts() {
//		Uri aggregationExceptionsUri = ContactsContract.AggregationExceptions.CONTENT_URI;
//		String[] projection = new String[]{ContactsContract.AggregationExceptions.RAW_CONTACT_ID1, 
//				ContactsContract.AggregationExceptions.RAW_CONTACT_ID2};
//		String selection = ContactsContract.AggregationExceptions.TYPE + " = " + 
//				ContactsContract.AggregationExceptions.TYPE_KEEP_TOGETHER;
//		return query(aggregationExceptionsUri, projection, selection);
//	}
//
//
//	/**
//	 * Constructs bath operation for delete previous contact display name override rows, and add the operation to 
//	 * providerOperations {@link ArrayList}. 
//	 * @param aggregationCandidate {@link AggregationCandidate}.
//	 */
//	private void AddOpDeletePreviousOverideRows(AggregationCandidate aggregationCandidate, Contact notChosen, 
//			int contactId) {
//		ContentProviderOperation.Builder opDelete = 
//				ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI);
//		opDelete.withSelection(ContactsContract.Data.CONTACT_ID + " = ? AND " + 
//				ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.DATA14 + " = ?",
//			new String[]{String.valueOf(contactId), 
//				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
//				"fido"});
//		providerOperations.add(opDelete.build());
//	}
//	
//	/**
//	 * Constructs bath operation for insert override rows with contact display name, and add the operation to 
//	 * providerOperations {@link ArrayList}. 
//	 * @param aggregationCandidate {@link AggregationCandidate}.
//	 */
//	private void AddOpOveridePrimaryContactDisplayName(AggregationCandidate aggregationCandidate, Contact notChosen) {		
//		ContentProviderOperation.Builder opInsert = 
//				ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
//		opInsert.withValue(ContactsContract.Data.MIMETYPE, 
//				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
//		opInsert.withValue(ContactsContract.Data.DATA1, aggregationCandidate.getChosenNameContact().getDisplayName());
//		opInsert.withValue(ContactsContract.Data.DATA14, "fido");
//		opInsert.withValue(ContactsContract.Data.RAW_CONTACT_ID, notChosen.getRawContactId());
//		opInsert.withValue(ContactsContract.Data.IS_PRIMARY, 1);
//		providerOperations.add(opInsert.build());
//	}
//	
//	/**
//	 * Constructs bath operation for updating contact photo id, and add the operation to providerOperations 
//	 * {@link ArrayList}.
//	 * @param aggregationCandidate {@link AggregationCandidate}.
//	 */
//	private void AddOpUpdatePhotoId(AggregationCandidate aggregationCandidate) {
//		ContentProviderOperation.Builder opUpdate = 
//				ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
//		opUpdate.withSelection(ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + 
//				ContactsContract.Data.MIMETYPE + " = ?", 
//			new String[]{String.valueOf(aggregationCandidate.getChosenPhotoContact().getRawContactId()), 
//				ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE});
//		opUpdate.withValue(ContactsContract.Data.IS_PRIMARY, 1);
//		opUpdate.withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1);
//		providerOperations.add(opUpdate.build());
//	}
//	
//	/**
//	 * Builds a selection string based on lookup strings from AggregationContactslist.
//	 * @return {@link String} lookUpString
//	 */
//	private String buildLookupSelection() {
//		ArrayList<Contact> contacts = new ArrayList<Contact>(aggregationContacts.values());
//		String lookupString = "(";
//		for (int i = 0; i < contacts.size() - 1; i++) {
//			String lookupKey = contacts.get(i).getLookupKey();
//			lookupString += "'" + lookupKey + "',";
//		}
//		String lastLookupKey = contacts.get(contacts.size() - 1).getLookupKey();
//		lookupString += "'" + lastLookupKey + "')";
//		return lookupString;
//	}
//	
//	private String buildRawContactIdSelection(ArrayList<RawContact> rawContacts) {
//		String rawContactIdString = "(";
//		for (int i = 0; i < rawContacts.size() - 1; i++) {
//			String rawContactId = Integer.toString(rawContacts.get(i).getRawContactId());
//			rawContactIdString += "'" + rawContactId + "',";
//		}
//		String lastRawContactId = Integer.toString(rawContacts.get(rawContacts.size() - 1).getRawContactId());
//		rawContactIdString += "'" + lastRawContactId + "')";
//		return rawContactIdString;
//	}
//		
//	/**
//	 * Returns a candidate from the list or create new if it dos't exist yet.
//	 * @param lookup {@link String} lookupKey
//	 * @return contact {@link Contact}
//	 */
//	private Contact getContact(String lookup) {
//		Contact contact;
//		if (aggregationContacts.containsKey(lookup)){
//			contact = aggregationContacts.get(lookup);
//		}
//		else {
//			contact = new Contact(lookup);
//			aggregationContacts.put(lookup, contact);
//		}
//		return contact;
//	}
//
//	private RawContact getRawContact(int rawContactId) {
//		RawContact contact;
//		if (aggregatedRawContacts.containsKey(rawContactId)){
//			contact = aggregatedRawContacts.get(rawContactId);
//		}
//		else {
//			contact = new RawContact(rawContactId);
//			aggregatedRawContacts.put(rawContactId, contact);
//		}
//		return contact;
//	}
//
//	
//	/**
//	 * Gets Cursor with contact display name and photoId based on contactId.
//	 * @param contactId - contact ID
//	 * @return {@link Cursor} with contact display name and photoId.
//	 */
//	private Cursor queryContactDisplayNameAndPhotoId(int contactId) {
//		String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.PHOTO_ID};
//		String selection = ContactsContract.Contacts._ID + " = " + contactId;
//		Cursor contactCursor = queryContact(projection, selection);
//		contactCursor.moveToFirst();
//		return contactCursor;
//	}
//	
//	/**
//	 * Gets contact ID by the rawContacId given.
//	 * @param rawContactId id oft the rawContact.
//	 * @return contact ID by the rawContacId given.
//	 */
//	private int getContactId(int rawContactId) {
//		String[] projection = new String[]{ContactsContract.RawContacts.CONTACT_ID};
//		String selection = ContactsContract.RawContacts._ID + " = " + rawContactId;
//		Cursor contactCursor = queryRawContact(projection, selection);
//		contactCursor.moveToFirst();
//		return contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));
//	}
//	
//	/**
//	 * Returns displayName of a contact based on given LookupKey.
//	 * @param lookupKey LookupKey of the contact.
//	 * @return String displayName of the contact.
//	 */
//	private String getDisplayName(String lookupKey){
//		Cursor cursor = queryDisplayName(lookupKey);
//		cursor.moveToFirst();
//		return cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//	}
//	
//	/**
//	 * Gets raw contact ID of the last aggregated contact.
//	 * @return raw contact ID of the last aggregated contact.
//	 */
//	private int getLastAggregatedRawContactId() {
//		Uri uri = ContactsContract.AggregationExceptions.CONTENT_URI;
//		String[] projection = new String[]{ContactsContract.AggregationExceptions.RAW_CONTACT_ID1};
//		String sortOrder = ContactsContract.AggregationExceptions._ID + " ASC LIMIT 1";
//		Cursor cursor = contentResolver.query(uri, projection, null, null, sortOrder);
//		cursor.moveToFirst();
//		return cursor.getInt(cursor.getColumnIndex(ContactsContract.AggregationExceptions.RAW_CONTACT_ID1));
//	}
//	
//	
//	private void intitAggregationExceptions() {
//		aggregationExceptions = new HashSet<NumberPair>();
//		Cursor exceptionsData = queryAggregationExceptions();
//		if(exceptionsData.moveToFirst()){
//			do {
//				NumberPair rawIdPair = new NumberPair(exceptionsData.getInt(exceptionsData.getColumnIndex(
//						ContactsContract.AggregationExceptions.RAW_CONTACT_ID1)), 
//					exceptionsData.getInt(exceptionsData.getColumnIndex(
//						ContactsContract.AggregationExceptions.RAW_CONTACT_ID2)));
//			aggregationExceptions.add(rawIdPair);
////			System.out.println(rawIdPair.getFirstNumber() + "-" + rawIdPair.getSecondNumber());
//			} while(exceptionsData.moveToNext());
//		}
//
//		exceptionsData.close();
//	}
//	
//	private void initContactCount(){
//		String[] projection = new String[]{ContactsContract.Contacts._COUNT};
//		Cursor contactCursor = queryContact(projection, null);
//		contactCursor.moveToFirst();
//		numberOfContacts = contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.Contacts._COUNT));
//	}
//	
//	/**
//	 * Basic querying method. 
//	 * @param uri Uri of the table.
//	 * @param projection Defines which columns we want.
//	 * @param selection Defines which rows we want.
//	 * @return Cursor with chosen data.
//	 */
//	private Cursor query(Uri uri, String[] projection, String selection){
//		return contentResolver.query(uri, projection, selection, null, null);
//	}
//	
//	/**
//	 * Gets data to for filling aggregation contacts.
//	 * @return {@link Cursor} with data for aggregation contacts.
//	 */
//	private Cursor queryAggregationContactsData(){
//		String[] projection = new String[]{	ContactsContract.Data.DISPLAY_NAME, 
//											ContactsContract.Data.RAW_CONTACT_ID,
//											ContactsContract.Data.LOOKUP_KEY, 
//											ContactsContract.Data.PHOTO_URI,
//											ContactsContract.Data.PHOTO_ID};
//		String selection = ContactsContract.Data.LOOKUP_KEY + " IN " + buildLookupSelection();
//		return queryData(projection, selection);
//	}
//	
//	private Cursor queryAggregatedContactsData(){
//		String[] projection = new String[]{	ContactsContract.Data.RAW_CONTACT_ID,
//											ContactsContract.Data.MIMETYPE,
//											ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
//											ContactsContract.CommonDataKinds.Photo.PHOTO};
//		ArrayList<RawContact> rawContacts = new ArrayList<RawContact>(aggregatedRawContacts.values());
//		String selection = ContactsContract.Data.RAW_CONTACT_ID + " IN " + buildRawContactIdSelection(rawContacts) + 
//				" AND " + ContactsContract.Data.MIMETYPE + " IN ('" + 
//					ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "','" + 
//					ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE + "','" +
//					AppMimetypeContract.MIMETYPE + "')";
//		return queryData(projection, selection);
//	}
//	
//	private Cursor queryAggregationExceptions(){
//		Uri uri = ContactsContract.AggregationExceptions.CONTENT_URI;
//		String[] projection = new String[]{	ContactsContract.AggregationExceptions.RAW_CONTACT_ID1, 
//				ContactsContract.AggregationExceptions.RAW_CONTACT_ID2};
//		String selection = ContactsContract.AggregationExceptions.TYPE + " = " + 
//				ContactsContract.AggregationExceptions.TYPE_KEEP_TOGETHER;
//		return query(uri, projection, selection);
//	}
//
//	/**
//	 * Method for querying data from ContactsContract.Contacts table.
//	 * @param projection Defines which columns we want.
//	 * @param selection Defines which rows we want.
//	 * @return Cursor with chosen data.
//	 */
//	private Cursor queryContact(String[] projection, String selection){
//    	Uri uri = ContactsContract.Contacts.CONTENT_URI;
//    	return query(uri, projection, selection);
//    }
//	
//	/**
//	 * Method for querying displayName based on contact's lookupKey.
//	 * @param lookupKey Contact's lookupKey.
//	 * @return Cursor with contact's displayName
//	 */
//	private Cursor queryDisplayName (String lookupKey){
//		String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
//		String selection = ContactsContract.Data.LOOKUP_KEY + " = '" + lookupKey + "'";
//		return queryContact(projection, selection);
//	}
//	
//	/**
//	 * Method for querying data from ContactsContract.RawContacts table.
//	 * @param projection Defines which columns we want.
//	 * @param selection Defines which rows we want.
//	 * @return Cursor with chosen data.
//	 */
//	private Cursor queryRawContact(String[] projection, String selection){
//    	Uri uri = ContactsContract.RawContacts.CONTENT_URI;
//    	return query(uri, projection, selection);
//    }
//	
//	private Cursor queryAllRawContactsForVirtualContacts(){
//		String[] projection = new String[]{ContactsContract.RawContacts.CONTACT_ID, ContactsContract.RawContacts._ID};
//		return queryRawContact(projection, null);
//	}
//
//
//	public ArrayList<AggregationCandidate> getApprovedAggregationCandidatesList() {
//		ArrayList<AggregationCandidate> approvedAggregationCandidates= new ArrayList<AggregationCandidate>();
//		for (AggregationCandidate candidate : aggregationCandidatesList) {
//			if(candidate.isMergeApproved()){
//				approvedAggregationCandidates.add(candidate);
//			}
//		}
//		return approvedAggregationCandidates;
//	}
//
//
//	public void disconnectContacts(AggregatedContact aggregatedContact) {
//		ContentValues cv = new ContentValues();
//		cv.put(AggregationExceptions.TYPE, AggregationExceptions.TYPE_KEEP_SEPARATE);
//		cv.put(AggregationExceptions.RAW_CONTACT_ID1, aggregatedContact.getFirstRawContact().getRawContactId());
//		cv.put(AggregationExceptions.RAW_CONTACT_ID2, aggregatedContact.getSecondRawContact().getRawContactId());
//		contentResolver.update(AggregationExceptions.CONTENT_URI, cv, null, null);
//		int firstRawContactId = aggregatedContact.getFirstRawContact().getRawContactId();
//		int secondRawContactId = aggregatedContact.getSecondRawContact().getRawContactId();
//		removeAppMark(firstRawContactId, secondRawContactId);
//		removeAppMark(secondRawContactId, firstRawContactId);
//		removeNameHack(firstRawContactId);
//		removeNameHack(secondRawContactId);
//	}
//
//
//	private void removeNameHack(int rawContactId) {
//		String where = ContactsContract.Data.RAW_CONTACT_ID + " = " + rawContactId + " AND " +
//						ContactsContract.Data.MIMETYPE + " = '" + 
//						ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "' AND " + 
//						ContactsContract.Data.DATA14 + " = 'fido'";
//		contentResolver.delete(ContactsContract.Data.CONTENT_URI, where, null);
//	}
//
//
//	public void removeMergedContact(AggregatedContact aggregatedContact) {
//		aggregatedContactsList.remove(aggregatedContact);
//		aggregatedContactsListChanged = true;
//	}
//
//
//	public void addTrickyName(Contact chosenNameContact) {
//		System.out.println(chosenNameContact.getRawContactId());
//		ContentValues cv = new ContentValues();
//		cv.put(ContactsContract.Data.RAW_CONTACT_ID, chosenNameContact.getRawContactId());
//		cv.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
//		cv.put(ContactsContract.Data.IS_PRIMARY, 1);
////		cv.put(ContactsContract.Data.IS_SUPER_PRIMARY, 1);
//		cv.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "žluťoučký kůň úpěl");
//		cv.put(ContactsContract.Data.DATA14, "fido");
//		contentResolver.insert(ContactsContract.Data.CONTENT_URI, cv);
//	}
//
//
//	public void removeTrickyNames() {
//		String where = ContactsContract.Data.MIMETYPE + " = '" + 
//				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "' AND " + 
//				ContactsContract.Data.DATA14 + " = 'fido'";
//		contentResolver.delete(ContactsContract.Data.CONTENT_URI, where, null);
//	}
//
//}
