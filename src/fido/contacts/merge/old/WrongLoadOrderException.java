package fido.contacts.merge.old;

public class WrongLoadOrderException extends Exception {
	private static final String EXCEPTION_TEXT = "Wrong loading order: ";
	
	public static final String CONTACT_ALREADY_FILLED_ERROR = "Contacts already filled!";	
	public static final String RAW_CONTACT_ALREADY_FILLED_ERROR = "Raw contacts already filled!";
	
	public WrongLoadOrderException(String message) {
		super(EXCEPTION_TEXT + message);
	}

	private static final long serialVersionUID = -6735835450274258122L;

}
