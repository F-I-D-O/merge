package fido.contacts.merge.old;

import java.util.ArrayList;


public class Contact {
	
	private long id;
	
	private String lookupKey;
	
	private ArrayList<RawContact> rawContacts;

	public Contact(long id, String lookupKey) {
		this.id = id;
		this.lookupKey = lookupKey;
	}
}
