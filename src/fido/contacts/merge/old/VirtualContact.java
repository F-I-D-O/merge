//package fido.contacts.merge.old;
//
//import java.util.ArrayList;
//
//import android.util.Log;
//
//import fido.contacts.merge.StringService;
//import fido.contacts.merge.data.RawContact;
//
//public class VirtualContactt {
//	
//	private int id;
//	
//	private int contactId;
//	
//	private ArrayList<RawContact> rawContacts;
//	
//	private String displayName;
//
//	public VirtualContact(int contactId, RawContact rawContact) {
//		this.contactId = contactId;
//		rawContacts = new ArrayList<RawContact>();
//		rawContacts.add(rawContact);
//		id = (int) (Math.random() * 1000);
//	}
//	
//	public void addRawContact(RawContact rawContact){
//		rawContacts.add(rawContact);
//	}
//
//	public int getId() {
//		return id;
//	}
//
//	public ArrayList<RawContact> getRawContacts() {
//		return rawContacts;
//	}
//
//	public int getContactId() {
//		return contactId;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		if(this.displayName == null){
//			this.displayName = displayName;
//		}
//		else if(!this.displayName.equals(displayName)){
//			Log.e("Virtual contact setDisplayName", "old: " + this.displayName + " new: " + displayName);
//		}
//		
//	}
//
//	public String getRawContactNamesString() {
//		String nameString = "(" + id + ") ";
//		for (RawContact rawContact : rawContacts) {
//			nameString += "(" + rawContact.getAccountType() + ")" + rawContact.getRawContactId() + " - " + rawContact.getDisplayName() + ", ";		
//		}
//		nameString = nameString.substring(
//				StringService.FIRST_CHARACTER_POSITION, StringService.getLastCharacterPosition(nameString) - 1);
//		return nameString;
//	}
//
//	
//	
//}
