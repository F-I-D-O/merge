//package fido.contacts.merge.old;
//
///**
// * Pair of raw contacts aggregated by aggregation exception.
// * @author Fido
// * @author david_000
// * Class representing AggregatedContact. Aggregated contact is a pair of raw contacts connected in agg_exceprions table.
// */
//public class AggregatedContact {
//	
//	/**
//	 * ID of the first raw contact
//	 * Id of the first raw contact in this aggregation pair.
//	 */
//	private RawContact firstRawContact;
//	
//	/**
//	 * ID of the second raw contact
//	 * Id of the second raw contact in this aggregation pair.
//	 */
//	private RawContact secondRawContact;
//
//	/**
//	 * Determines if this pair was aggregated by this application.
//	 * indicates if this aggregated contact was created by this app.
//	 */
//	private boolean aggregatedWithApp;
//	
//	
//	public RawContact getFirstRawContact() {
//		return firstRawContact;
//	}
//
//	public RawContact getSecondRawContact() {
//		return secondRawContact;
//	}
//
//	public boolean isAggregatedWithApp() {
//		return aggregatedWithApp;
//	}
//
//	public void setAggregatedWithApp(boolean aggregatedWithApp) {
//		this.aggregatedWithApp = aggregatedWithApp;
//	}
//	
//
//	/**
//	 * Main Constructor.
//	 * @param firstRawContact First aggregated {@link RawContact}. 
//	 * @param secondRawContact Second aggregated {@link RawContact}. 
//	 */
//	public AggregatedContact(RawContact firstRawContact, RawContact secondRawContact) {
//		this.firstRawContact = firstRawContact;
//		this.secondRawContact = secondRawContact;
//	}
//	
//	/**
//	 * Constructor for raw contact ID params. Not used.
//	 * @param firstRawContactId ID of the first aggregated raw contact.
//	 * @param secondRawContactId ID of the second aggregated raw contact.
//	 */
//	public AggregatedContact(int firstRawContactId, int secondRawContactId){
//		this(new RawContact(firstRawContactId), new RawContact(secondRawContactId));
//	}
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((firstRawContact == null) ? 0 : firstRawContact.hashCode());
//		result = prime * result + ((secondRawContact == null) ? 0 : secondRawContact.hashCode());
//		return result;
//	}
//
//	/* (non-Javadoc)
//	 * @see java.lang.Object#equals(java.lang.Object)
//	 * Return true if this 
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj){
//			return true;
//		}
//		if (obj == null || getClass() != obj.getClass()){
//			return false;
//		}
//		AggregatedContact other = (AggregatedContact) obj;
//		if (firstRawContact.equals(other.firstRawContact) && secondRawContact.equals(other.secondRawContact) ||
//				firstRawContact.equals(other.secondRawContact) && secondRawContact.equals(other.firstRawContact)) {
//			return true;	
//		}
//		return false;
//	}
//}
