package fido.contacts.merge;

import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.searchModules.ModuleManagerException;
import fido.contacts.merge.searchModules.ModulesManager;
import fido.contacts.merge.ui.SearchActivity;
import android.os.AsyncTask;

public class SearchThread extends AsyncTask<Void, Void, Void> {
	
	private SearchActivity searchActivity;

	public SearchThread(SearchActivity searchActivity) {
		this.searchActivity = searchActivity;
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			DataManager dataManager = DataManager.get();
			dataManager.initSearch();
			ModulesManager.get().runModules(this);
			dataManager.fillContacts();
			dataManager.groupAggregationCandidates();
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ModuleManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
		searchActivity.updateProgress();
	}


	@Override
	protected void onPostExecute(Void result) {
		searchActivity.completed();
		super.onPostExecute(result);
	}

	public void updateProgress() {
		publishProgress();
	}
	
	

}
