package fido.contacts.merge;

public enum BooleanSetting {
	
	FACEBOOK_CONTACTS_LOCKED("facebook_contacts_locked", true),
	APROVE_UNLOCK_DIALOG("approve_unlock_dialog", false),
	;
	
	private final String name;
	private final boolean defaultValue;
	
	public String getName() {
		return name;
	}

	public boolean getDefaultValue() {
		return defaultValue;
	}

	private BooleanSetting(String name, boolean defaultValue) {
        this.name = name;
        this.defaultValue = defaultValue;
    }
}
