package fido.contacts.merge;

public class Progress {
	protected int totalProgress = 0;
	private int totalProgressHeaderStringId;

	public int getTotalProgress() {
		return totalProgress;
	}

	public void setTotalProgress(int progress) {
		this.totalProgress = progress;
	}
	
	public int getTotalProgressHeaderStringId() {
		return totalProgressHeaderStringId;
	}

	public void setTotalProgressHeaderStringId(int totalProgressHeaderStringId) {
		this.totalProgressHeaderStringId = totalProgressHeaderStringId;
	}


}
