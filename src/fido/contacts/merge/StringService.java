package fido.contacts.merge;

/**
 * @author Fido
 * Useful constants to work with trings.
 */
public class StringService {
	
	/**
	 * position of the first character in the string.
	 */
	public static final int FIRST_CHARACTER_POSITION = 0;
	
	/**
	 * Whitespace regex.
	 */
	public static final String WHITESPACES = "\\s";
	
	public static int getLastCharacterPosition(String string){
		return string.length() - 1;
	}
}
