package fido.contacts.merge;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SqliteDatabase {
	
	private SQLiteDatabase database;

	public SqliteDatabase(String filePath) {
		database = SQLiteDatabase.openDatabase(filePath, null, SQLiteDatabase.OPEN_READWRITE);
	}
	
	public void rawQuery(String sql){
		Cursor cursor = database.rawQuery(sql, null);
		cursor.moveToFirst();
		cursor.close();     
	}

}
