package fido.contacts.merge;

import java.util.ArrayList;

import fido.contacts.merge.data.AggregationCandidate;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.ui.IAsyncWait;
import android.content.OperationApplicationException;
import android.os.AsyncTask;
import android.os.RemoteException;

public class MergeThread extends AsyncTask<Void, Double, Void> {
	private IAsyncWait mergeActivity;
	
	private DataManager dataManager;

	public MergeThread(IAsyncWait mergeActivity) {
		this.mergeActivity = mergeActivity;
		try {
			dataManager = DataManager.get();
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected Void doInBackground(Void... params) {
//		System.out.println("doInBackgrnd");
		ArrayList<AggregationCandidate> candidates = dataManager.getAggregationCandidatesForMerge();
		try {
			merge(candidates);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OperationApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private void merge(ArrayList<AggregationCandidate> candidates) throws RemoteException, OperationApplicationException {
//		System.out.println(candidates.size());
		dataManager.initProvierOperations();
		for (AggregationCandidate aggCandidate : candidates) {
//			dataManager.addTrickyName(aggCandidate.getChosenNameContact());
			dataManager.aggregateContacts(aggCandidate);
			dataManager.setupContactUpdateBatch(aggCandidate);
			dataManager.removeAggregationCandidate(aggCandidate);
		}
//		dataManager.removeTrickyNames();
		dataManager.executeProviderOperations();
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		mergeActivity.asyncCompleted();
	}
}
