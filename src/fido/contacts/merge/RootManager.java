package fido.contacts.merge;

import java.io.DataOutputStream;
import java.io.File;
import java.util.List;
import java.util.Observable;

import android.os.AsyncTask;

import eu.chainfire.libsuperuser.Shell;
import fido.contacts.merge.data.TableService;

public class RootManager extends Observable{
	
	/*
	 * STATIC
	 */
	
		
	/*
	 * Commands
	 */
	
	private static final String COMMAND_SU = "su";
	private static final String COMMAND_COPY_CONTACT_DB_TO_APP_DIR = 
			"cp -f /data/data/com.android.providers.contacts/databases/contacts2.db /data/data/fido.contacts.merge/contacts2.db";
	private static final String COMMAND_COPY_CONTACT_DB_TO_DOWNLOAD_DIR = 
			"cp -f /data/data/com.android.providers.contacts/databases/contacts2.db /sdcard/download/contacts2.db";
	private static final String COMMAND_MOVE_CONTACT_DB_TO_CONTACT_PROVIDER_DIR_FROM_DOWNLOAD_DIR = 
			"cp -f /sdcard/download/contacts2.db /data/data/com.android.providers.contacts/databases/contacts2.db";
	private static final String COMMAND_MOVE_CONTACT_DB_TO_CONTACT_PROVIDER_DIR_FROM_APP_DIR = 
			"cp -f /data/data/fido.contacts.merge/contacts2.db /data/data/com.android.providers.contacts/databases/contacts2.db";
	private static final String COMMAND_REMOVE_CONTACT_DB_FROM_DOWNLOAD_DIR = 
			"rm /sdcard/download/contacts2.db";
	private static final String COMMAND_REMOVE_CONTACT_DB_FROM_CONTACT_PROVIDER_DIR = 
			"rm /data/data/com.android.providers.contacts/databases/contacts2.db";
	private static final String COMMAND_REMOVE_CONTACT_DB_FROM_APP_DIR = 
			"rm contacts2.db";
	private static final String COMMAND_REMOVE_CONTACT_DB_JOURNAL_FROM_APP_DIR = 
			"rm contacts2.db-journal";
	
	// its necessary to bundle sqlite3 command line program to run these commands
//	private static final String COMMAND_SQLITE3_LOCK = 
//			"adb shell /data/data/fido.contacts.merge/sqlite3 /data/data/com.android.providers.contacts/databases/contacts2.db \"UPDATE raw_contacts SET is_restricted = 1\"";
//	private static final String COMMAND_SQLITE3_UNLOCK = 
//			"adb shell /data/data/fido.contacts.merge/sqlite3 /data/data/com.android.providers.contacts/databases/contacts2.db \"UPDATE raw_contacts SET is_restricted = 0\"";
	
	private static final String COMMAND_DISABLE_PROVIDER = "pm disable com.android.providers.contacts";
	private static final String COMMAND_ENABLE_PROVIDER = "pm enable com.android.providers.contacts";
	private static final String COMMAND_MOVE_TO_PROVIDER_DIR = "cd /data/data/com.android.providers.contacts/";
	private static final String COMMAND_MOVE_TO_APP_DIR = "cd /data/data/fido.contacts.merge/";
	
	// awk is not anymore on android (:
//	private static final String COMMAND_SAVE_PROVIDER_OWNER_TO_VAR = "user=`ls -ld databases | awk '{print $3}'`";
//	private static final String COMMAND_SAVE_APP_OWNER_TO_VAR = "user=`ls -ld ../fido.contacts.merge | awk '{print $3}'`";
	
	private static final String COMMAND_SAVE_PROVIDER_OWNER_TO_VAR = "user=`ls -ld databases`; user=${user#* }; user=${user%% *}";
	private static final String COMMAND_SAVE_APP_OWNER_TO_VAR = "user=`ls -ld ../fido.contacts.merge`; user=${user#* }; user=${user%% *}";
	
	private static final String COMMAND_CHANGE_DATABASE_OWNER_TO_PROVIDER = "chown $user:$user databases/contacts2.db";
	private static final String COMMAND_CHANGE_DATABASE_OWNER_TO_APP = "chown $user:$user contacts2.db";
	private static final String COMMAND_EXIT = "exit";
	
	/*
	 * SQL
	 */
	private static final String SQL_LOCK_RAW_CONTACTS = "UPDATE raw_contacts SET is_restricted = 1 WHERE account_id IN (SELECT _id FROM accounts WHERE account_type IN ?);";
	private static final String SQL_UNLOCK_CONTACTS = "UPDATE contacts SET single_is_restricted = 0;";
	private static final String SQL_UNLOCK_RAW_CONTACTS = "UPDATE raw_contacts SET is_restricted = 0;";
	
	
	/*
	 * Other
	 */
	
	private static final String LOG_TAG = "root manager";
	
	private static final String LOG_MESSAGE_CONSOLE_OUT = "console output: ";
	
	private static final String[] LOCKED_ACCOUNT_TYPES = {
		"com.facebook.auth.login", 
		"com.sonyericsson.facebook.account"
	};
	

	
    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        try {
            File file = new File("/system/app/Superuser.apk");
            return file.exists();
        } 
        catch (Exception e){
        	return false;
        }
    }

//    private static boolean checkRootMethod3() {
//        return new ExecShell().executeCommand(ShelCmd.CHECK_SU_BINARY)!= null;
//    }
    
    private static boolean checkRootMethod4() {
        String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
                "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
        for (String where : places) {
            if (new File(where + COMMAND_SU).exists() ) {
                return true;
            }
        }
        return false;
    }
    
   
    
    
    /**
	 * INSTANCE
	 */
    
    private App app;
    
	private Process process;
	
	private DataOutputStream console;
	
	private boolean deviceRooted;
	
	private boolean deviceRootTestCompleted;
	
	
	
	public boolean isDeviceRooted() {
		return deviceRooted;
	}
	
	public boolean isDeviceRootTestCompleted(){
		return deviceRootTestCompleted;
	}
	
	private void setDeviceRootTestCompleted(boolean isCompleted){
		deviceRootTestCompleted = isCompleted;
		setChanged();
		notifyObservers();
	}
	
	private class RootAvailableTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			deviceRooted = Shell.SU.available();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			setDeviceRootTestCompleted(true);
		}
	}
    
    public RootManager(App app){
    	this.app = app;
    	new RootAvailableTask().execute();
	}
    
    public void printSql(){
    	String inString = TableService.buildInString(LOCKED_ACCOUNT_TYPES);
    	String sql = SQL_LOCK_RAW_CONTACTS.replace("?", inString);
    	app.getLogManager().logMessage("Root manager test SQL", sql);
    }
    
    public void executeSuCommand() throws Exception{
    	process = Runtime.getRuntime().exec(COMMAND_SU);   
    }
    
    private void executeSuCommand(String command) throws Exception{
    	process = Runtime.getRuntime().exec(COMMAND_SU);   
    	console = new DataOutputStream(process.getOutputStream()); 
    	console.writeBytes(command);  
		console.writeBytes(COMMAND_EXIT + "\n");
    	console.flush();   
    	process.waitFor();
    	if (process.exitValue() == 255) { 
    		throw new Exception("hdhd");
    	}
    }
    
    private void executeSuCommands(String[] commands) throws Exception{
    	process = Runtime.getRuntime().exec(COMMAND_SU); 
    	console = new DataOutputStream(process.getOutputStream()); 
    	for (String command : commands) {
    		console.writeBytes(command);  
    		app.getLogManager().logMessage(LOG_TAG, "command on stack: " + command);
		}    	
		console.writeBytes(COMMAND_EXIT + "\n");
		app.getLogManager().logMessage(LOG_TAG, "command on stack: " + COMMAND_EXIT);
    	console.flush();   
    	app.getLogManager().logMessage(LOG_TAG, "console flushed");
    	process.waitFor();
    	app.getLogManager().logMessage(LOG_TAG, "commands execution ended");
    	app.getLogManager().logMessage(LOG_TAG, "process exit value: " + process.exitValue() );
    	if (process.exitValue() == 255) { 
    		throw new Exception("hdhdn");
    	}
    }
    

    

    public void lockFacebookContacts() throws Exception{
    	SqliteDatabase db = loadDb();
		String inString = TableService.buildInString(LOCKED_ACCOUNT_TYPES);
    	String sql = SQL_LOCK_RAW_CONTACTS.replace("?", inString);
		db.rawQuery(sql);
		saveDb();
    }

	public void unlockFacebookContacts() throws Exception{
		SqliteDatabase db = loadDb();
		db.rawQuery(SQL_UNLOCK_RAW_CONTACTS);
		db.rawQuery(SQL_UNLOCK_CONTACTS);
		saveDb();
	}
	
	private SqliteDatabase loadDb() throws Exception{		
		String[] commands = {COMMAND_DISABLE_PROVIDER + "\n", 
				COMMAND_COPY_CONTACT_DB_TO_APP_DIR + "\n", 
				COMMAND_MOVE_TO_APP_DIR + "\n",
				COMMAND_SAVE_APP_OWNER_TO_VAR + "\n",
				COMMAND_CHANGE_DATABASE_OWNER_TO_APP + "\n",};
		List<String> consoleOut = Shell.SU.run(commands);
		logConsoleOut(consoleOut);
		return new SqliteDatabase(app.getApplicationInfo().dataDir + "/contacts2.db");
	}
	
	private SqliteDatabase loadDbTest() throws Exception{		
		String[] commands = {COMMAND_DISABLE_PROVIDER + "\n", 
				COMMAND_COPY_CONTACT_DB_TO_APP_DIR + "\n", 
				COMMAND_MOVE_TO_APP_DIR + "\n",
				COMMAND_SAVE_APP_OWNER_TO_VAR + "\n",
				COMMAND_CHANGE_DATABASE_OWNER_TO_APP + "\n",};
		
		List<String> consoleOut = Shell.SU.run(commands[0]);
		logConsoleOut(consoleOut);
		
		consoleOut = Shell.SU.run(commands[1]);
		logConsoleOut(consoleOut);
		
		consoleOut = Shell.SU.run(new String[]{commands[2],commands[3],commands[4]});
		logConsoleOut(consoleOut);
		
		return new SqliteDatabase(app.getApplicationInfo().dataDir + "/contacts2.db");
	}
	
	
	private void logConsoleOut(List<String> consoleOut) {
		for (String line : consoleOut) {
			app.getLogManager().logMessage(LOG_TAG, LOG_MESSAGE_CONSOLE_OUT + line);
		}	
	}

	private void saveDb() throws Exception{
		String[] commands = {COMMAND_MOVE_CONTACT_DB_TO_CONTACT_PROVIDER_DIR_FROM_APP_DIR + "\n",
        		COMMAND_MOVE_TO_PROVIDER_DIR + "\n", 
        		COMMAND_SAVE_PROVIDER_OWNER_TO_VAR + "\n", 
        		COMMAND_CHANGE_DATABASE_OWNER_TO_PROVIDER + "\n", 
        		COMMAND_MOVE_TO_APP_DIR + "\n",
        		COMMAND_REMOVE_CONTACT_DB_FROM_APP_DIR + "\n",
        		COMMAND_REMOVE_CONTACT_DB_JOURNAL_FROM_APP_DIR + "\n",
        		COMMAND_ENABLE_PROVIDER + "\n"};
		List<String> consoleOut = Shell.SU.run(commands);
		logConsoleOut(consoleOut);
	}

	
	
//	private void copyContactDbToDownloadDir() throws Exception{
//		console.writeBytes(COMMAND_COPY_CONTACT_DB_TO_DOWNLOAD_DIR + "\n");  
//		console.writeBytes(COMMAND_EXIT + "\n");
//    	console.flush();   
//    	process.waitFor();
//    	if (process.exitValue() == 255) { 
//    		throw new Exception("hdhd");
//    	}
//	}
//	
//	private void moveContactDbBack() throws Exception{
//		console.writeBytes(COMMAND_COPY_CONTACT_DB_TO_APP_DIR + "\n");  
//		console.writeBytes(COMMAND_EXIT + "\n");
//    	console.flush();   
//    	process.waitFor();
//    	if (process.exitValue() == 255) { 
//    		throw new Exception("hdhd");
//    	}
//	}
//
//	private void copyContactDbToAppDir() throws Exception{
//		console.writeBytes(COMMAND_COPY_CONTACT_DB_TO_APP_DIR + "\n");  
//		console.writeBytes(COMMAND_EXIT + "\n");
//    	console.flush();   
//    	process.waitFor();
//    	if (process.exitValue() == 255) { 
//    		throw new Exception("hdhd");
//    	}
//	}
//	
	
	
	
	
}
