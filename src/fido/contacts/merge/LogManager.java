package fido.contacts.merge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Environment;
import android.util.Log;

public class LogManager {
	
	private static final String ERROR_LOG_FILE_NOT_FOUND = "Application cannot acces log file";
	
	private static final String LOG_TAG = "Log manager error";
	
	private static final String UNCAUGHT_ERROR_TAG = "Uncaught error";
	
	private enum LogType {
		ERROR("Error"),
		MESSAGE("Message");
		
		private String label;

		private LogType(String label) {
			this.label = label;
		}
		
		public String getLabel(){
			return label;
		}

	}
	
	private String filename;
	
	private String log;
	
	private boolean loggingEnabled;
	
	
	
	
	public LogManager(String filename, boolean loggingEnabled) {
		this.filename = filename;
		this.loggingEnabled = loggingEnabled;
		log = "";
	}

	public void logError(String tag, String message){
		if(loggingEnabled){
			Log.e(tag, message);
			log += getLogMessage(LogType.ERROR, tag, message);
		}
	}
	
	public void logMessage(String tag, String message){
		if(loggingEnabled){
			Log.v(tag, message);
			log += getLogMessage(LogType.MESSAGE, tag, message);
		}
	}
	
	public void logUncaughtError(Throwable exception){
		if(loggingEnabled){
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			exception.printStackTrace(printWriter);
			String message = stringWriter.toString();
			log += getLogMessage(LogType.ERROR, UNCAUGHT_ERROR_TAG, message);
			save();
		}
	}

	public void save(){
	    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), 
	    		filename);
	    
	    try {
			FileOutputStream fos = new FileOutputStream(file, true);
			PrintWriter pw = new PrintWriter(fos);
			
			pw.write(log);
			pw.close();
			
			log = "";
			
		} catch (FileNotFoundException e) {
			logError(LOG_TAG, ERROR_LOG_FILE_NOT_FOUND);
		}
	    
	}
	
	private String getLogMessage(LogType logType, String tag, String message) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
		Date date = new Date();
		return String.format("%s | %s | %s | %s \n", dateFormat.format(date), logType.getLabel(), tag, message);
	}



}
