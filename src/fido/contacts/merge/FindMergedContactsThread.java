package fido.contacts.merge;

import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.ui.MergedContactsActivity;
import fido.contacts.merge.ui.optionsActivity.OptionsActivity;
import android.content.Intent;
import android.os.AsyncTask;

public class FindMergedContactsThread extends AsyncTask<Void, Void, Void> {

	private OptionsActivity optionsActivity;
	
	public FindMergedContactsThread(OptionsActivity optionsActivity) {
		super();
		this.optionsActivity = optionsActivity;
	}

	@Override
	protected Void doInBackground(Void... params) {	
		try {
			DataManager.create(optionsActivity.getContentResolver());
			DataManager dataManager = DataManager.get();
			dataManager.findAggregatedContacts();
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;		
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		optionsActivity.startActivity(new Intent(optionsActivity, MergedContactsActivity.class));
	}
	
	

}
