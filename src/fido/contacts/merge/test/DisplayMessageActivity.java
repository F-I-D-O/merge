package fido.contacts.merge.test;

import fido.contacts.merge.R;
import fido.contacts.merge.R.id;
import fido.contacts.merge.R.layout;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.ui.optionsActivity.OptionsActivity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SimpleCursorAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;

public class DisplayMessageActivity extends Activity {
	
	private ListView testView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		setContentView(R.layout.activity_display_message);
		
		Intent intent = getIntent();
//        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		
		// Show the Up button in the action bar.
		setupActionBar();
		
		setupActionBar();
		
//		testView = (ListView) findViewById(R.id.testList);
		
		populateContactList();

//		textView.setText(ContactsContract.Contacts.Entity.CONTENT_DIRECTORY.toString());
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@SuppressLint("NewApi")
	private void populateContactList() {
//        Cursor cursor = getAllRawContacts();
//        Cursor cursor = getAllContacts();
//        Cursor cursor = DataManager.getAllData(this.getContentResolver());
        
//        Test.saveColumnNames(this, cursor);
//        
////        names of cursor columns
//        String[] fields = new String[] {
//                ContactsContract.Data.DISPLAY_NAME
//        };
//        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.contact_entry, cursor,
//                fields, new int[] {R.id.contactEntryText}, 0);
//        testView.setAdapter(adapter);
    }
	
	
	
	
	
	

}
