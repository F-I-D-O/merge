package fido.contacts.merge.test;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.widget.Toast;
import fido.contacts.merge.ui.UICommon;

public final class Test {
	
	
	public static void printColumnNames (Activity activity, Cursor cursor) {
		int i = 0;
        String sloupce = "";
        while(i < cursor.getColumnCount()){
        	sloupce = sloupce + "/n" + cursor.getColumnName(i);
        	i++;
        }
        UICommon.messageBox(activity, "bddf", sloupce);
	}
	
	public static void saveColumnNames(Activity activity, Cursor cursor){
		int i = 0;
        String sloupce = "";
        while(i < cursor.getColumnCount()){
        	sloupce = String.format("%s%s%s",sloupce, System.getProperty("line.separator"), 
        			cursor.getColumnName(i)); 
        	i++;
        }
		
		// Get the directory for the user's public pictures directory. 
	    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), 
	    		"sloupce.txt");
	    
	    try {
			FileOutputStream fos = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(fos);
			
			pw.write(sloupce);
			pw.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//	    if (!file.mkdirs()) {
//	        Log.e(LOG_TAG, "Directory not created");
//	    }
//	    return file;
	    
	    

	}


	public static void saveAlkyMail(Activity activity){
		Account[] ac = AccountManager.get(activity).getAccounts();
		for (Account account : ac) {
			System.out.println(account.type + "-" +  account.name);
		}
		
		int i = 0;
	    String sloupce = "";
	    Uri uri = ContactsContract.RawContacts.CONTENT_URI;
	    Cursor cursor = activity.getContentResolver().query(uri, null, null, null, 
	    		ContactsContract.RawContacts._ID + " ASC");
//	    + " AND " + ContactsContract.Contacts.IN_VISIBLE_GROUP +" = 1"
	    if (!cursor.moveToFirst()){
	    	System.out.println("sdgsdgfdgfdgdgdgsdfgfhgf");
	    }
//	    while(i < cursor.getColumnCount()){
//	    	sloupce = String.format("%s%s%s: %s",sloupce, System.getProperty("line.separator"), 
//	    			cursor.getColumnName(i), cursor.getString(i)); 
//	    	i++;
//	    }
	    
	    while(cursor.moveToNext()){
	    	sloupce = String.format("%s%s[%s]%s: %s", sloupce, System.getProperty("line.separator"), 
	    			cursor.getString(cursor.getColumnIndex(ContactsContract.RawContacts._ID)),
	    			cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)),
	    			cursor.getString(cursor.getColumnIndex(ContactsContract.RawContacts.ACCOUNT_TYPE))); 
	    	i++;
	    }
		
		// Get the directory for the user's public pictures directory. 
	    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), 
	    		"alky.txt");
	    
	    try {
			FileOutputStream fos = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(fos);
			
			pw.write(sloupce);
			pw.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	    
	public static String getSelection() {
		return null;
	}
	
	public static void root(Activity act){
	    Process p;   
	    try {   
	       // Preform su to get root privledges  
	       p = Runtime.getRuntime().exec("su");   
	         
	       // Attempt to write a file to a root-only   
	       DataOutputStream os = new DataOutputStream(p.getOutputStream());   
	       System.out.println("pred");
	       os.writeBytes("cp /data/data/com.android.providers.contacts/databases/contacts2.db /data/data/fido.contacts.merge/contacts2.db\n");  
//	       os.flush(); 
	       System.out.println("po");
//	        Close the terminal  
	       os.writeBytes("exit\n");   
	       os.flush();   
	       System.out.println("poExitu");
	       try {   
	          p.waitFor();   
	               if (p.exitValue() != 255) {   
	                  // TODO Code to run on success  
	                  toastMessage("root", act);  
	                  SQLiteDatabase db = SQLiteDatabase.openDatabase("/data/data/fido.contacts.merge/contacts2.db", null, SQLiteDatabase.OPEN_READWRITE);
	                  db.rawQuery("UPDATE raw_contacts SET is_restricted = 0", null);
	               }   
	               else {   
	                   // TODO Code to run on unsuccessful  
	                   toastMessage("not root", act);      
	               }   
	       } catch (InterruptedException e) {   
	          // TODO Code to run in interrupted exception  
	           toastMessage("not root", act);   
	       }   
	    } catch (IOException e) {   
	       // TODO Code to run in input/output exception  
	        toastMessage("not root", act);   
	    }  
	}

	private static void toastMessage(String string, Activity act) {
		Context context = act.getApplicationContext();
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, string, duration);
		toast.show();
		
	}
	
	public static void changRest(Activity act) {
		SQLiteDatabase db = SQLiteDatabase.openDatabase(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/contacts2.db", null, SQLiteDatabase.OPEN_READWRITE);
        Cursor c = db.rawQuery("UPDATE raw_contacts SET is_restricted = 0", null);
        c.moveToFirst();
        c.close();
        db.close();
        System.out.println("uspech!");
	}
}
