package fido.contacts.merge.test;

import fido.contacts.merge.R;
import fido.contacts.merge.data.AggregationCandidate;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.ui.resultDetail.ResultDetailActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TestResultFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.test_result_fragment_view, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		DataManager manager;
		try {
			manager = DataManager.get();

			ResultDetailActivity activity = (ResultDetailActivity) getActivity();
			AggregationCandidate ac = manager.getAggregationCandidatesList().get(activity.index);
			
			//View layout = inflater.inflate(R.layout.test_result_fragment_view, container, false);
			TextView tField = (TextView) getActivity().findViewById(R.id.testView1);
			tField.setText(getInfo(manager, ac));
			tField.setMovementMethod(new ScrollingMovementMethod());
			
		} catch (DataManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private CharSequence getInfo(DataManager manager, AggregationCandidate ac) {
		String info = null;
		
//		info = writeInfo(manager, ac.getLookup1());
//		info = String.format("%s%s", info, System.getProperty("line.separator"));
//		info += writeInfo(manager, ac.getLookup2());
		
//		info = writeDifferencies(manager, ac.getFirstRawContact().getLookupKey(), ac.getSecondRawContact().getLookupKey());
		
		return info;
	}

//	private String writeDifferencies(DataManager manager, String lookup1,
//			String lookup2) {
////		String s = "";
//////		Cursor data1 = manager.queryData(null, ContactsContract.Data.LOOKUP_KEY + " = '" + lookup1 + "'");
//////		Cursor data2 = manager.queryData(null, ContactsContract.Data.LOOKUP_KEY + " = '" + lookup2 + "'");
////		data1.moveToFirst();
////		data2.moveToFirst();
//////		System.out.println(data1.getColumnCount());
//////		System.out.println(data2.getColumnCount());
////		
////		for(int i = 0; i < data1.getColumnCount(); i++){
////			//s += data.getColumnName(i) + ": " + data.getString(i) + "/n";
//////			System.out.println(data1);
//////			System.out.println(data2);
//////			System.out.println(data1.getString(i) == null ? "null" : data1.getString(i));
//////			System.out.println(data2.getString(i) == null ? "null" : data2.getString(i));
////			if(data1.getString(i) != null && data2.getString(i) != null){
////				s = String.format("%s%s%s:%s%s%s%s", s, System.getProperty("line.separator"), 
////						data1.getColumnName(i), System.getProperty("line.separator"), data1.getString(i),
////						System.getProperty("line.separator"), data2.getString(i)); 
////			}
////		}
////		
////		return s;
//	}

//	private String writeInfo(DataManager manager, String lookupKey) {
////		Cursor data = manager.queryData(null, ContactsContract.Data.LOOKUP_KEY + " = '" + lookupKey + "'");
////		String s = "";
////		data.moveToFirst();
////		for(int i = 0; i < data.getColumnCount(); i++){
////			//s += data.getColumnName(i) + ": " + data.getString(i) + "/n";
////			s = String.format("%s%s%s: %s", s, System.getProperty("line.separator"), 
////					data.getColumnName(i), data.getString(i)); 
////		}
////		return s;
//	}
	
	
	

}
