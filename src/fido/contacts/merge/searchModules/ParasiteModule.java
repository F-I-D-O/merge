package fido.contacts.merge.searchModules;

import java.util.ArrayList;
import java.util.HashMap;

import fido.contacts.merge.R;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.DataManagerException;

public abstract class ParasiteModule<E extends Enum<E>> extends SearchModule<E> {
	
	protected ArrayList<Class> requiredModules;
	
	protected HashMap<Class<SearchModule>, ArrayList<ModuleData>> data;

	public ParasiteModule(boolean activated) {
		super(activated);
		requiredModules = new ArrayList<Class>();
		addRequiredModules();
	}
	
	@Override
	public void run() throws DataManagerException, ModuleManagerException {	
		super.run();
		moduleManager.updateModuleProgress(PROGRESS_VALUE_QUERING_DATA, R.string.search_module_status_obtaining_data);
		getData();
		moduleManager.updateModuleProgress(PROGRESS_VALUE_SEARCHING_FOR_AGGREGATION_CANDIDATES, 
				R.string.search_module_status_searching_aggregation_candidates);
		findAggregationCandidates();
	}

	private void getData() throws DataManagerException, ModuleManagerException {
		data = new HashMap<Class<SearchModule>, ArrayList<ModuleData>>();
		for (Class<SearchModule> dataSourceModule : requiredModules) {
			ArrayList<ModuleData> dataFromSourceModule = moduleManager.getDataFromModule(dataSourceModule);
			data.put(dataSourceModule, dataFromSourceModule);
		}
	}
	
	protected abstract void addRequiredModules();

}
