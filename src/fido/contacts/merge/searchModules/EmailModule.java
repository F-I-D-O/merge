package fido.contacts.merge.searchModules;

import java.util.EnumMap;
import android.database.Cursor;
import android.provider.ContactsContract;
import fido.contacts.merge.R;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.data.Email;

/**
 * @author Fido
 * Module for searching for aggregation candidates on the basis of email.
 */
public class EmailModule extends SimpleSearchModule<EmailModule.SearchSettings> {
		
	public enum SearchSettings {
		SEARCH_SIMIALR,
		DEEP_SEARCH
	}
	
	/**
	 * Constructor. initializes all settings.
	 */
	public EmailModule() {
		super(true);	
	}

	@Override
	public int getDisplayNameId() {
		return R.string.email_display_name;
	}

	@Override
	protected void createSearchOptions() {
		searchSettingList = new EnumMap<SearchSettings, ModuleSetting>(SearchSettings.class);
		searchSettingList.put(SearchSettings.SEARCH_SIMIALR, 
				new ModuleSettingCheckbox(R.string.email_module_setting_search_similar_display_name, true));
		searchSettingList.put(SearchSettings.DEEP_SEARCH, 
				new ModuleSettingCheckbox(R.string.email_module_setting_deep_search_display_name, false));
	}

	@Override
	public void run() throws DataManagerException, ModuleManagerException {	
		super.run();
	}
	
	@Override
	protected void fillDataFromCursorLine(Cursor cursor) {
		dataList.add(new Email(cursor.getString(cursor.getColumnIndex(ContactsContract.Data.LOOKUP_KEY)),
				cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)),
				cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)),
				cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA1))));
	}

	@Override
	protected String[] getProjection() {
		return new String[]{
				ContactsContract.Data.LOOKUP_KEY,
				ContactsContract.Data.CONTACT_ID,
				ContactsContract.Data.RAW_CONTACT_ID,
				ContactsContract.Data.DATA1
		};
	}

	@Override
	protected String getSelection() {
		return ContactsContract.Data.MIMETYPE + " = '" + 
				ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE + "'";
	}

	@Override
	protected boolean dataAreAggregationCandidates(ModuleData dataObject1, ModuleData dataObject2) {
		Email email1 = (Email) dataObject1;
		Email email2 = (Email) dataObject2;
	
		if(email1.getAddres().equalsIgnoreCase(email2.getAddres())){
			return true;
		}
		
		else if (isSettingActivated(SearchSettings.SEARCH_SIMIALR)){
			return isSimilar(email1, email2);
		}
		else{
			return false;
		}
	}

	private boolean isSimilar(Email email1, Email email2) {
		String emailName1 = email1.getAddresWithotDomain();
		String emailName2 = email2.getAddresWithotDomain();
		if(emailName1.equalsIgnoreCase(emailName2)){
			return true;
		}
		else if(isSettingActivated(SearchSettings.DEEP_SEARCH)){
			return isSimilarDeepSearch(emailName1, emailName2);
		}
		else{
			return false;
		}
	}

	private boolean isSimilarDeepSearch(String emailName1, String emailName2) {
		
		return false;
	}

}
