package fido.contacts.merge.searchModules;

import java.util.ArrayList;
import java.util.EnumMap;

import fido.contacts.merge.R;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import fido.contacts.merge.data.Email;
import fido.contacts.merge.data.Name;

public class EmailNameModule extends ParasiteModule<EmailNameModule.SearchSettings> {

	public enum SearchSettings {
		IGNORE_NAME_ORDER,
		DIFFERENT_NAME_LENGTH_COMPARISON
	}
	
	public EmailNameModule() {
		super(true);
	}

	@Override
	public int getDisplayNameId() {
		return R.string.email_name_module_display_name;
	}

	@Override
	protected void createSearchOptions() {
		searchSettingList = new EnumMap<SearchSettings, ModuleSetting>(SearchSettings.class);
		searchSettingList.put(SearchSettings.IGNORE_NAME_ORDER, new ModuleSettingCheckbox(
				R.string.name_module_setting_ignore_name_order, false));
		searchSettingList.put(SearchSettings.DIFFERENT_NAME_LENGTH_COMPARISON, new ModuleSettingCheckbox(
				R.string.name_module_setting_different_name_length_comparison, false));
	}

	@Override
	protected void findAggregationCandidates() throws DataManagerException {
		ArrayList<ModuleData> emails = data.get(EmailModule.class);
		ArrayList<ModuleData> names = data.get(NameModule.class);
		for (int i = 0; i < emails.size() - 1; i++) {
			ModuleData dataObject1 = emails.get(i);
//			System.out.println(((Email) dataObject1).getAddresNameParts().toString());
			for (int j = 0; j < names.size(); j++){
				ModuleData dataObject2 = names.get(j);
				if (dataAreAggregationCandidates(dataObject1, dataObject2) && 
						!dataFromSameContact(dataObject1, dataObject2)){
					DataManager.get().addAgregaitonCandidate(dataObject1.getContactId(), dataObject2.getContactId(),
							dataObject1.getRawContactId(), dataObject2.getRawContactId(), getDisplayNameId());
				}
			}
			moduleManager.updatePartialProgress(i+1, String.format("%d/%d", i + 1, emails.size()));
		}
	}

	@Override
	protected boolean dataAreAggregationCandidates(ModuleData dataObject1, ModuleData dataObject2) {
		Email email = (Email) dataObject1;
		Name name = (Name) dataObject2;
		ArrayList<String> emailNameParts = email.getAddresNameParts();
//		ArrayList<String> emailNameParts = new ArrayList<String>();
		return CompareNameParts(emailNameParts, name.getParts());
	}

	private boolean CompareNameParts(ArrayList<String> emailNameParts, ArrayList<String> NameParts) {
		if(emailNameParts.size() == NameParts.size() || 
				isSettingActivated(SearchSettings.DIFFERENT_NAME_LENGTH_COMPARISON)){
			int matchedPartsCount = 0;
			int shorterNameParts = emailNameParts.size() < NameParts.size() ? emailNameParts.size() : NameParts.size();
			for (String emailNamePart : emailNameParts) {
				matchedPartsCount += coutMatches(emailNamePart, emailNameParts.indexOf(emailNamePart), NameParts);	
			}
			if (shorterNameParts == matchedPartsCount){
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	private int coutMatches(String emailNamePart, int indexOfEmailNamePart, ArrayList<String> nameParts) {
		int matchedPartsCount = 0;
		if(isSettingActivated(SearchSettings.IGNORE_NAME_ORDER)){
			for (String namePart : nameParts) {
				if(partMatch(emailNamePart, namePart)){
					matchedPartsCount++;
					break;
				}
			}
		}
		else{
			if(nameParts.size() > indexOfEmailNamePart && 
					partMatch(emailNamePart, nameParts.get(indexOfEmailNamePart))){
				matchedPartsCount++;
			}
		}
		return matchedPartsCount;
	}

	private boolean partMatch(String emailNamePart, String namePart) {
		return emailNamePart.equalsIgnoreCase(namePart);
	}

	@Override
	protected void addRequiredModules() {
		requiredModules.add(EmailModule.class);
		requiredModules.add(NameModule.class);
	}

}
