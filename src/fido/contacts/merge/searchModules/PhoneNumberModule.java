package fido.contacts.merge.searchModules;

import java.util.EnumMap;
import fido.contacts.merge.R;
import android.database.Cursor;
import android.provider.ContactsContract;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.PhoneNumber;

public class PhoneNumberModule extends SimpleSearchModule<PhoneNumberModule.SearchSettings> {
	
	public enum SearchSettings {
		SPLIT_PREFIX
	}

	public PhoneNumberModule() {
		super(true);
	}
	
	@Override
	public int getDisplayNameId() {
		return R.string.phone_number_module_display_name;
	}


	@Override
	protected boolean dataAreAggregationCandidates(ModuleData dataObject1, ModuleData dataObject2) {
		PhoneNumber phoneNumber1 = (PhoneNumber) dataObject1;
		PhoneNumber phoneNumber2 = (PhoneNumber) dataObject2;
		return phoneNumber1.getPhoneNumber().equals(phoneNumber2.getPhoneNumber());
	}

	
	@Override
	protected void createSearchOptions() {
		searchSettingList = new EnumMap<SearchSettings, ModuleSetting>(SearchSettings.class);
		searchSettingList.put(SearchSettings.SPLIT_PREFIX, 
				new ModuleSettingCheckbox(R.string.phone_module_setting_split_prefix, true));
	}

	@Override
	protected void fillDataFromCursorLine(Cursor cursor) {
		dataList.add(
			new PhoneNumber(
				cursor.getString(cursor.getColumnIndex(ContactsContract.Data.LOOKUP_KEY)),
				cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)),
				cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)),
				cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)), 
				isSettingActivated(SearchSettings.SPLIT_PREFIX)));
	}

	@Override
	protected String[] getProjection() {
		return new String[]{
			ContactsContract.Data.LOOKUP_KEY,
			ContactsContract.Data.CONTACT_ID,
			ContactsContract.Data.RAW_CONTACT_ID,	
			ContactsContract.CommonDataKinds.Phone.NUMBER
		};
	}

	@Override
	protected String getSelection() {
		return ContactsContract.Data.MIMETYPE + " = '" + 
				ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'";
	}

}
