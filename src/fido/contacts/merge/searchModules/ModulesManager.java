package fido.contacts.merge.searchModules;

import java.util.ArrayList;

import fido.contacts.merge.R;
import fido.contacts.merge.SearchThread;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.DataManagerException;

/**
 * @author Fido
 * Singleton for managing search modules.
 */
public class ModulesManager {
	
	/**
	 * STATIC
	 */
	
	/**
	 * ModulesManager instance.
	 */
	private static ModulesManager modulesManager;
	
	
	/**
	 * Creates ModulManager instance.
	 * @throws ModuleManagerException 
	 */
	public static void create() throws ModuleManagerException{
		if(modulesManager != null){
			throw new ModuleManagerException(ModuleManagerException.MANAGER_ALREADY_CREATED);
		}
		modulesManager = new ModulesManager();
	}
	
	/**
	 * Returns ModulesManager instance.
	 * @return ModulesManager instance.
	 * @throws ModuleManagerException 
	 */
	public static ModulesManager get() throws ModuleManagerException{
		if(modulesManager == null){
			throw new ModuleManagerException(ModuleManagerException.MANAGER_NULL);
		}
		return modulesManager;
	}
	
	/**
	 * Determines if there is already an instance of ModuleManager created.
	 * @return if module manager has been already created.
	 */
	public static boolean isInit(){
		return modulesManager != null;
	}
	
	
	
	
	/**
	 * INSTANCE
	 */
	
	private SearchThread searchTrhread;
	
	/**
	 * List of search modules filled in constructor.
	 */
	private ArrayList<SearchModule> searchModules;
	
	private SearchProgress progress;
	
	private int numberOfActivatedModules = 0;
	
	private int numberOfCompletedModules = 0;
	
	
	/**
	 * SearchModules list getter.
	 * @return List of search modules.
	 */
	public ArrayList<SearchModule> getSearchModules() {
		return searchModules;
	}

	public SearchProgress getProgress() {
		return progress;
	}

	public int getNumberOfActivatedModules() {
		return numberOfActivatedModules;
	}

	/**
	 * Run all modules.
	 * @param searchThread 
	 * @throws DataManagerException If DataManager has't been created yet.
	 * @throws ModuleManagerException 
	 */
	public void runModules(SearchThread searchThread) throws DataManagerException, ModuleManagerException {
		this.searchTrhread = searchThread;
		progress = new SearchProgress();
		for (SearchModule searchModule : getSearchModules()) {
			if(searchModule.isActivated()){
				updateOverallProgress(searchModule.getDisplayNameId());
				searchModule.run();
				numberOfCompletedModules++;		
			}
		}
	}
	
	public void countActivatedModules() {
		for (SearchModule searchModule : getSearchModules()) {
			if(searchModule.isActivated()){
				numberOfActivatedModules++;
			}
		}
	}
	
	public void updatePartialProgressMax(int completedValue){
		progress.setPartialProgresMaxValue(completedValue);
	}
	
	public void updatePartialProgress(int progressValue, String info){
		progress.setPartialProgressHeaderString(info);
		progress.setPartialProgress(progressValue);
		searchTrhread.updateProgress();
	}
	
	public void updateModuleProgress(int progressValue, int progresStringId){
		progress.setModuleProgressHeaderStringId(progresStringId);
		progress.setModuleProgress(progressValue);
		searchTrhread.updateProgress();
	}
	
	private void updateOverallProgress(int activeModuleNameId){
		progress.setTotalProgress(numberOfCompletedModules);
		progress.setTotalProgressHeaderStringId(activeModuleNameId);
		progress.setModuleProgressHeaderStringId(R.string.blank);
		searchTrhread.updateProgress();
	}
	
	
	/**
	 * Constructor. Initializes the searchModule list and fills it with search modules.
	 */
	private ModulesManager(){
		searchModules = new ArrayList<SearchModule>();
		addModule(new EmailModule());
		addModule(new PhoneNumberModule());
		addModule(new NameModule());
		addModule(new EmailNameModule());
	}

	private void addModule(SearchModule searchModule) {
		searchModules.add(searchModule);		
	}

	public ArrayList<ModuleData> getDataFromModule(Class<SearchModule> dataSourceModuleClass) throws DataManagerException, ModuleManagerException {
		SimpleSearchModule dataSourceModule = getModule(dataSourceModuleClass);
		if(!dataSourceModule.isActivated()){
			dataSourceModule.parasiteRun();
		}
		return dataSourceModule.getDataList();
	}

	private SimpleSearchModule getModule(Class<SearchModule> dataSourceModuleClass) {
		for (SearchModule searchModule : searchModules) {
			if (searchModule.getClass() == dataSourceModuleClass) {
				return (SimpleSearchModule) searchModule;
			}
		}
		return null;
	}	
}
