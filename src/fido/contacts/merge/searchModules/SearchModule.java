package fido.contacts.merge.searchModules;

import java.util.ArrayList;
import java.util.EnumMap;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;
import android.provider.ContactsContract;


/**
 * @author Fido
 * Abstract class for implementing search modules
 */
public abstract class SearchModule<E extends Enum<E>> {
	
	protected static final int PROGRESS_VALUE_QUERING_DATA = 0;
	protected static final int PROGRESS_VALUE_GETING_DATA_FROM_CURSOR = 50;
	protected static final int PROGRESS_VALUE_SEARCHING_FOR_AGGREGATION_CANDIDATES = 60;
	
	/**
	 * Determine if module is activated, or not. when module is deactivated, it won't do any search procedure. This 
	 * value can be changed in the Options menu.
	 */
	private boolean moduleActivated;
	
	/**
	 * List of search settings. These settings can be changed in search module Settings menu.
	 */
	protected EnumMap<E, ModuleSetting> searchSettingList;
	
	/**
	 * reference to module manager
	 */
	protected ModulesManager moduleManager;
	
	/**
	 * reference to data manager
	 */
	protected DataManager dataManager;
	
	
	
	
	
	/**
	 * Determine if module is activated or not.
	 * @return true if module is activated, false otherwise.
	 */
	public boolean isActivated() {
		return moduleActivated;
	}

	/**
	 * Set the active state of the module.
	 * @param activated True to Activate the module, false to deactivate.
	 */
	public void setActivated(boolean activated) {
		this.moduleActivated = activated;
	}
	
	public ArrayList<ModuleSetting> getSearchSettingList() {
		return new ArrayList<ModuleSetting>(searchSettingList.values());
	}
	
	
	

	/**
	 * @author Fido
	 * Root abstract class for setting up the module.
	 */
	public static abstract class ModuleSetting{
		
		/**
		 * Display name ID of the setting. This determines the sting displayed in the module setting menu.
		 */
		private int displayNameId;

		/**
		 * Getter for displayNameId.
		 * @return Display name ID.
		 */
		public int getDisplayNameId() {
			return displayNameId;
		}

		/**
		 * Constructor.
		 * @param displayNameId Display name.
		 */
		public ModuleSetting(int displayNameId) {
			this.displayNameId = displayNameId;
		}
		
		/**
		 * Returns info about the state of the setting. 
		 * @return Object containing the info about setting state.
		 */
		public abstract Object getState();
		
		/**
		 * Set the setting in module.
		 * @param state Object containing new state of the setting.
		 * @throws WrongDataTypeException If the state parameter is incompatible with the setting.
		 */
		public abstract void setState(Object state) throws WrongDataTypeException;
	}
	
	/**
	 * @author Fido
	 * Class for checkBox module setting
	 */
	public static class ModuleSettingCheckbox extends ModuleSetting{
		
		/**
		 * Determines whether the setting is activated or not.
		 */
		private boolean settingActivated;
		
		/**
		 * Constructor. Set the settingActivated property to the default state.
		 * @param displayNameId Display name.
		 */
		public ModuleSettingCheckbox(int displayNameId, boolean defaultState) {
			super(displayNameId);
			settingActivated = defaultState;
		}

		@Override
		public Boolean getState() {
			return settingActivated;
		}

		@Override
		public void setState(Object state) throws WrongDataTypeException {
			if (state.getClass() != Boolean.class){
				throw new WrongDataTypeException(state.getClass().toString(), Boolean.class.toString(), 
						this.getClass().toString());
			}
			settingActivated = (Boolean) state;
		}
	}

	
	
	
	/**
	 * Constructor.
	 * @param activated Default active state.
	 */
	public SearchModule(boolean activated) {
		this.moduleActivated = activated;
		createSearchOptions();
	}
	
	/**
	 * Getter for resource id of the module display name.
	 * @return Id of the String representing {@link SearchModule} display name
	 */
	public abstract int getDisplayNameId(); 
	
	/**
	 * Main execution method of the module.
	 * @throws DataManagerException If Data manager hasn't been created yet.
	 * @throws ModuleManagerException 
	 */
	public void run() throws DataManagerException, ModuleManagerException {
		dataManager = DataManager.get(); 
		moduleManager = ModulesManager.get();
	}
	
	/**
	 * Fill searchSettingList with all available options
	 */
	protected abstract void createSearchOptions(); 
	
	/**
	 * Determines if two data object belongs to the same contact
	 * @param dataObject1 first data object
	 * @param dataObject2 second data object
	 * @return true if data belongs to the same contact, false otherwise
	 */
	protected abstract boolean dataAreAggregationCandidates(ModuleData dataObject1, ModuleData dataObject2);
	
	/**
	 * Determines if two data records belongs to the same {@link ContactsContract.Contacts} contact.
	 * @param dataObject1 First data record.
	 * @param dataObject2 Second data record.
	 * @return True if both records belongs to the same contact.
	 */
	protected boolean dataFromSameContact(ModuleData dataObject1, ModuleData dataObject2) {
		return dataObject1.getContactId() == dataObject2.getContactId();
	}
	
	/**
	 * Finds all Candidates for Aggregation. Main Search method.
	 * @throws DataManagerException If Data manager hasn't been created yet.
	 */
	protected abstract void findAggregationCandidates() throws DataManagerException;
	
	/**
	 * 
	 * @param searchSettingKey
	 * @return
	 */
	protected boolean isSettingActivated (E searchSettingKey){
		return ((ModuleSettingCheckbox) searchSettingList.get(searchSettingKey)).getState();
	}
	
}
