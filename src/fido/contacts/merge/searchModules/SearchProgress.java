package fido.contacts.merge.searchModules;

import fido.contacts.merge.Progress;

public class SearchProgress extends Progress {
	private int moduleProgress = 0;
	private int partialProgress = 0;
	private int moduleProgressHeaderStringId;
	private String partialProgressHeaderString;
	private int partialProgresMaxValue;
	
	public int getModuleProgress() {
		return moduleProgress;
	}
	
	public void setModuleProgress(int moduleProgress) {
		this.moduleProgress = moduleProgress;
	}
	
	public int getPartialProgress() {
		return partialProgress;
	}

	public void setPartialProgress(int partialProgress) {
		this.partialProgress = partialProgress;
	}

	public int getModuleProgressHeaderStringId() {
		return moduleProgressHeaderStringId;
	}
	
	public void setModuleProgressHeaderStringId(int moduleProgressHeaderStringId) {
		this.moduleProgressHeaderStringId = moduleProgressHeaderStringId;
	}

	public String getPartialProgressHeaderString() {
		return partialProgressHeaderString;
	}
	
	public void setPartialProgressHeaderString(String info) {
		this.partialProgressHeaderString = info;
	}

	public int getPartialProgresMaxValue() {
		return partialProgresMaxValue;
	}

	public void setPartialProgresMaxValue(int partialProgresMaxValue) {
		this.partialProgresMaxValue = partialProgresMaxValue;
	}
	
	
}
