package fido.contacts.merge.searchModules;

import java.util.ArrayList;
import java.util.EnumMap;

import android.database.Cursor;
import android.provider.ContactsContract;
import fido.contacts.merge.R;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.Name;

public class NameModule extends SimpleSearchModule<NameModule.SearchSettings> {
	
	public enum SearchSettings{
		IGNORE_NAME_ORDER,
		DIFFERENT_NAME_LENGTH_COMPARISON
	}

	public NameModule() {
		super(true);
	}

	@Override
	protected boolean dataAreAggregationCandidates(ModuleData dataObject1, ModuleData dataObject2) {
		Name name1 = (Name) dataObject1;
		Name name2 = (Name) dataObject2;
		return CompareNameParts(name1.getParts(), name2.getParts());
	}

	private boolean CompareNameParts(ArrayList<String> parts1, ArrayList<String> parts2) {
		if(parts1.size() == parts2.size() || isSettingActivated(SearchSettings.DIFFERENT_NAME_LENGTH_COMPARISON)){
			int matchedPartsCount = 0;
			int shorterNameParts = parts1.size() < parts2.size() ? parts1.size() : parts2.size();
			for (String name1Part : parts1) {
				matchedPartsCount += coutMatches(name1Part,parts1.indexOf(name1Part), parts2);	
			}
			if (shorterNameParts == matchedPartsCount){
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	private int coutMatches(String name1Part, int name1PartIndex, ArrayList<String> parts2) {
		int matchedPartsCount = 0;
		if(isSettingActivated(SearchSettings.IGNORE_NAME_ORDER)){
			for (String name2Part : parts2) {
				if(partMatch(name1Part, name2Part)){
					matchedPartsCount++;
					break;
				}
			}
		}
		else{
			if(parts2.size() > name1PartIndex && partMatch(name1Part, parts2.get(name1PartIndex))){
				matchedPartsCount++;
			}
		}
		return matchedPartsCount;
	}

	private boolean partMatch(String name1Part, String name2Part) {
		return name1Part.equalsIgnoreCase(name2Part);
	}

	@Override
	public int getDisplayNameId() {
		return R.string.name_module_display_name;
	}

	@Override
	protected void createSearchOptions() {
		searchSettingList = new EnumMap<NameModule.SearchSettings, SearchModule.ModuleSetting>(SearchSettings.class);
		searchSettingList.put(SearchSettings.IGNORE_NAME_ORDER, new ModuleSettingCheckbox(
				R.string.name_module_setting_ignore_name_order, false));
		searchSettingList.put(SearchSettings.DIFFERENT_NAME_LENGTH_COMPARISON, new ModuleSettingCheckbox(
				R.string.name_module_setting_different_name_length_comparison, false));
	}

	@Override
	protected void fillDataFromCursorLine(Cursor cursor) {
//		if(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME)) != null){
//			System.out.println(cursor.getString(cursor.getColumnIndex(
//				ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME)));
//		}
//		else {
//			System.out.println("null - " + cursor.getLong(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)));
//		}
		if(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME)) 
				!= null){
			dataList.add(
					new Name(
						cursor.getString(cursor.getColumnIndex(ContactsContract.Data.LOOKUP_KEY)),
						cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID)),
						cursor.getInt(cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID)),
						cursor.getString(cursor.getColumnIndex(
								ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME))));
		}
	}

	@Override
	protected String[] getProjection() {
		return new String[]{
				ContactsContract.Data.LOOKUP_KEY,
				ContactsContract.Data.CONTACT_ID,	
				ContactsContract.Data.RAW_CONTACT_ID,
				ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME
			};
	}

	@Override
	protected String getSelection() {
		return ContactsContract.Data.MIMETYPE + " = '" + 
				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'";
	}

}
