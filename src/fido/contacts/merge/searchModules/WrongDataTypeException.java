package fido.contacts.merge.searchModules;

public class WrongDataTypeException extends Exception {
	private static final long serialVersionUID = 1L;

	private static final String EXCEPTION_TEXT = "Data type exception: ";
	private static final String RECEIVED = "data received: ";
	private static final String EXPECTED = ", data expected: ";
	private static final String IN = " in: ";

	public WrongDataTypeException(String receivedData, String expectedData, String setting) {
		super(EXCEPTION_TEXT + RECEIVED + receivedData + EXPECTED + expectedData + IN + setting);
	}
}
