package fido.contacts.merge.searchModules;

public class ModuleManagerException extends Exception {
	private static final long serialVersionUID = -471943954327167777L;

	private static final String EXCEPTION_TEXT = "Module manager exception: ";
	
	public static final String MANAGER_ALREADY_CREATED = "Module manager has been already created!";	
	public static final String MANAGER_NULL = "Module manager has not been created!";
	
	public ModuleManagerException(String message) {
		super(EXCEPTION_TEXT + message);
	}
}
