package fido.contacts.merge.searchModules;

import java.util.ArrayList;

import android.database.Cursor;
import android.provider.ContactsContract;

import fido.contacts.merge.R;
import fido.contacts.merge.data.ModuleData;
import fido.contacts.merge.data.DataManager;
import fido.contacts.merge.data.DataManagerException;

public abstract class SimpleSearchModule<E extends Enum<E>> extends SearchModule<E> {

	/**
	 * list of data rows which are needed for searching
	 */
	protected ArrayList<ModuleData> dataList;
	
	public ArrayList<ModuleData> getDataList() {
		return dataList;
	}
	
	public SimpleSearchModule(boolean activated) {
		super(activated);
	}
	
	public void parasiteRun() throws DataManagerException, ModuleManagerException{
		moduleManager = ModulesManager.get();
		moduleManager.updateModuleProgress(PROGRESS_VALUE_QUERING_DATA, R.string.search_module_status_obtaining_data);
		Cursor cursor = queryData();
		moduleManager.updatePartialProgressMax(cursor.getCount());
		moduleManager.updateModuleProgress(PROGRESS_VALUE_GETING_DATA_FROM_CURSOR, 
				R.string.search_module_status_geting_data_from_cursor);
		dataList = new ArrayList<ModuleData>();
		fillListFromCursor(cursor);
	}

	@Override
	public void run() throws DataManagerException, ModuleManagerException {	
		super.run();
		moduleManager.updateModuleProgress(PROGRESS_VALUE_QUERING_DATA, R.string.search_module_status_obtaining_data);
		Cursor cursor = queryData();
		moduleManager.updatePartialProgressMax(cursor.getCount());
		moduleManager.updateModuleProgress(PROGRESS_VALUE_GETING_DATA_FROM_CURSOR, 
				R.string.search_module_status_geting_data_from_cursor);
		dataList = new ArrayList<ModuleData>();
		fillListFromCursor(cursor);
		moduleManager.updateModuleProgress(PROGRESS_VALUE_SEARCHING_FOR_AGGREGATION_CANDIDATES, 
				R.string.search_module_status_searching_aggregation_candidates);
		findAggregationCandidates();
	}
	
	/**
	 * Add one {@link ModuleData} object to dataList. The object is created from data from one {@link Cursor} line.
	 * @param cursor {@link Cursor} with the data needed for searching.
	 */
	protected abstract void fillDataFromCursorLine(Cursor cursor);
	
	@Override
	protected void findAggregationCandidates() throws DataManagerException {
		for (int i = 0; i < dataList.size() - 1; i++) {			
			ModuleData dataObject1 = dataList.get(i);
			for (int j = i + 1; j < dataList.size(); j++){
				ModuleData dataObject2 = dataList.get(j);
				if (dataAreAggregationCandidates(dataObject1, dataObject2) && 
						!dataFromSameContact(dataObject1, dataObject2)){
//					System.out.println(dataObject1.getRawContactId() + "*" + dataObject2.getRawContactId());
					DataManager.get().addAgregaitonCandidate(dataObject1.getContactId(), dataObject2.getContactId(), 
							dataObject1.getRawContactId(), dataObject2.getRawContactId(), getDisplayNameId());
				}
			}
			moduleManager.updatePartialProgress(i + 1, String.format("%d/%d", i + 1, dataList.size()));
		}
	}
	
	/**
	 * Determines which type of data are needed for module
	 * @return Projection - columns of {@link ContactsContract.Data} table we want.
	 */
	protected abstract String[] getProjection();
	
	/**
	 * Determines which data rows should be included/excluded for module.
	 * @return Selection - rows of {@link ContactsContract.Data} table we want.
	 */
	protected abstract String getSelection();
	
	/**
	 * Ask the Data manager for data needed for searching. The projection and selection is determined by implementation
	 * of getProjection and GetSelection methods in concrete module.
	 * @return Cursor filled with data for search in.
	 * @throws DataManagerException If Data manager hasn't been created yet.
	 */
	protected Cursor queryData() throws DataManagerException {
		String[] projection = getProjection();
		String selection = getSelection();
		return DataManager.get().getDataService().queryData(projection, selection);
	}

	/**
	 * Fill dataList with data from {@link Cursor}.
	 * @param cursor {@link Cursor} with the data needed for searching.
	 */
	private void fillListFromCursor(Cursor cursor){
		if (cursor.moveToFirst()){
			int counter = 0;
			do{
				fillDataFromCursorLine(cursor);
				moduleManager.updatePartialProgress(++counter, String.format("%d/%d", counter, cursor.getCount()));
		
			}while(cursor.moveToNext());
		}
		cursor.close();
	}
	
}
