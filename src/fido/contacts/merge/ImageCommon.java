package fido.contacts.merge;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class ImageCommon {
	
	public static BitmapDrawable resize(Drawable image, Context context) {
	    Bitmap b = ((BitmapDrawable)image).getBitmap();
	    Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 100, 100, false);
	    return new BitmapDrawable(context.getResources(), bitmapResized);
	}

}
