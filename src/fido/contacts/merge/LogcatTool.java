package fido.contacts.merge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class LogcatTool {
	
	private static final String LOGCAT_TAG = "Logcat Tool";
	
	private static final String ERROR_READING_FROM_BUFFERD_READER = "Cannot read from log stream";
	private static final String ERROR_EXECUTING_LOGCAT = "Failed to execute logcat: ";
	private static final String ERROR_CANNOT_WRITE_LOGCAT_TO_FILE = "Cannot write logcat to external storage";
//	private static final String COMMAND_GET_APP_LOGCAT = "logcat -d -s \"?\"";
	private static final String COMMAND_GET_APP_LOGCAT = "adb logcat";
		
	private String appName;

	public LogcatTool(Context context) {
//		appName = context.getString(context.getApplicationInfo().labelRes);
		appName = "fido.contacts.merge";
	}

	public void saveLogcat(){
		String logcatString = getAppLogcat();
		System.out.println("tu6");
		// Get the directory for the user's public downloads directory. 
	    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), 
	    		appName + ".txt");
	    
	    try {
			FileOutputStream fos = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(fos);
			
			pw.write(logcatString);
			pw.close();
			
		} catch (FileNotFoundException e) {
			Log.e(LOGCAT_TAG, ERROR_CANNOT_WRITE_LOGCAT_TO_FILE);
		}
	}
	
	private String getAppLogcat(){
		Process process;
		String logString = "";
		String commandGetLogcat = COMMAND_GET_APP_LOGCAT.replace("?", appName);
		try {
			process = Runtime.getRuntime().exec(commandGetLogcat);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			System.out.println("tu42");
			StringBuilder log = new StringBuilder();
			String line;
			
			try {
				while ((line = bufferedReader.readLine()) != null) {
					log.append(line);
				}
				logString = log.toString();
				System.out.println("tu5");
			} catch (IOException e) {
				logString = ERROR_READING_FROM_BUFFERD_READER;
				Log.e(LOGCAT_TAG, ERROR_READING_FROM_BUFFERD_READER);
			}
		} catch (IOException e1) {
			logString = ERROR_EXECUTING_LOGCAT + commandGetLogcat;
			Log.e(LOGCAT_TAG, ERROR_EXECUTING_LOGCAT + commandGetLogcat);
			e1.printStackTrace();
		}
		
		return logString;
	}
}
